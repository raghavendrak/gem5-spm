#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <spm_api_1.h>
#include <arm_neon.h>
#define SAMPLES 16384

int32_t y[SAMPLES], x[SAMPLES], z[SAMPLES];

#ifdef __arm__
#define DUMP
#endif
#ifdef DUMP
#include <m5op.h>
#endif


int main(int argc, char *argv[]) {
	spm_initialize(2);
	int rc = 0;
	uint32_t i  = 0, j = 0;



#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif




    for (i = 0; i < SAMPLES; i++) {
    	y[i] = x[i] + z[i];
    }



#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif


    for (i = 0; i < SAMPLES; i++) {
    	printf("%d ", y[i]);
    }
	return 0;
}
