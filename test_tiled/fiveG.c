#include <stdio.h>
#include <stdlib.h>
#include <spm_api.h>
#include <spm_api_1.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>     /* read(), close() */

int func()
{
	int x;
	spm_allocate((unsigned long)&x, (unsigned long)(0x0), 4, ALLOC_STACK);
	// printf ("address x: %x\n", &x);
	x = 3 + 8;
	// spm_deallocate((unsigned long)&x, (unsigned long)(0x0), 4, DEALLOC_STACK);
	return x;
}

int main()
{
	int *a;
	spm_allocate((unsigned long)&a, (unsigned long)(0x0), 8, ALLOC_STACK); // When using STACK, the second address is dummy
	int x;
	x = 20;
	a = (int *) malloc(sizeof(int) * 10);
	a[0] = x;
	a[1] = 24;
	spm_initialize(4);
	spm_allocate((unsigned long)a, (unsigned long)(local_spm[0]), sizeof(int) * 256, COPY);

	spm_block(1000000, BLOCK_FOR_X_CYCLES);

	local_spm[1][0] = 5;
	// local_spm[1][0]++;
	local_spm[0][0] = 26;

	// printf ("program addr: %x a[0]: %d addr_a: %x addr_x: %x stack_base: %x\n", a, a[0], &a, &x, STACK_BASE);

	spm_deallocate((unsigned long)a, (unsigned long)(local_spm[0]), sizeof(int) * 256, WRITE_BACK);
	// printf ("program addr: %x a[0]: %d\n", a, a[0]);

	// printf ("Data in local_spm[0][0, 1, 2, 3, 4]: %d %d %d %d %d\n", local_spm[0][0], local_spm[0][1], local_spm[0][2], local_spm[0][3], local_spm[0][4]);

	// printf ("Data in local_spm[1][0]: %d\n", local_spm[1][0]);
	// printf ("Program done\n");

	spm_deallocate((unsigned long)&a, (unsigned long)(0x0), 8, DEALLOC_STACK);

	// spm_deallocate((unsigned long)(0x0), (unsigned long)(0x0), 4, DEALLOC_STACK_POP);
	// printf ("Value returned by func: %d\n", y);
	return 0;
}
