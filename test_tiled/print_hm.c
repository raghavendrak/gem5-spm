#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <spm_api_1.h>
#ifdef __arm__
#define DUMP
#endif
#ifdef DUMP
#include <m5op.h>
#endif

#define TASK_THREADS 8
pthread_barrier_t barr;
int a[10000], b[10000], c[10000];

pthread_t threads[TASK_THREADS];

void* soft_demap_pthread(void *p) {
	int *temp = (int*)p;
	int id = *temp;
	int x;
	int z = 0, i;
	int *spm;
	spm = (int *) local_spm[7];
	if (id == 0) {
		spm_block(2000000, BLOCK_TILL_X_CYCLES);
		spm[0] = 8;
		//spm_block(6000000, BLOCK_INJECTION_X_CYCLES);
		/*
		for (i = 0; i < 10; i++)
			z += i;
		// local_spm[1][0] = 3;
		spm[0] = z;
		*/
		// printf ("spm[0]: %d\n", spm[0]);
	}
	if (id == 1) {
		spm_block(2000000, BLOCK_TILL_X_CYCLES);
		spm[0] = 8;
		//spm_block(6000000, BLOCK_FOR_X_CYCLES);
		// x = local_spm[1][0]++;
		// x = spm[0];
		//printf ("value of x: %d\n", x);
	}
	else {
		spm_block(2000000, BLOCK_TILL_X_CYCLES);
		spm[0] = 8;
	}
	//pthread_barrier_wait(&barr);
}

int main(int argc, char *argv[]) {
	spm_initialize(TASK_THREADS);
	int rc = 0;
	int i  = 0;
	int args[TASK_THREADS];
	//pthread_barrier_init(&barr, NULL, TASK_THREADS);
	int x = 0;
	uint64_t dummy;
	int *spm;
	int *spm1, *spm2;



	//m5_spm_block(0, PRINT_MARKER, dummy);
#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif
    	for (i=1; i<TASK_THREADS; i++) {
    		args[i] = i;
    		pthread_create(&threads[i], NULL, soft_demap_pthread, (void*) (args + i) );
    	}
    	args[0] = 0;
    	soft_demap_pthread((void*) (args + 0));
#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif

        /*
    m5_spm_block(1, PRINT_MARKER_START, dummy);
    for (i = 0; i < 1000; i++) {
    	// printf ("a[i] = %d\n", a[i]);
    }
    m5_spm_block(1, PRINT_MARKER_END, dummy);
    */

	spm = (int *) local_spm[0];
	//spm2 = (int *) local_spm[7];
	//spm[0] = 5;
	//spm2[0] = 6;
	//printf ("spm[0] = %d spm[1] = %d\n", spm[0], spm[1]);
	//spm_allocate(((unsigned long)&spm[0]), (unsigned long)(&spm[1]), 1, SPM_MEM_COPY);
	//printf ("spm[0] = %d spm[1] = %d\n", spm[0], spm[1]);

    return 0;
}
