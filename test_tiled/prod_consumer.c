#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <spm_api_1.h>
#ifdef __arm__
#define DUMP
#endif
#ifdef DUMP
#include <m5op.h>
#endif

#define TASK_THREADS 8
pthread_barrier_t barr;

pthread_t threads[TASK_THREADS];

void* soft_demap_pthread(void *p) {
	int *dummy;
	int *temp = (int*)p;
	int id = *temp;
	int x;
	int z = 0, i;
	int *spm;

	spm_allocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, COPY_MARK);
	printf("hello from id: %d\n", id);
	/*
	spm = (int *) local_spm[7];
	if (id == 0) {
		//spm_block(6000000, BLOCK_INJECTION_X_CYCLES);
		for (i = 0; i < 10; i++)
			z += i;
		// local_spm[1][0] = 3;
		spm[0] = z;
		// printf ("spm[0]: %d\n", spm[0]);
	}
	if (id == 1) {
		// spm_block(6000000, BLOCK_FOR_X_CYCLES);
		// x = local_spm[1][0]++;
		x = spm[0];
		//printf ("value of x: %d\n", x);
	}
	*/
	// pthread_barrier_wait(&barr);
	spm_deallocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, WRITE_BACK_MARK);
	spm_block(6000000, BLOCK_FOR_X_CYCLES);
}

void* soft_demap_pthread_1(void *p) {
	int *dummy;
	int *temp = (int*)p;
	int id = *temp;
	int x;
	int z = 0, i;
	int *spm;
	printf("hello from id: %d\n", id);
	spm_allocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, COPY_MARK);
	/*
	spm = (int *) local_spm[7];
	if (id == 0) {
		//spm_block(6000000, BLOCK_INJECTION_X_CYCLES);
		for (i = 0; i < 10; i++)
			z += i;
		// local_spm[1][0] = 3;
		spm[0] = z;
		// printf ("spm[0]: %d\n", spm[0]);
	}
	if (id == 1) {
		// spm_block(6000000, BLOCK_FOR_X_CYCLES);
		// x = local_spm[1][0]++;
		x = spm[0];
		//printf ("value of x: %d\n", x);
	}
	// pthread_barrier_wait(&barr);
	*/
	spm_deallocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, WRITE_BACK_MARK);
	spm_block(6000000, BLOCK_FOR_X_CYCLES);
}

int main(int argc, char *argv[]) {
	spm_initialize(TASK_THREADS);
	int rc = 0;
	int i  = 0;
	int args[TASK_THREADS];
	pthread_barrier_init(&barr, NULL, TASK_THREADS);

#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif
	for (i = 1; i < (TASK_THREADS / 2); i++) {
		args[i] = i;
		pthread_create(&threads[i], NULL, soft_demap_pthread, (void*) (args + i) );
	}
	for (i = (TASK_THREADS / 2); i < TASK_THREADS; i++) {
		args[i] = i;
		pthread_create(&threads[i], NULL, soft_demap_pthread_1, (void*) (args + i) );
	}
	args[0] = 0;
	soft_demap_pthread((void*) (args + 0));
#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif
	return 0;
}
