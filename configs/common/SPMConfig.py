# Copyright (c) 2012-2013, 2015 ARM Limited
# All rights reserved
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2010 Advanced Micro Devices, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Lisa Hsu

# Configure the M5 cache hierarchy config in one place
#

import m5
from m5.objects import *
from Caches import *

def config_cache(options, system):
    if options.external_memory_system and (options.caches or options.l2cache):
        print "External caches and internal caches are exclusive options.\n"
        sys.exit(1)

    if options.external_memory_system:
        ExternalCache = ExternalCacheFactory(options.external_memory_system)

    if options.cpu_type == "arm_detailed":
        try:
            from O3_ARM_v7a import *
        except:
            print "arm_detailed is unavailable. Did you compile the O3 model?"
            sys.exit(1)

        dcache_class, icache_class, l2_cache_class = \
            O3_ARM_v7a_DCache, O3_ARM_v7a_ICache, O3_ARM_v7aL2
    else:
        dcache_class, icache_class, l2_cache_class = \
            L1_DCache, L1_ICache, L2Cache

    # Set the cache line size of the system
    system.cache_line_size = options.cacheline_size

    if options.l2cache:
        # Provide a clock for the L2 and the L1-to-L2 bus here as they
        # are not connected using addTwoLevelCacheHierarchy. Use the
        # same clock as the CPUs.
        system.l2 = l2_cache_class(clk_domain=system.cpu_clk_domain,
                                   size=options.l2_size,
                                   assoc=options.l2_assoc)

        system.tol2bus = L2XBar(clk_domain = system.cpu_clk_domain)
        system.l2.cpu_side = system.tol2bus.master
        system.l2.mem_side = system.membus.slave

    if options.memchecker:
        system.memchecker = MemChecker()

    pmmu_nodes = []
    for i in xrange(options.num_cpus):
        if options.caches:
            responseFromSPM = MessageBuffer(ordered = True);
            responseToSPM = MessageBuffer(ordered = True);
            requestFromSPM = MessageBuffer(ordered = True);
            requestToSPM = MessageBuffer(ordered = False);
            responseToNetwork = MessageBuffer(ordered = True);
            requestToNetwork = MessageBuffer(ordered = True);
            
            requestToNet1 = MessageBuffer(ordered = True);
            requestToSPM1 = MessageBuffer(ordered = False);
            requestToNet2 = MessageBuffer(ordered = True);
            requestToSPM2 = MessageBuffer(ordered = False);
            requestToNet3 = MessageBuffer(ordered = True);
            requestToSPM3 = MessageBuffer(ordered = False);
            requestToNet4 = MessageBuffer(ordered = True);
            requestToSPM4 = MessageBuffer(ordered = False);
            requestToNet5 = MessageBuffer(ordered = True);
            requestToSPM5 = MessageBuffer(ordered = False);
            requestToNet6 = MessageBuffer(ordered = True);
            requestToSPM6 = MessageBuffer(ordered = False);
            requestToNet7 = MessageBuffer(ordered = True);
            requestToSPM7 = MessageBuffer(ordered = False);
            requestToNet8 = MessageBuffer(ordered = True);
            requestToSPM8 = MessageBuffer(ordered = False);
            requestToNet9 = MessageBuffer(ordered = True);
            requestToSPM9 = MessageBuffer(ordered = False);
            requestToNet10 = MessageBuffer(ordered = True);
            requestToSPM10 = MessageBuffer(ordered = False);
            requestToNet11 = MessageBuffer(ordered = True);
            requestToSPM11 = MessageBuffer(ordered = False);
            requestToNet12 = MessageBuffer(ordered = True);
            requestToSPM12 = MessageBuffer(ordered = False);
            requestToNet13 = MessageBuffer(ordered = True);
            requestToSPM13 = MessageBuffer(ordered = False);
            requestToNet14 = MessageBuffer(ordered = True);
            requestToSPM14 = MessageBuffer(ordered = False);
            requestToNet15 = MessageBuffer(ordered = True);
            requestToSPM15 = MessageBuffer(ordered = False);
            requestToNet16 = MessageBuffer(ordered = True);
            requestToSPM16 = MessageBuffer(ordered = False);
            requestToNet17 = MessageBuffer(ordered = True);
            requestToSPM17 = MessageBuffer(ordered = False);
            requestToNet18 = MessageBuffer(ordered = True);
            requestToSPM18 = MessageBuffer(ordered = False);
            requestToNet19 = MessageBuffer(ordered = True);
            requestToSPM19 = MessageBuffer(ordered = False);
            requestToNet20 = MessageBuffer(ordered = True);
            requestToSPM20 = MessageBuffer(ordered = False);
            requestToNet21 = MessageBuffer(ordered = True);
            requestToSPM21 = MessageBuffer(ordered = False);
            requestToNet22 = MessageBuffer(ordered = True);
            requestToSPM22 = MessageBuffer(ordered = False);
            requestToNet23 = MessageBuffer(ordered = True);
            requestToSPM23 = MessageBuffer(ordered = False);
            requestToNet24 = MessageBuffer(ordered = True);
            requestToSPM24 = MessageBuffer(ordered = False);
            requestToNet25 = MessageBuffer(ordered = True);
            requestToSPM25 = MessageBuffer(ordered = False);
            requestToNet26 = MessageBuffer(ordered = True);
            requestToSPM26 = MessageBuffer(ordered = False);
            requestToNet27 = MessageBuffer(ordered = True);
            requestToSPM27 = MessageBuffer(ordered = False);
            requestToNet28 = MessageBuffer(ordered = True);
            requestToSPM28 = MessageBuffer(ordered = False);
            requestToNet29 = MessageBuffer(ordered = True);
            requestToSPM29 = MessageBuffer(ordered = False);
            requestToNet30 = MessageBuffer(ordered = True);
            requestToSPM30 = MessageBuffer(ordered = False);
            requestToNet31 = MessageBuffer(ordered = True);
            requestToSPM31 = MessageBuffer(ordered = False);
            requestToNet32 = MessageBuffer(ordered = True);
            requestToSPM32 = MessageBuffer(ordered = False);
            
            

            pmmu = PMMU(version=i,
                        page_size_bytes=options.spm_page_size,
                        responseFromSPM=responseFromSPM,
                        responseToSPM=responseToSPM,
                        requestFromSPM=requestFromSPM,
                        requestToSPM=requestToSPM,
                        responseToNetwork=responseToNetwork,
                        requestToNetwork=requestToNetwork,
                        requestToNet1=requestToNet1,
                        requestToSPM1=requestToSPM1,
						requestToNet2=requestToNet2,
						requestToSPM2=requestToSPM2,
						requestToNet3=requestToNet3,
						requestToSPM3=requestToSPM3,
						requestToNet4=requestToNet4,
						requestToSPM4=requestToSPM4,
						requestToNet5=requestToNet5,
						requestToSPM5=requestToSPM5,
						requestToNet6=requestToNet6,
						requestToSPM6=requestToSPM6,
						requestToNet7=requestToNet7,
						requestToSPM7=requestToSPM7,
						requestToNet8=requestToNet8,
						requestToSPM8=requestToSPM8,
						requestToNet9=requestToNet9,
						requestToSPM9=requestToSPM9,
						requestToNet10=requestToNet10,
						requestToSPM10=requestToSPM10,
						requestToNet11=requestToNet11,
						requestToSPM11=requestToSPM11,
						requestToNet12=requestToNet12,
						requestToSPM12=requestToSPM12,
						requestToNet13=requestToNet13,
						requestToSPM13=requestToSPM13,
						requestToNet14=requestToNet14,
						requestToSPM14=requestToSPM14,
						requestToNet15=requestToNet15,
						requestToSPM15=requestToSPM15,
						requestToNet16=requestToNet16,
						requestToSPM16=requestToSPM16,
						requestToNet17=requestToNet17,
						requestToSPM17=requestToSPM17,
						requestToNet18=requestToNet18,
						requestToSPM18=requestToSPM18,
						requestToNet19=requestToNet19,
						requestToSPM19=requestToSPM19,
						requestToNet20=requestToNet20,
						requestToSPM20=requestToSPM20,
						requestToNet21=requestToNet21,
						requestToSPM21=requestToSPM21,
						requestToNet22=requestToNet22,
						requestToSPM22=requestToSPM22,
						requestToNet23=requestToNet23,
						requestToSPM23=requestToSPM23,
						requestToNet24=requestToNet24,
						requestToSPM24=requestToSPM24,
						requestToNet25=requestToNet25,
						requestToSPM25=requestToSPM25,
						requestToNet26=requestToNet26,
						requestToSPM26=requestToSPM26,
						requestToNet27=requestToNet27,
						requestToSPM27=requestToSPM27,
						requestToNet28=requestToNet28,
						requestToSPM28=requestToSPM28,
						requestToNet29=requestToNet29,
						requestToSPM29=requestToSPM29,
						requestToNet30=requestToNet30,
						requestToSPM30=requestToSPM30,
						requestToNet31=requestToNet31,
						requestToSPM31=requestToSPM31,
						requestToNet32=requestToNet32,
						requestToSPM32=requestToSPM32,              
                        governor = system.governor,
                        gov_type = options.gov_type,
			num_spms=options.num_spms)
            icache = icache_class(size=options.l1i_size,
                                  assoc=options.l1i_assoc)
            dcache = L1_DSPM(size=options.l1dspm_size)
            #                      pmmu=pmmu)

            system.cpu[i].pmmu = pmmu

            pmmu.spm_s_side = dcache.pmmu_m_side
            pmmu.spm_m_side = dcache.pmmu_s_side

            pmmu_nodes.append(pmmu)

            if options.memchecker:
                dcache_mon = MemCheckerMonitor(warn_only=True)
                dcache_real = dcache

                # Do not pass the memchecker into the constructor of
                # MemCheckerMonitor, as it would create a copy; we require
                # exactly one MemChecker instance.
                dcache_mon.memchecker = system.memchecker

                # Connect monitor
                dcache_mon.mem_side = dcache.cpu_side

                # Let CPU connect to monitors
                dcache = dcache_mon

            # When connecting the caches, the clock is also inherited
            # from the CPU in question
            if buildEnv['TARGET_ISA'] == 'x86':
                system.cpu[i].addPrivateSplitL1Caches(icache, dcache,
                                                      PageTableWalkerCache(),
                                                      PageTableWalkerCache())
            else:
                system.cpu[i].addPrivateSplitL1Caches(icache, dcache)

            if options.memchecker:
                # The mem_side ports of the caches haven't been connected yet.
                # Make sure connectAllPorts connects the right objects.
                system.cpu[i].dcache = dcache_real
                system.cpu[i].dcache_mon = dcache_mon

        elif options.external_memory_system:
            # These port names are presented to whatever 'external' system
            # gem5 is connecting to.  Its configuration will likely depend
            # on these names.  For simplicity, we would advise configuring
            # it to use this naming scheme; if this isn't possible, change
            # the names below.
            if buildEnv['TARGET_ISA'] in ['x86', 'arm']:
                system.cpu[i].addPrivateSplitL1Caches(
                        ExternalCache("cpu%d.icache" % i),
                        ExternalCache("cpu%d.dcache" % i),
                        ExternalCache("cpu%d.itb_walker_cache" % i),
                        ExternalCache("cpu%d.dtb_walker_cache" % i))
            else:
                system.cpu[i].addPrivateSplitL1Caches(
                        ExternalCache("cpu%d.icache" % i),
                        ExternalCache("cpu%d.dcache" % i))

        system.cpu[i].createInterruptController()
        if options.l2cache:
            system.cpu[i].connectAllPorts(system.tol2bus, system.membus)
        elif options.external_memory_system:
            system.cpu[i].connectUncachedPorts(system.membus)
        else:
            system.cpu[i].connectAllPorts(system.membus)

    return pmmu_nodes, system

# ExternalSlave provides a "port", but when that port connects to a cache,
# the connecting CPU SimObject wants to refer to its "cpu_side".
# The 'ExternalCache' class provides this adaptation by rewriting the name,
# eliminating distracting changes elsewhere in the config code.
class ExternalCache(ExternalSlave):
    def __getattr__(cls, attr):
        if (attr == "cpu_side"):
            attr = "port"
        return super(ExternalSlave, cls).__getattr__(attr)

    def __setattr__(cls, attr, value):
        if (attr == "cpu_side"):
            attr = "port"
        return super(ExternalSlave, cls).__setattr__(attr, value)

def ExternalCacheFactory(port_type):
    def make(name):
        return ExternalCache(port_data=name, port_type=port_type,
                             addr_ranges=[AllMemory])
    return make
