#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "uplink.h"

pthread_t threads[TASK_THREADS];

void initData()
{
	int i;
	for(i = 0; i < nmbSymbols; i++)
	{
		in[i].re = rand()%255;
		in[i].im = rand()%255;
	}
}

void* soft_demap_pthread(void *p) {
	int *temp = (int*)p;
	int id = *temp;

	int L;
	int i,k;
	int temp_real, temp_imag;
	int ind = 0;

	int scaling_factor = SCAL_FACT;
	int mod = MOD_64QAM; 
	int n = nmbSymbols;
	int num_threads = TASK_THREADS;

	int num_items = n/num_threads;
	int i_start = num_items * id;
	int i_end = num_items + i_start;

	//new code start
	//int num_tiles = 8;
	int num_tiles = 8;
	int num_items_per_tile = num_items / num_tiles;
	int ii, ii2;
	//new code end

	printf("in thd %d, n = %d, num_items = %d, num_threads = %d, i_start = %d i_end = %d\n", id, n, num_items, num_threads, i_start, i_end);

	switch (mod) {
		case MOD_64QAM:
		{

/*
			if( i_end > n)
				i_end = n;

			for (i= i_start; i < i_end; i++) {
				ind = i*2*6;
				for (k=0; k<6; k++) {
					L = k*4096;
					temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
					temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
					if (temp_real < 0)
						temp_real = 0;
					else if (temp_real > (L - 1))
						temp_real = (L - 1);
					if (temp_imag < 0)
						temp_imag = 0;
					else if (temp_imag > (L - 1))
						temp_imag = (L - 1);
					out_pth[ind++] = temp_real%255;
					out_pth[ind++] = temp_imag%255;
				}
			}
*/

			for(ii = 0; ii < num_tiles; ii++)
			{
				printf("thd = %d out_size = %d num_items %d start = %d\n", id, num_items_per_tile * NUM_SLOTS*MOD_64QAM * sizeof(char), num_items_per_tile, i_start + ii*num_items_per_tile*2*6);
				printf("thd = %d in_size = %d num_items %d start = %d\n", id,  num_items_per_tile * sizeof(complex), num_items_per_tile, i_start + ii*num_items_per_tile);

				// SPM_ARRAY_ALLOC (out_pth, num_items_per_tile * NUM_SLOTS * MOD_64QAM, char, COPY, MAX_IMPORTANCE, HIGH_PRIORITY);
				// SPM_ALLOC((unsigned long)in, (unsigned long)in + sizeof(complex) * num_items_per_tile, COPY, MAX_IMPORTANCE, HIGH_PRIORITY);
				//insert dma here
				for(ii2 = 0; ii2 < num_items_per_tile; ii2++)
				{
					i = i_start + ii*num_items_per_tile + ii2;
					ind = i*NUM_SLOTS*6; //i*2*6;

					for (k=0; k<6; k++) {
						L = k*4096;
						temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
						temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
						if (temp_real < 0)
							temp_real = 0;
						else if (temp_real > (L - 1))
							temp_real = (L - 1);
						if (temp_imag < 0)
							temp_imag = 0;
						else if (temp_imag > (L - 1))
							temp_imag = (L - 1);
						out_pth[ind++] = temp_real%255;
						out_pth[ind++] = temp_imag%255;
					}



				}
			}

		}
		break;
		default:
			printf("Modulation not supported: %i\n", mod);
	}
}

/*
void soft_demap(complex* in, int scaling_factor, int mod, int n, char* out) {
	int L;
	int i,k;

	switch (mod) {
		case MOD_64QAM:
		{
			int temp_real, temp_imag;
			int ind = 0;

			for (i=0; i<n; i++) {
				for (k=0; k<6; k++) {
					L = k*4096;
					temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
					temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
					if (temp_real < 0)
						temp_real = 0;
					else if (temp_real > (L - 1))
						temp_real = (L - 1);
					if (temp_imag < 0)
						temp_imag = 0;
					else if (temp_imag > (L - 1))
						temp_imag = (L - 1);
					out[ind++] = temp_real%255;
					out[ind++] = temp_imag%255;
				}
			}
			printf("i = %d, ind = %d, n = %d\n", i, ind, n);
		}
		break;
		default:
			printf("Modulation not supported: %i\n", mod);
	}
}
*/

int main(int argc, char *argv[]) {
	int rc = 0;
	int i  = 0;
	int args[TASK_THREADS];

	initData();
	printf("Done init\n");
	//soft_demap(in, SCAL_FACT, MOD_64QAM, nmbSymbols, out);

	//Start a number of worker threads
	for (i=1; i<TASK_THREADS; i++) {
		args[i] = i;
		pthread_create(&threads[i], NULL, soft_demap_pthread, (void*) (args + i) );
	}
	args[0] = 0;
	soft_demap_pthread((void*) (args + 0));
	/*
        for(i=0; i < 12*nmbSymbols; i++)
	{
		if(out[i] != out_pth[i])
			printf("error in pth\n");
		//printf("s %#x p %#x e %#x\n", out[i], out_pth[i], out_epi[i]);
	}
	*/
	return 0;
}
