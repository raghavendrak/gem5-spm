#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "uplink.h"
#include <spm_api_1.h>
#ifdef __arm__
#define DUMP
#endif
#ifdef DUMP
#include <m5op.h>
#endif

pthread_t threads[TASK_THREADS];
int g_x;

void initData()
{
	int i;
	for(i = 0; i < nmbSymbols; i++)
	{
		in[i].re = rand()%255;
		in[i].im = rand()%255;
	}
}

void* soft_demap_pthread(void *p) {
	int *dummy;
	//spm_allocate((unsigned long)&dummy, (unsigned long)(0x0), 1023, ALLOC_STACK); // When using STACK, the second address is dummy
	int *temp = (int*)p;
	int id = *temp;
	// spm_allocate((unsigned long)((unsigned long)&dummy - 100), (unsigned long)(0x0), 1023, ALLOC_STACK); // When using STACK, the second address is dummy
	// spm_allocate((unsigned long)((unsigned long)&dummy - 100), (unsigned long)(0x0), 1023, ALLOC_STACK); // When using STACK, the second address is dummy
	// printf ("in thread: id = %d startVAAddr = %x startSPMAddr = %x\n", id, ((unsigned long)&dummy), (unsigned long)(local_spm[id]));

	spm_allocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, COPY_MARK);

	//spm_allocate(((unsigned long)0x9c4c), (unsigned long)(local_spm[id] + 30830), 12, COPY_MARK);
	printf ("Done init id: %d\n", id);

	int L;
	int i,k;
	int temp_real, temp_imag;
	int ind = 0;

	int scaling_factor = SCAL_FACT;
	int mod = MOD_64QAM; 
	int n = nmbSymbols;
	int num_threads = TASK_THREADS;

	int num_items = n/num_threads;
	int i_start = num_items * id;
	int i_end = num_items + i_start;

	//new code start
	//int num_tiles = 8;
	int num_tiles = 8;
	int num_items_per_tile = num_items / num_tiles;
	int ii, ii2;
	//new code end

	//printf("in thd %d, n = %d, num_items = %d, num_threads = %d, i_start = %d i_end = %d\n", id, n, num_items, num_threads, i_start, i_end);
	//printf("Address of k: %x temp_real: %x temp_imag: %x in: %x out: %x\n", &k, &temp_real, &temp_imag, &in, &out_pth);

	switch (mod) {
		case MOD_64QAM:
		{

/*
			if( i_end > n)
				i_end = n;

			for (i= i_start; i < i_end; i++) {
				ind = i*2*6;
				for (k=0; k<6; k++) {
					L = k*4096;
					temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
					temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
					if (temp_real < 0)
						temp_real = 0;
					else if (temp_real > (L - 1))
						temp_real = (L - 1);
					if (temp_imag < 0)
						temp_imag = 0;
					else if (temp_imag > (L - 1))
						temp_imag = (L - 1);
					out_pth[ind++] = temp_real%255;
					out_pth[ind++] = temp_imag%255;
				}
			}
*/

			for(ii = 0; ii < num_tiles; ii++)
			{
				printf("thd = %d out_size = %d num_items %d start = %d\n", id, num_items_per_tile * NUM_SLOTS*MOD_64QAM * sizeof(char), num_items_per_tile, i_start + ii*num_items_per_tile*2*6);
				printf("thd = %d in_size = %d num_items %d start = %d\n", id,  num_items_per_tile * sizeof(complex), num_items_per_tile, i_start + ii*num_items_per_tile);

				spm_allocate((unsigned long)(in + i_start + ii*num_items_per_tile), (unsigned long)(local_spm[id]), num_items_per_tile * sizeof(complex), COPY);
				spm_allocate((unsigned long)(out_pth + (i_start + ii*num_items_per_tile) * (NUM_SLOTS*6)), (unsigned long)(local_spm[id] + num_items_per_tile * sizeof(complex)), num_items_per_tile * NUM_SLOTS*MOD_64QAM * sizeof(char), UNINITIALIZE);
				complex *in_spm;
				in_spm = (complex *)local_spm[id];
				//printf("id: %d Address in_spm: %x\n", id, &in_spm);
				//a_spm = (int *) malloc(sizeof(int) * 10);
				//spm_allocate((unsigned long)a_spm, (unsigned long)(local_spm[id] + num_items_per_tile * sizeof(complex)), num_items_per_tile * NUM_SLOTS*MOD_64QAM * sizeof(char), COPY);
				char *out_spm;
				out_spm = (char *)(local_spm[id] + num_items_per_tile * sizeof(complex));
				int x = 0;
				for(ii2 = 0; ii2 < num_items_per_tile; ii2++)
				{
					i = i_start + ii*num_items_per_tile + ii2;
					ind = i*NUM_SLOTS*6; //i*2*6;

					for (k=0; k<6; k++) {
						L = k*4096;
						temp_real = (int)(((L - 1) - ((in_spm[ii2].re * scaling_factor)>>8) + (L - 1)) / 2.0);
						temp_imag = (int)(((L - 1) - ((in_spm[ii2].im * scaling_factor)>>8) + (L - 1)) / 2.0);
						if (temp_real < 0)
							temp_real = 0;
						else if (temp_real > (L - 1))
							temp_real = (L - 1);
						if (temp_imag < 0)
							temp_imag = 0;
						else if (temp_imag > (L - 1))
							temp_imag = (L - 1);

						//out_pth[ind++] = temp_real%255;
						//out_pth[ind++] = temp_imag%255;

						out_spm[x++] = temp_real%255;
						out_spm[x++] = temp_imag%255;
					}
				}
				spm_deallocate((unsigned long)(out_pth + (i_start + ii*num_items_per_tile) * (NUM_SLOTS*6)), (unsigned long)(local_spm[id] + num_items_per_tile * sizeof(complex)), num_items_per_tile * NUM_SLOTS*MOD_64QAM * sizeof(char), WRITE_BACK);
			}

		}
		break;
		default:
			printf("Modulation not supported: %i\n", mod);
	}
	//printf ("Done computation id: %d\n", id);
	spm_deallocate(((unsigned long)&dummy - 100), (unsigned long)(local_spm[id] + 30720), 100, WRITE_BACK_MARK);
	//spm_deallocate(((unsigned long)0x9c4c), (unsigned long)(local_spm[id] + 30830), 12, WRITE_BACK_MARK);
	//spm_deallocate((unsigned long)(0x0), (unsigned long)(0x0), 1023, DEALLOC_STACK_POP);
	pthread_barrier_wait(&barr);
}

int* allocTest()
{
	//int *x;
	//x = (int *) malloc (sizeof(int) * 16);
	//return x;
	return (malloc (sizeof(int) * 16));
}

/*
void soft_demap(complex* in, int scaling_factor, int mod, int n, char* out) {
	int L;
	int i,k;

	switch (mod) {
		case MOD_64QAM:
		{
			int temp_real, temp_imag;
			int ind = 0;

			for (i=0; i<n; i++) {
				for (k=0; k<6; k++) {
					L = k*4096;
					temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
					temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
					if (temp_real < 0)
						temp_real = 0;
					else if (temp_real > (L - 1))
						temp_real = (L - 1);
					if (temp_imag < 0)
						temp_imag = 0;
					else if (temp_imag > (L - 1))
						temp_imag = (L - 1);
					out[ind++] = temp_real%255;
					out[ind++] = temp_imag%255;
				}
			}
			printf("i = %d, ind = %d, n = %d\n", i, ind, n);
		}
		break;
		default:
			printf("Modulation not supported: %i\n", mod);
	}
}
*/

int main(int argc, char *argv[]) {
	spm_initialize(TASK_THREADS);
	int rc = 0;
	int i  = 0;
	int args[TASK_THREADS];
	pthread_barrier_init(&barr, NULL, TASK_THREADS);
	int *y;

	// initData();
	//printf("Done init\n");
	//soft_demap(in, SCAL_FACT, MOD_64QAM, nmbSymbols, out);

	//Start a number of worker threads
	// printf("Addr local_spm: %x 0: %x\n", &local_spm, &local_spm[0]);
	y = allocTest();
	// y[0] = 1;
	//free (y);
#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif
	for (i=1; i<TASK_THREADS; i++) {
		args[i] = i;
		pthread_create(&threads[i], NULL, soft_demap_pthread, (void*) (args + i) );
	}
	args[0] = 0;
	soft_demap_pthread((void*) (args + 0));
#ifdef DUMP
        m5_dump_stats(0, 0);
        m5_reset_stats(0, 0);
#endif
	/*
        for(i=0; i < 12*nmbSymbols; i++)
	{
		if(out[i] != out_pth[i])
			printf("error in pth\n");
		//printf("s %#x p %#x e %#x\n", out[i], out_pth[i], out_epi[i]);
	}
	*/
    /*
    i = 32768 * 32768;
    while (i--);
    */
	return 0;
}
