#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include "complex_def.h"

#define OFDM_IN_SLOT 7
//#define MAX_LAYERS 2
#define MAX_LAYERS 4
#define MAX_SC 1200
#define MOD_64QAM 6
#define MAX_RX 4
#define TASK_THREADS MAX_LAYERS*NUM_SYMBOLS
#define SCAL_FACT -978046001
#define MOD_64QAM 6
// #define nmbSymbols 28800
#define nmbSymbols 28800
//#define NUM_SYMBOLS 6
#define NUM_SYMBOLS 4
#define pth_size 691200
#define NUM_SLOTS 2

complex in[nmbSymbols];
//char out[nmbSymbols*NUM_SLOTS*NUM_SYMBOLS];
char out_pth[nmbSymbols*NUM_SLOTS*MOD_64QAM];
void* soft_demap_pthread(void *p);
char a_spm[10];
pthread_barrier_t barr;
