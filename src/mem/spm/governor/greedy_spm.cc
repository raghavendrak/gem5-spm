#include "mem/spm/governor/greedy_spm.hh"

#include <iostream>

GreedySPM *
GreedySPMParams::create()
{
    return new GreedySPM(this);
}

GreedySPM::GreedySPM(const Params *p)
    : RandomSPM(p)
{
    gov_type = "GreedySPM";
    for (int i = 0; i < MAX_THREADS; i++) {
    	sendSPMAddr[i] = false;
    	call_count[i] = 0;
    }
}

GreedySPM::~GreedySPM()
{

}

void
GreedySPM::init()
{

}

/*
Example of (col, row) ordering:
(2,0)  (1,0)  (0,0)
(2,1)  (1,1)  (0,1)
(2,2)  (1,2)  (0,2)
*/
void
GreedySPM::addPMMU(PMMU *p)
{
    vector<PMMU *>::iterator it = pmmus.begin();
    pmmus.insert(it,p);

    int col = p->getMachineID().num % num_column;
    int row = p->getMachineID().num / num_column;
    assert (coord_to_pmmu.find(make_pair(col,row)) == coord_to_pmmu.end());
    coord_to_pmmu[make_pair(col,row)] = p;
    assert (pmmu_to_coord.find(p) == pmmu_to_coord.end());
    pmmu_to_coord[p] = make_pair(col,row);
}

int
GreedySPM::findHopDistance (pair <int, int> pmmu_o, pair <int, int> pmmu_h)
{
    return abs(pmmu_o.first - pmmu_h.first) + abs(pmmu_o.second - pmmu_h.second);
}

void
GreedySPM::distanceBasedSort(pair <int, int> central_pmmu)
{
    bool have_swapped = true;
    for (unsigned j = 1; have_swapped && j < pmmus.size(); ++j) {
        have_swapped = false;
        for (unsigned i = 0; i < pmmus.size() - j; ++i) {
            if (findHopDistance(pmmu_to_coord.at(pmmus[i]),central_pmmu) >
                findHopDistance(pmmu_to_coord.at(pmmus[i+1]),central_pmmu)) {
                have_swapped = true;
                swap(pmmus[i], pmmus[i + 1]);
            }
        }
    }
}

int
GreedySPM::allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                    AllocationModes alloc_mode,
                    DataImportance data_importance,
                    ThreadPriority app_priority)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *requester_pmmu = spm->myPMMU;
    PMMU *host_pmmu = NULL;
    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

    // TODO: page alignment needs to be fixed (inclusive or exclusive?)
    const int page_size = requester_pmmu->getPageSizeBytes();
    Addr aligned_start_addr = requester_pmmu->spmPageAlignUP(start_addr);
    Addr aligned_end_addr = requester_pmmu->spmPageAlignDown(end_addr);
    const Addr addr_range = aligned_end_addr - aligned_start_addr;
    const int total_num_pages = CEIL_DIV(addr_range, page_size);
    int remaining_pages = total_num_pages;

    distanceBasedSort(pmmu_to_coord.at(requester_pmmu));
    assert(requester_pmmu == pmmus[0]);

    vector<PMMU *>::size_type pmmu_it = 0;
    while (remaining_pages > 0 && pmmu_it != pmmus.size()) {
        host_pmmu = pmmus[pmmu_it];
        remaining_pages -= allocation_helper_on_free_pages(requester_pmmu,
                                                           requester_pt,
                                                           aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                                           remaining_pages,
                                                           alloc_mode,
                                                           data_importance,
                                                           app_priority,
                                                           host_pmmu);

        pmmu_it++;
    }
    return total_num_pages - remaining_pages;
}

int
GreedySPM::allocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params)
{
	unsigned id = tc->contextId();
	call_count[id]++;
	if (!sendSPMAddr[id]) {
		assert (call_count[id] == 1);
		// VA and size sent
		startVAAddr[id] = start_addr;
		alloc_size[id] = params;
		endVAAddr[id] = start_addr + alloc_size[id] - 1;
		sendSPMAddr[id] = true;
	}
	else {
		assert (call_count[id] == 2);
		call_count[id] = 0;
		startSPMAddr[id] = start_addr;
		endSPMAddr[id] = start_addr + alloc_size[id] - 1;
		sendSPMAddr[id] = false;


	    BaseCPU *cpu = tc->getCpuPtr();
	    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
	    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
	    PMMU *pmmu = spm->myPMMU;
	    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

	    if (static_cast<AllocationModes>(params) == ALLOC_STACK) {
	    	pmmu->trigger_spm_stack_allocate(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), alloc_size[id], params);
	    }
	    else if (static_cast<AllocationModes>(params) == SPM_MEM_COPY) {
	    	// startVAAddr = spm_src; startSPMAddr = spm_dest
	    	pmmu->trigger_spm_copy(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), alloc_size[id], params);
	    }
	    else {
	    	// printf ("id = %d, startVAAddr[id] = %x, endVAAddr[id] = %x, startSPMAddr[id] = %x, endSPMAddr[id] = %x\n", id, startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id]);
	    	pmmu->trigger_spm_allocate_dma(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), alloc_size[id], params); // Second call will have alloc_mode in params
	    }
	}


	return 0;
}

int
GreedySPM::deAllocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params)
{
	unsigned id = tc->contextId();
	call_count[id]++;
	if (!sendSPMAddr[id]) {
		assert (call_count[id] == 1);
		// VA and size sent
		startVAAddr[id] = start_addr;
		dealloc_size[id] = params;
		endVAAddr[id] = start_addr + dealloc_size[id] - 1;
		sendSPMAddr[id] = true;
	}
	else {
		assert (call_count[id] == 2);
		call_count[id] = 0;
		startSPMAddr[id] = start_addr;
		endSPMAddr[id] = start_addr + dealloc_size[id] - 1;
		sendSPMAddr[id] = false;


	    BaseCPU *cpu = tc->getCpuPtr();
	    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
	    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
	    PMMU *pmmu = spm->myPMMU;
	    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

	    if (static_cast<DeallocationModes>(params) == DEALLOC_STACK) {
	    	pmmu->trigger_spm_stack_deallocate(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), dealloc_size[id], 0); // Second call will have alloc_mode in params
	    }
	    else if (static_cast<DeallocationModes>(params) == DEALLOC_STACK_POP) {
	    	pmmu->trigger_spm_stack_deallocate(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), dealloc_size[id], 1); // Second call will have alloc_mode in params
	    }
	    else {
	    	pmmu->trigger_spm_deallocate_dma(startVAAddr[id], endVAAddr[id], startSPMAddr[id], endSPMAddr[id], pmmu->getMachineID(), requester_pt,
	    			pmmu->getNodeID(), dealloc_size[id], params); // Second call will have alloc_mode in params
	    }
	}
	return 0;
}

int
GreedySPM::blockEpiphany(ThreadContext *tc, uint64_t block_cycles, uint64_t block_mode)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *pmmu = spm->myPMMU;
    pmmu->blockEpiphany(block_cycles, block_mode);
	return 0;
}
