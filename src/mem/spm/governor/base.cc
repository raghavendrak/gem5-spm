#include "mem/spm/governor/base.hh"
#include "mem/spm/governor/local_spm.hh"
#include "mem/spm/governor/random_spm.hh"
#include "mem/spm/governor/greedy_spm.hh"
#include "mem/spm/governor/guaranteed_greedy_spm.hh"

#include <iostream>
using namespace std;

BaseGovernor *
BaseGovernorParams::create()
{
    if (gov_type.compare("Local") == 0)
        return new LocalSPM(this);
    else if (gov_type.compare("Random") == 0)
        return new RandomSPM(this);
    else if (gov_type.compare("Greedy") == 0)
        return new GreedySPM(this);
    else if (gov_type.compare("GuaranteedGreedy") == 0)
        return new GuaranteedGreedySPM(this);
    else
        panic ("undefined governor type");
}

BaseGovernor::BaseGovernor(const Params *p)
    : SimObject(p), num_column(p->col), num_row(p->row), gov_type("BaseGovernor")
{

}

void
BaseGovernor::getMaxContiguousFreePages(PMMU *host_pmmu, int *max_num_pages_found, Addr *base_free_pages)
{
    getMaxContiguousFreePages(host_pmmu, host_pmmu->getSPMSizePages(), 0, max_num_pages_found, base_free_pages);
}

void
BaseGovernor::getMaxContiguousFreePages(PMMU *host_pmmu, int max_num_pages_needed, int start_page_index,
                                        int *max_num_pages_found, Addr *base_free_pages_found)
{
    int ctr;
    int current_max = 0;
    *max_num_pages_found = 0;
    Addr current_base_found = Addr(0);
    *base_free_pages_found = Addr(0);

    for (ctr = start_page_index; ctr < host_pmmu->getSPMSizePages(); ctr++) {
        if (!host_pmmu->my_spm_ptr->spmSlots[ctr].isFree()){
            current_max = 0;
            current_base_found = Addr((ctr + 1) * host_pmmu->getPageSizeBytes());
        } else {
            current_max++;
        }

        if (current_max > *max_num_pages_found) {
            *max_num_pages_found = current_max;
            *base_free_pages_found = current_base_found;
        }

        if (*max_num_pages_found >= max_num_pages_needed)
            break;
    }
}

int
BaseGovernor::allocation_helper_on_free_pages(PMMU *requester_pmmu,
                                              FuncPageTable *requester_pt,
                                              uint64_t aligned_start_addr,
                                              int total_num_pages,
                                              AllocationModes alloc_mode,
                                              DataImportance data_importance,
                                              ThreadPriority app_priority,
                                              PMMU *host_pmmu)
{
    const int page_size = requester_pmmu->getPageSizeBytes();
    int remaining_pages = total_num_pages;

    int num_pages_this_iteration = 0;
    Addr base_free_pages = 0;
    int max_free_pages = 0;
    int page_counter = 0;

    while (remaining_pages > 0) {

        base_free_pages = 0;
        max_free_pages = 0;
        getMaxContiguousFreePages(host_pmmu, &max_free_pages, &base_free_pages);
        num_pages_this_iteration = MIN(remaining_pages, max_free_pages);

        if (num_pages_this_iteration > 0) {
        	DPRINTF(GOV, "Allocating page, aligned_start_addr: %x\n", (aligned_start_addr + page_size*page_counter));
            int num_added_pages = requester_pmmu->addATTMappingsVAddress(aligned_start_addr + page_size*page_counter,
                                                                         host_pmmu->getMachineID(),
                                                                         base_free_pages,
                                                                         num_pages_this_iteration,
                                                                         requester_pt,
                                                                         alloc_mode);
            host_pmmu->setUsedPages(base_free_pages, num_added_pages,
                                    data_importance, app_priority,
                                    requester_pmmu->getNodeID());


            DPRINTF(GOV, "%s: Allocating %d/%d SPM slot(s) for node (%d, %d) on node (%d, %d)\n",
                    gov_type,
                    num_added_pages, num_pages_this_iteration,
                    requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column,
                    host_pmmu->getMachineID().num / num_column, host_pmmu->getMachineID().num % num_column);

            page_counter += num_pages_this_iteration;
            remaining_pages -= num_pages_this_iteration;
        }
        else {
            // no more space on this spm OR all pages are already mapped on chip
            DPRINTF(GOV, "%s: Couldn't allocate %d SPM slot(s) for node (%d, %d) on node (%d, %d)\n",
                    gov_type,
                    remaining_pages,
                    requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column,
                    host_pmmu->getMachineID().num / num_column, host_pmmu->getMachineID().num % num_column);
            break;
        }
    }
    return total_num_pages - remaining_pages;
}

int
BaseGovernor::allocation_helper_on_occupied_pages(PMMU *requester_pmmu,
                                                  FuncPageTable *requester_pt,
                                                  uint64_t aligned_start_addr,
                                                  int total_num_pages,
                                                  AllocationModes alloc_mode,
                                                  DataImportance data_importance,
                                                  ThreadPriority app_priority,
                                                  PMMU *host_pmmu,
                                                  Addr base_free_pages,
                                                  PMMU *user_pmmu)
{
    if (total_num_pages > 0) {
        int num_added_pages = requester_pmmu->addATTMappingsVAddress(aligned_start_addr,
                                                                     host_pmmu->getMachineID(),
                                                                     base_free_pages,
                                                                     total_num_pages,
                                                                     requester_pt,
                                                                     alloc_mode,
                                                                     true,
                                                                     user_pmmu->getNodeID());
        host_pmmu->setUsedPages(base_free_pages, num_added_pages,
                                data_importance, app_priority,
                                requester_pmmu->getNodeID());

        DPRINTF(GOV, "%s: Allocating %d/%d SPM slot(s) for node (%d, %d) on node (%d, %d)\n",
                gov_type,
                num_added_pages, total_num_pages,
                requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column,
                host_pmmu->getMachineID().num / num_column, host_pmmu->getMachineID().num % num_column);

    }
    return total_num_pages;
}

int
BaseGovernor::dallocation_helper_virtual_address(PMMU *requester_pmmu,
                                                 FuncPageTable *requester_pt,
                                                 uint64_t aligned_start_addr,
                                                 int total_num_pages,
                                                 DeallocationModes dealloc_mode,
                                                 PMMU *user_pmmu)
{
    int num_removed_pages = requester_pmmu->removeATTMappingsVAddress(aligned_start_addr,
                                                                      total_num_pages,
                                                                      requester_pt,
                                                                      dealloc_mode);

    DPRINTF(GOV, "%s: Deallocating %d/%d SPM slot(s) for node (%d, %d)\n",
            gov_type,
            num_removed_pages, total_num_pages,
            requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column);

    return num_removed_pages;
}

int
BaseGovernor::dallocation_helper_spm_address(PMMU *current_user_pmmu,
                                             FuncPageTable *current_user_pt,
                                             PMMU *host_pmmu,
                                             uint64_t start_spm_p_addr,
                                             int total_num_pages,
                                             DeallocationModes dealloc_mode,
                                             PMMU *future_user_pmmu)
{

    current_user_pmmu->removeATTMappingsSPMAddress(host_pmmu->getMachineID(),
                                                   start_spm_p_addr,
                                                   total_num_pages,
                                                   current_user_pt,
                                                   dealloc_mode);

    DPRINTF(GOV, "%s: Evicting %d SPM slot(s) on node (%d, %d) used by node (%d, %d)\n",
            gov_type,
            total_num_pages,
            host_pmmu->getMachineID().num / num_column, host_pmmu->getMachineID().num % num_column,
            current_user_pmmu->getMachineID().num / num_column, current_user_pmmu->getMachineID().num % num_column);

    return total_num_pages;
}

void
BaseGovernor::relocation_helper_virtual_address(PMMU *user_pmmu,
                                                Addr aligned_start_addr,
                                                int total_num_pages,
                                                PMMU *future_host_pmmu,
                                                Addr future_start_spm_p_addr)
{

}

void
BaseGovernor::relocation_helper_spm_address(PMMU *user_pmmu,
                                            PMMU *current_host_pmmu,
                                            Addr current_start_spm_p_addr,
                                            int total_num_pages,
                                            PMMU *future_host_pmmu,
                                            Addr future_start_spm_p_addr)
{

    user_pmmu->changeATTMappingSPMAddress(current_host_pmmu->getMachineID(),
                                          current_start_spm_p_addr,
                                          total_num_pages,
                                          future_host_pmmu->getMachineID(),
                                          future_start_spm_p_addr);

    future_host_pmmu->setUsedPages(future_start_spm_p_addr, total_num_pages,
                                   DataImportance::MAX_IMPORTANCE, //TODO: fix us
                                   ThreadPriority::HIGH_PRIORITY,
                                   user_pmmu->getNodeID());

    DPRINTF(GOV, "%s: Relocating %d SPM slot(s) belong to node (%d, %d) from node (%d, %d) to node (%d, %d)\n",
            gov_type,
            total_num_pages,
            user_pmmu->getMachineID().num / num_column, user_pmmu->getMachineID().num % num_column,
            current_host_pmmu->getMachineID().num / num_column, current_host_pmmu->getMachineID().num % num_column,
            future_host_pmmu->getMachineID().num / num_column, future_host_pmmu->getMachineID().num % num_column);

}
