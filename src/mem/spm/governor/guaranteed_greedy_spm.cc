#include "mem/spm/governor/guaranteed_greedy_spm.hh"

#include <iostream>

GuaranteedGreedySPM *
GuaranteedGreedySPMParams::create()
{
    return new GuaranteedGreedySPM(this);
}

GuaranteedGreedySPM::GuaranteedGreedySPM(const Params *p)
    : GreedySPM(p)
{
    gov_type = "GuaranteedGreedySPM";
    local_share = p->local_share;
    guest_slot_selection_policy = p->guest_slot_selection_policy;
    guest_slot_relocation_policy = p->guest_slot_relocation_policy;

}

GuaranteedGreedySPM::~GuaranteedGreedySPM()
{

}

void
GuaranteedGreedySPM::init()
{

}

/* What percentage of owner_pmmu's spm is used by user_pmmu? */
float
GuaranteedGreedySPM::spmShare(PMMU *owner_pmmu, PMMU *user_pmmu)
{
    int num_slots = owner_pmmu->getSPMSizePages();

    int slot_idx;
    int slots_used = 0;

    for (slot_idx = 0; slot_idx < num_slots; slot_idx++) {
        if (!owner_pmmu->my_spm_ptr->spmSlots[slot_idx].isFree()&&
            owner_pmmu->my_spm_ptr->spmSlots[slot_idx].getOwner() == user_pmmu->getNodeID())
            slots_used++;
    }
    return float(slots_used)/float(num_slots);
}

int
GuaranteedGreedySPM::findLeastIndexedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt)
{
    int candidate_slot_idx = 0;
    while (owner_pmmu->my_spm_ptr->spmSlots[candidate_slot_idx].getOwner() == owner_pmmu->getNodeID())
        candidate_slot_idx++;

    assert(candidate_slot_idx < owner_pmmu->getSPMSizePages());

    return candidate_slot_idx;
}

int
GuaranteedGreedySPM::findLeastRecentlyUsedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt)
{
    int candidate_slot_idx = 0;
    Tick least_recent_tick = UINT64_MAX;
    for (int i=0; i<owner_pmmu->getSPMSizePages(); i++) {
        if (!(owner_pmmu->my_spm_ptr->spmSlots[i].getOwner() == owner_pmmu->getNodeID())) {
            if (owner_pmmu->my_spm_ptr->spmSlots[i].getLastTickAccessed() < least_recent_tick) {
                least_recent_tick = owner_pmmu->my_spm_ptr->spmSlots[i].getLastTickAccessed();
                candidate_slot_idx = i;
            }
        }
    }
    return candidate_slot_idx;
}

int
GuaranteedGreedySPM::findLeastFrequentlyUsedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt)
{
    int candidate_slot_idx = 0;
    uint64_t least_access_count = UINT64_MAX;
    for (int i=0; i<owner_pmmu->getSPMSizePages(); i++) {
        if (!(owner_pmmu->my_spm_ptr->spmSlots[i].getOwner() == owner_pmmu->getNodeID())) {
            if (owner_pmmu->my_spm_ptr->spmSlots[i].getRefCount() < least_access_count) {
                least_access_count = owner_pmmu->my_spm_ptr->spmSlots[i].getRefCount();
                candidate_slot_idx = i;
            }
        }
    }
    return candidate_slot_idx;
}

int
GuaranteedGreedySPM::findGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt, string selection_policy)
{
    int candidate_slot_idx = -1;

    if (selection_policy.compare("LeastIndexed") == 0) {
        candidate_slot_idx = findLeastIndexedGuestSlotToRelocate(owner_pmmu, user_pmmu, user_pt);
    } else if (selection_policy.compare("LeastRecentlyUsed") == 0) {
        candidate_slot_idx = findLeastRecentlyUsedGuestSlotToRelocate(owner_pmmu, user_pmmu, user_pt);
    } else if (selection_policy.compare("LeastFrequentlyUsed") == 0) {
        candidate_slot_idx = findLeastFrequentlyUsedGuestSlotToRelocate(owner_pmmu, user_pmmu, user_pt);
    } else {
        panic("%s selection policy is not implemented", selection_policy);
    }

    NodeID user_id = owner_pmmu->my_spm_ptr->spmSlots[candidate_slot_idx].getOwner();
    int user_col = user_id % num_column;
    int user_row = user_id / num_column;
    *user_pmmu  = coord_to_pmmu[make_pair(user_col,user_row)];
    BaseCPU *user_cpu = dynamic_cast<BaseCPU*>(((*user_pmmu)->my_spm_ptr->getSlavePort("cpu_side", 0).getMasterPort()).getOwner());
    *user_pt = dynamic_cast<FuncPageTable*>(user_cpu->getContext(0)->getProcessPtr()->pTable);

    return candidate_slot_idx;
}

bool
GuaranteedGreedySPM::findClosestFreeSlots(PMMU *requester_pmmu,
                                          int total_num_pages,
                                          PMMU **host_pmmu,
                                          Addr *base_free_pages)
{
    PMMU *previous_center = pmmus[0];
    bool found = false;

    distanceBasedSort(pmmu_to_coord.at(requester_pmmu));
    assert(requester_pmmu == pmmus[0]);

    vector<PMMU *>::size_type pmmu_it = 0;
    while (pmmu_it != pmmus.size()) {
        *host_pmmu = pmmus[pmmu_it];

        int max_free_pages = 0;
        getMaxContiguousFreePages(*host_pmmu, &max_free_pages, base_free_pages);

        DPRINTF(GOV, "%s: Examining node (%d , %d) for free slots to be used by (%d , %d). Found = %d\n",
                gov_type,
                (*host_pmmu)->getMachineID().num / num_column, (*host_pmmu)->getMachineID().num % num_column,
                requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column,
                max_free_pages);

        if (max_free_pages >= total_num_pages) {
          found = true;
          break;
        }

        pmmu_it++;
    }

    host_pmmu = nullptr;
    distanceBasedSort(pmmu_to_coord.at(previous_center));

    return found;
}

void
GuaranteedGreedySPM::relocateSlots(PMMU *user_pmmu,
                                   FuncPageTable *user_pt,
                                   PMMU *current_host_pmmu,
                                   Addr current_start_spm_p_addr,
                                   int total_num_pages,
                                   string relocation_policy)
{
    // relocate the slot
    // option 1:  (relocate == evict)
    if (relocation_policy.compare("MoveOffChip") == 0) {

        dallocation_helper_spm_address(user_pmmu,
                                       user_pt,
                                       current_host_pmmu,
                                       current_start_spm_p_addr,
                                       total_num_pages,
                                       WRITE_BACK,
                                       current_host_pmmu);

    } else if (relocation_policy.compare("ClosestFreeSlot") == 0) {

        PMMU *future_host_pmmu;
        Addr future_base_address;
        // try relocating to a close free slot
        if (findClosestFreeSlots(user_pmmu, total_num_pages, &future_host_pmmu, &future_base_address)) {
            relocation_helper_spm_address(user_pmmu,
                                          current_host_pmmu,
                                          current_start_spm_p_addr,
                                          1,
                                          future_host_pmmu,
                                          future_base_address);
        }
        else { // if not available, deallocate
            dallocation_helper_spm_address(user_pmmu,
                                           user_pt,
                                           current_host_pmmu,
                                           current_start_spm_p_addr,
                                           total_num_pages,
                                           WRITE_BACK,
                                           current_host_pmmu);
        }

    } else {
        panic("%s relocation policy is not implemented", relocation_policy);
    }
}

int
GuaranteedGreedySPM::allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                              AllocationModes alloc_mode,
                              DataImportance data_importance,
                              ThreadPriority app_priority)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *requester_pmmu = spm->myPMMU;
//  PMMU *host_pmmu = NULL;
    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

    // TODO: page alignment needs to be fixed (inclusive or exclusive?)
    const int page_size = requester_pmmu->getPageSizeBytes();
    Addr aligned_start_addr = requester_pmmu->spmPageAlignUP(start_addr);
    Addr aligned_end_addr = requester_pmmu->spmPageAlignDown(end_addr);
    const Addr addr_range = aligned_end_addr - aligned_start_addr;
    const int total_num_pages = CEIL_DIV(addr_range, page_size);
    int remaining_pages = total_num_pages;

    // 1. Allocate on the local SPM first
    remaining_pages -= LocalSPM::allocate(tc,
                                          aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                          end_addr,
                                          alloc_mode,
                                          data_importance,
                                          app_priority);

    // 2. Check if the requester_pmmu has NOT used its minimum share of its local SPM yet
    float current_local_spm_share = spmShare(requester_pmmu, requester_pmmu);
    while (remaining_pages > 0 && (current_local_spm_share < local_share)) {
        DPRINTF(GOV, "%s: Current local share for node (%d , %d) = %f\n",
                      gov_type,
                      requester_pmmu->getMachineID().num / num_column, requester_pmmu->getMachineID().num % num_column,
                      current_local_spm_share);

        // find a slot to be relocated (TODO: for now we select the least indexed slot that belongs to a different node)
        PMMU *user_pmmu = NULL;
        FuncPageTable *user_pt = NULL;
        int num_slots_to_relocate = 1;
        int candidate_slot_idx = findGuestSlotToRelocate(requester_pmmu, &user_pmmu, &user_pt, guest_slot_selection_policy);
        Addr slot_to_be_evicted_p_addr = candidate_slot_idx * user_pmmu->getPageSizeBytes();

        relocateSlots(user_pmmu,
                      user_pt,
                      requester_pmmu,
                      slot_to_be_evicted_p_addr,
                      num_slots_to_relocate,
                      guest_slot_relocation_policy);

        // allocate one slot for the requested page on the local SPM
        remaining_pages -= allocation_helper_on_occupied_pages(requester_pmmu,
                                                               requester_pt,
                                                               aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                                               num_slots_to_relocate,
                                                               alloc_mode,
                                                               data_importance,
                                                               app_priority,
                                                               requester_pmmu,
                                                               slot_to_be_evicted_p_addr,
                                                               user_pmmu);

        current_local_spm_share += float(num_slots_to_relocate) / float(requester_pmmu->getSPMSizePages());
    }

    // 3. If we still need more SPM slots, look at the nearby SPMs in the order of hop distance
    remaining_pages -= GreedySPM::allocate(tc,
                                           aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                           end_addr,
                                           alloc_mode,
                                           data_importance,
                                           app_priority);

    return total_num_pages - remaining_pages;
}
