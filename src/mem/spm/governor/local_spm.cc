#include "mem/spm/governor/local_spm.hh"

#include <iostream>

LocalSPM *
LocalSPMParams::create()
{
    return new LocalSPM(this);
}

LocalSPM::LocalSPM(const Params *p)
    : BaseGovernor(p)
{
    gov_type = "LocalSPM";
}

LocalSPM::~LocalSPM()
{

}

void
LocalSPM::init()
{

}

int
LocalSPM::allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                   AllocationModes alloc_mode,
                   DataImportance data_importance,
                   ThreadPriority app_priority)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *requester_spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *requester_pmmu = requester_spm->myPMMU;
    PMMU *host_pmmu = NULL;
    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

    // TODO: page alignment needs to be fixed (inclusive or exclusive?)
    const int page_size = requester_pmmu->getPageSizeBytes();
    // Addr aligned_start_addr = requester_pmmu->spmPageAlignUP(start_addr);
    // Addr aligned_end_addr = requester_pmmu->spmPageAlignDown(end_addr);
    Addr aligned_start_addr = requester_pmmu->spmPageAlignDown(start_addr);
    Addr aligned_end_addr = requester_pmmu->spmPageAlignUP(end_addr);
    const Addr addr_range = aligned_end_addr - aligned_start_addr;
    const int total_num_pages = CEIL_DIV(addr_range, page_size);
    int remaining_pages = total_num_pages;

    // Allocate on local SPM
    host_pmmu = requester_pmmu;
    DPRINTF(SPMALLOC, "local_spm, start_addr %x end_addr %x aligned_start_addr %x exclusive %x\n", start_addr, end_addr, aligned_start_addr, aligned_start_addr + (total_num_pages - remaining_pages) * page_size);
    remaining_pages -= allocation_helper_on_free_pages(requester_pmmu,
                                                       requester_pt,
                                                       aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                                       remaining_pages,
                                                       alloc_mode,
                                                       data_importance,
                                                       app_priority,
                                                       host_pmmu);

    return total_num_pages - remaining_pages;
}

int
LocalSPM::deAllocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                     DeallocationModes dealloc_mode)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *requester_pmmu = spm->myPMMU;
    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

    // TODO: page alignment needs to be fixed (inclusive or exclusive?)
    const int page_size = requester_pmmu->getPageSizeBytes();
    Addr aligned_start_addr = requester_pmmu->spmPageAlignUP(start_addr);
    Addr aligned_end_addr = requester_pmmu->spmPageAlignDown(end_addr);
    const Addr addr_range = aligned_end_addr - aligned_start_addr;
    const int total_num_pages = CEIL_DIV(addr_range, page_size);

    int num_removed_pages = dallocation_helper_virtual_address(requester_pmmu,
                                                               requester_pt,
                                                               aligned_start_addr,
                                                               total_num_pages,
                                                               dealloc_mode);

    return num_removed_pages;
}

int
LocalSPM::allocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params)
{
	return 0;
}

int
LocalSPM::deAllocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params)
{
	return 0;
}

int
LocalSPM::blockEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params)
{
	return 0;
}
