#ifndef __GOV_REQ_INFO_HH__
#define __GOV_REQ_INFO_HH__

#include <limits>
#include "mem/ruby/common/TypeDefines.hh"
#include "mem/spm/api/spm_types.h"

#define UINT_MAXIMUM std::numeric_limits<unsigned int>::max()

class GovReqInfo
{
  private:

    enum ReqType {
        Allocation = 0,
        Deallocation,
        RelocationRead,
        RelocationWrite,
        NUM_REQ_TYPES
    };

    enum ReqStatus {
        Complete = 0,
        Incomplete,
        NUM_REQ_STATUS
    };

    bool validInfo;

    Addr spm_p_addr;
    Addr mem_p_addr;
    Addr mem_v_addr;
    ReqType request_type;
    ReqStatus request_status;

    // Allocation/Deallocation Modes
    AllocationModes alloc_mode;
    DeallocationModes dealloc_mode;

    bool stall_core;

    // These are used to signal a node that is waiting for us
    bool needs_to_signal;
    NodeID signalee;

    // These are used to wait for a signal from a node before
    // this request can proceed
    bool ready_to_be_fired;
    NodeID signaler;

    // Relocation-specific
    Addr future_spm_p_addr;
    NodeID future_host_node;

  public:

    GovReqInfo() : validInfo (false),
                   request_type(NUM_REQ_TYPES),
                   request_status(NUM_REQ_STATUS),
                   alloc_mode(NUM_ALLOCATION_MODE),
                   dealloc_mode(NUM_DEALLOCATION_MODE),
                   stall_core(true),
                   needs_to_signal(false),
                   signalee(UINT_MAXIMUM),
                   ready_to_be_fired(true),
                   signaler(UINT_MAXIMUM),
                   future_spm_p_addr(0),
                   future_host_node(UINT_MAXIMUM) {}

    const char *toString()
    {
        static const char * ReqTypeStrings[] = { "Allocation",
                                                 "Deallocation",
                                                 "RelocationRead",
                                                 "RelocationWrite",
                                                 "Uninitialized" };
        return ReqTypeStrings[request_type];
    }

    void markComplete()   { request_status = Complete; }
    void markIncomplete() { request_status = Incomplete; }

    bool isComplete() { return (request_status == Complete); }

    void makeAllocate()        { request_type = Allocation; }
    void makeDeallocate()      { request_type = Deallocation; }
    void makeRelocationRead()  { request_type = RelocationRead; }
    void makeRelocationWrite() { request_type = RelocationWrite; }

    bool isDeallocate()      { return request_type == Deallocation; }
    bool isAllocate()        { return request_type == Allocation; }
    bool isRelocationRead()  { return request_type == RelocationRead; }
    bool isRelocationWrite() { return request_type == RelocationWrite; }

    bool isGovReq()
    {
        return (isAllocate()         ||
                isDeallocate()       ||
                isRelocationRead()   ||
                isRelocationWrite());
    }

    void setAllocationMode(AllocationModes _alloc_mode)       { alloc_mode = _alloc_mode; }
    AllocationModes getAllocationMode()                       { return alloc_mode; }
    void setDeallocationMode(DeallocationModes _dealloc_mode) { dealloc_mode = _dealloc_mode; }
    DeallocationModes getDeallocationMode()                   { return dealloc_mode; }

    void setStallStatus(bool _stall_core) { stall_core = _stall_core;}
    bool shouldStall()                    { return stall_core;}

    bool hasSignalee()                 { return needs_to_signal; }
    void shouldSignal()                { needs_to_signal = true; }
    void shouldNotSignal()             { needs_to_signal = false; }
    void setSignalee(NodeID _signalee) { signalee = _signalee;}
    NodeID getSignalee()               { return signalee;}

    bool hasSignaler()                 { return !ready_to_be_fired; }
    void shouldWait()                  { ready_to_be_fired = false; }
    void shouldNotWait()               { ready_to_be_fired = true; }
    void setSignaler(NodeID _signaler) { signaler = _signaler;}
    NodeID getSignaler()               { return signaler;}

    void setAddresses(Addr _spm_p_addr, Addr _mem_p_addr, Addr _mem_v_addr)
    {
        spm_p_addr = _spm_p_addr;
        mem_p_addr = _mem_p_addr;
        mem_v_addr = _mem_v_addr;

        validInfo =  true;
    }

    void setSPMAddress(Addr _spm_p_addr)      { spm_p_addr = _spm_p_addr; }
    void setPhysicalAddress(Addr _mem_p_addr) { mem_p_addr = _mem_p_addr; }
    void setVirtualAddress(Addr _mem_v_addr)  { mem_v_addr = _mem_v_addr; }

    Addr getSPMAddress()      { return spm_p_addr; }
    Addr getPhysicalAddress() { return mem_p_addr; }
    Addr getVirtualAddress()  { return mem_v_addr; }

    void setFutureHost(NodeID _future_host_node)      {future_host_node = _future_host_node;}
    void setFutureSPMAddress(Addr _future_spm_p_addr) {future_spm_p_addr = _future_spm_p_addr;}

    NodeID getFutureHost()     { return future_host_node; }
    Addr getFutureSPMAddress() { return future_spm_p_addr; }

    bool shouldMarkinVAM;
};

#endif  /* __GOV_REQ_INFO_HH__ */
