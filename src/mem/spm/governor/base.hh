#ifndef __BASE_GOVERNOR_HH__
#define __BASE_GOVERNOR_HH__

#include "sim/sim_object.hh"
#include "sim/process.hh"
#include "params/BaseGovernor.hh"
#include "cpu/thread_context.hh"
#include "cpu/base.hh"
#include "mem/spm/pmmu.hh"
#include "mem/spm/spm_class/SPMPage.hh"
#include "mem/spm/spm_class/spm.hh"
#include "mem/spm/api/spm_types.h"
#include "debug/GOV.hh"

#include <string>
using namespace std;

#define CEIL_DIV(X,Y) (X%Y)?(X/Y)+1:(X/Y)
#define MIN(X,Y)      (X<Y)?X:Y

class BaseGovernor : public SimObject {

  public:

    typedef BaseGovernorParams Params;
    BaseGovernor(const Params *p);

    virtual ~BaseGovernor() {}
    virtual void init() {}

    // SPM APIs
    virtual int allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                         AllocationModes alloc_mode = AllocationModes::COPY,
                         DataImportance data_importance = DataImportance::MAX_IMPORTANCE,
                         ThreadPriority app_priority = ThreadPriority::HIGH_PRIORITY) = 0;
    virtual int deAllocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                           DeallocationModes dealloc_mode = DeallocationModes::WRITE_BACK) = 0;

    virtual int allocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params) = 0;
    virtual int deAllocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params) = 0;
    virtual int blockEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params) = 0;

    ATT global_att;

  protected:
    // dimensions of the mesh
    int num_column;
    int num_row;

    std::string gov_type;

    // SPM allocation/deallocation utilities
    void getMaxContiguousFreePages(PMMU *host_pmmu, int *max_num_pages, Addr *base_free_pages);
    void getMaxContiguousFreePages(PMMU *host_pmmu, int max_num_pages_needed, int start_page_index,
                                   int *max_num_pages_found, Addr *base_free_pages_found);

    // SPM allocation/deallocation/relocation primitives
    virtual int allocation_helper_on_free_pages(PMMU *requester_pmmu,
                                                FuncPageTable *requester_pt,
                                                uint64_t aligned_start_addr,
                                                int total_num_pages,
                                                AllocationModes alloc_mode,
                                                DataImportance data_importance,
                                                ThreadPriority app_priority,
                                                PMMU *host_pmmu);

    virtual int allocation_helper_on_occupied_pages(PMMU *requester_pmmu,
                                                    FuncPageTable *requester_pt,
                                                    uint64_t aligned_start_addr,
                                                    int total_num_pages,
                                                    AllocationModes alloc_mode,
                                                    DataImportance data_importance,
                                                    ThreadPriority app_priority,
                                                    PMMU *host_pmmu,
                                                    Addr base_free_pages,
                                                    PMMU *user_pmmu);

    virtual int dallocation_helper_virtual_address(PMMU *requester_pmmu,
                                                   FuncPageTable *requester_pt,
                                                   uint64_t aligned_start_addr,
                                                   int total_num_pages,
                                                   DeallocationModes dealloc_mode,
                                                   PMMU *user_pmmu = NULL);

    virtual int dallocation_helper_spm_address(PMMU *current_user_pmmu,
                                               FuncPageTable *current_user_pt,
                                               PMMU *host_pmmu,
                                               uint64_t start_spm_p_addr,
                                               int total_num_pages,
                                               DeallocationModes dealloc_mode,
                                               PMMU *future_user_pmmu);

    virtual void relocation_helper_virtual_address(PMMU *user_pmmu,
                                                   Addr aligned_start_addr,
                                                   int total_num_pages,
                                                   PMMU *future_host_pmmu,
                                                   Addr future_start_spm_p_addr);

    virtual void relocation_helper_spm_address(PMMU *user_pmmu,
                                               PMMU *current_host_pmmu,
                                               Addr current_start_spm_p_addr,
                                               int total_num_pages,
                                               PMMU *future_host_pmmu,
                                               Addr future_start_spm_p_addr);
};

#endif  /* __BASE_GOVERNOR_HH__ */
