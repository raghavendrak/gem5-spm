/* This class implements an SPM governor which greedily maps the allocation
 * requests to the closest free on-chip SPM.
 *
 * Authors: Majid
 * */

#ifndef __GREEDY_SPM_HH__
#define __GREEDY_SPM_HH__

#include "params/GreedySPM.hh"
#include "mem/spm/governor/random_spm.hh"
#define MAX_THREADS 1024

class GreedySPM : public RandomSPM {

  public:
    GreedySPM(const Params *p);
    virtual ~GreedySPM();
    virtual void init();

    virtual int allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                         AllocationModes alloc_mode = AllocationModes::COPY,
                         DataImportance data_importance = DataImportance::MAX_IMPORTANCE,
                         ThreadPriority app_priority = ThreadPriority::HIGH_PRIORITY);
    virtual void addPMMU(PMMU *p);

    virtual int allocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);
    virtual int deAllocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);
    virtual int blockEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);

    bool sendSPMAddr[MAX_THREADS];
    Addr startVAAddr[MAX_THREADS];
    Addr endVAAddr[MAX_THREADS];
    Addr startSPMAddr[MAX_THREADS];
    Addr endSPMAddr[MAX_THREADS];
    unsigned call_count[MAX_THREADS];
    unsigned alloc_size[MAX_THREADS];
    unsigned dealloc_size[MAX_THREADS];

  protected:
    map<PMMU *, pair<int,int>> pmmu_to_coord;

    int findHopDistance (pair <int, int> pmmu_o, pair <int, int> pmmu_h);
    void distanceBasedSort(pair <int, int> central_pmmu);

};

#endif  /* __GREEDY_SPM_HH__ */
