/* This class implements an SPM governor which maps the allocation
 * requests to the local SPMs only. It first finds the highest number
 * of free contiguous slots on the local SPM and then maps as many pages
 * as possible to that region and repeats this process until no page is free
 * and then the rest would go off chip.
 *
 * Authors: Majid, Donny
 * */

#ifndef __LOCAL_SPM_HH__
#define __LOCAL_SPM_HH__

#include "params/LocalSPM.hh"
#include "mem/spm/governor/base.hh"
#include "debug/SPMALLOC.hh"

using namespace std;

class LocalSPM : public BaseGovernor {

  public:
    LocalSPM(const Params *p);
    virtual ~LocalSPM();
    virtual void init();

    // SPM APIs
    virtual int allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                         AllocationModes alloc_mode = AllocationModes::COPY,
                         DataImportance data_importance = DataImportance::MAX_IMPORTANCE,
                         ThreadPriority app_priority = ThreadPriority::HIGH_PRIORITY);
    virtual int deAllocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                           DeallocationModes dealloc_mode = DeallocationModes::WRITE_BACK);

    virtual int allocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);
    virtual int deAllocateEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);
    virtual int blockEpiphany(ThreadContext *tc, uint64_t start_addr, uint64_t params);

};

#endif  /* __LOCAL_SPM_HH__ */
