#include "mem/spm/governor/random_spm.hh"

#include <iostream>
#include <algorithm>

RandomSPM *
RandomSPMParams::create()
{
    return new RandomSPM(this);
}

RandomSPM::RandomSPM(const Params *p)
    : LocalSPM(p)
{
    gov_type = "RandomSPM";
}

RandomSPM::~RandomSPM()
{

}

void
RandomSPM::init()
{

}

/*
Example of (col, row) ordering:
(2,0)  (1,0)  (0,0)
(2,1)  (1,1)  (0,1)
(2,2)  (1,2)  (0,2)
*/
void
RandomSPM::addPMMU(PMMU *p)
{
    vector<PMMU *>::iterator it = pmmus.begin();
    pmmus.insert(it,p);

    int col = p->getMachineID().num % num_column;
    int row = p->getMachineID().num / num_column;
    assert (coord_to_pmmu.find(make_pair(col,row)) == coord_to_pmmu.end());
    coord_to_pmmu[make_pair(col,row)] = p;
}

int
RandomSPM::allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                    AllocationModes alloc_mode,
                    DataImportance data_importance,
                    ThreadPriority app_priority)
{
    BaseCPU *cpu = tc->getCpuPtr();
    BaseSPM::SPMSlavePort& spm_port = dynamic_cast<BaseSPM::SPMSlavePort&>(cpu->getMasterPort("dcache_port", 0).getSlavePort());
    SPM *spm = dynamic_cast<SPM*>(spm_port.getOwner());
    PMMU *requester_pmmu = spm->myPMMU;
    PMMU *host_pmmu = NULL;
    FuncPageTable *requester_pt = dynamic_cast<FuncPageTable*>(tc->getProcessPtr()->pTable);

    // TODO: page alignment needs to be fixed (inclusive or exclusive?)
    const int page_size = requester_pmmu->getPageSizeBytes();
    Addr aligned_start_addr = requester_pmmu->spmPageAlignUP(start_addr);
    Addr aligned_end_addr = requester_pmmu->spmPageAlignDown(end_addr);
    const Addr addr_range = aligned_end_addr - aligned_start_addr;
    const int total_num_pages = CEIL_DIV(addr_range, page_size);
    int remaining_pages = total_num_pages;

    // 1. Allocate on the local SPM first
    remaining_pages -= LocalSPM::allocate(tc,
                                          aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                          end_addr,
                                          alloc_mode,
                                          data_importance,
                                          app_priority);

    // 2. If we still need more SPM slots, target randomly selected remote SPMs
    if (remaining_pages > 0) {
        vector<int> indices;
        for (int i=0; i < pmmus.size(); i++) {
            indices.push_back(i);
        }
        random_shuffle(indices.begin(), indices.end());

        while (indices.size() > 0 && remaining_pages > 0) {
            int ind = *indices.begin();
            indices.erase(indices.begin());
            host_pmmu = pmmus[ind];
            remaining_pages -= allocation_helper_on_free_pages(requester_pmmu,
                                                               requester_pt,
                                                               aligned_start_addr + (total_num_pages - remaining_pages) * page_size,
                                                               remaining_pages,
                                                               alloc_mode,
                                                               data_importance,
                                                               app_priority,
                                                               host_pmmu);

        }
    }
    return total_num_pages - remaining_pages;
}
