/* This class implements an SPM governor which greedily maps the allocation
 * requests to the closest free SPM, while guaranteeing a each thread gets
 * defined percentage of its local SPM should it needs that space.
 *
 * Authors: Majid, Donny
 * */

#ifndef __GUARANTEED_GREEDY_SPM_HH__
#define __GUARANTEED_GREEDY_SPM_HH__

#include "params/GuaranteedGreedySPM.hh"
#include "mem/spm/governor/greedy_spm.hh"

class GuaranteedGreedySPM : public GreedySPM {

  public:
    GuaranteedGreedySPM(const Params *p);
    virtual ~GuaranteedGreedySPM();
    virtual void init();

    virtual int allocate(ThreadContext *tc, uint64_t start_addr, uint64_t end_addr,
                         AllocationModes alloc_mode = AllocationModes::COPY,
                         DataImportance data_importance = DataImportance::MAX_IMPORTANCE,
                         ThreadPriority app_priority = ThreadPriority::HIGH_PRIORITY);

  protected:
    float local_share;
    string guest_slot_selection_policy;
    string guest_slot_relocation_policy;

    virtual float spmShare(PMMU *owner_pmmu, PMMU *user_pmmu);

    virtual int findGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt, string selection_policy = "LeastIndexed");
    virtual int findLeastIndexedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt);
    virtual int findLeastFrequentlyUsedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt);
    virtual int findLeastRecentlyUsedGuestSlotToRelocate(PMMU *owner_pmmu, PMMU **user_pmmu, FuncPageTable **user_pt);

    virtual bool findClosestFreeSlots(PMMU *requester_pmmu, int total_num_pages, PMMU **host_pmmu, Addr *base_free_pages);

    virtual void relocateSlots(PMMU *user_pmmu,
                               FuncPageTable *user_pt,
                               PMMU *current_host_pmmu,
                               Addr current_start_spm_p_addr,
                               int total_num_pages,
                               string relocation_policy);
};

#endif  /* __GUARANTEED_GREEDY_SPM_HH__ */
