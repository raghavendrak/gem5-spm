#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <sstream>
#include <string>
#include <typeinfo>

#include "base/compiler.hh"
#include "base/cprintf.hh"

#include "mem/protocol/Types.hh"
#include "mem/ruby/system/RubySystem.hh"
#include "mem/ruby/slicc_interface/RubySlicc_includes.hh"

#include "debug/PMMU.hh"
#include "debug/ATT.hh"
#include "debug/SPM_DEBUG.hh"
#include "debug/SPMMisses.hh"
#include "debug/fiveG.hh"
#include "debug/MARKER.hh"

#include "mem/spm/pmmu.hh"
#include "mem/spm/governor/local_spm.hh"
#include "mem/spm/governor/random_spm.hh"
#include "mem/spm/governor/greedy_spm.hh"
#include "mem/spm/governor/guaranteed_greedy_spm.hh"

int PMMU::m_num_controllers = 0;
int PMMU::m_page_size_bytes;

PMMU *
PMMUParams::create()
{
    return new PMMU(this);
}

PMMU::PMMU(const Params *p)
    : AbstractController(p),
      my_spm_ptr(nullptr),
      my_governor_ptr(p->governor),
      pending_gov_reqs(0)
{
    m_machineID.type = MachineType_PMMU;
    m_machineID.num = m_version;
    m_num_controllers++;

    spmSlavePort = new SPMSideSlavePort(p->name + ".spm_s_side", this, 0);
    spmMasterPort = new SPMSideMasterPort(p->name + ".spm_m_side", this, 1);

    m_page_size_bytes = p->page_size_bytes;
    assert(isPowerOf2(m_page_size_bytes));

    m_in_ports = 2; //@TODO: Should this be 2?
//    m_dma_sequencer_ptr->setController(this);
//    m_request_latency = p->request_latency;
    m_responseFromSPM_ptr = p->responseFromSPM;
    m_responseToSPM_ptr = p->responseToSPM;
    m_requestFromSPM_ptr = p->requestFromSPM;
    m_requestToSPM_ptr = p->requestToSPM;
    m_responseToNetwork_ptr = p->responseToNetwork;
    m_requestToNetwork_ptr = p->requestToNetwork;

	m_net_net_ptr[0] = p->requestToNet1;
	m_net_spm_ptr[0] = p->requestToSPM1;
	m_net_net_ptr[1] = p->requestToNet2;
	m_net_spm_ptr[1] = p->requestToSPM2;
	m_net_net_ptr[2] = p->requestToNet3;
	m_net_spm_ptr[2] = p->requestToSPM3;
	m_net_net_ptr[3] = p->requestToNet4;
	m_net_spm_ptr[3] = p->requestToSPM4;
	m_net_net_ptr[4] = p->requestToNet5;
	m_net_spm_ptr[4] = p->requestToSPM5;
	m_net_net_ptr[5] = p->requestToNet6;
	m_net_spm_ptr[5] = p->requestToSPM6;
	m_net_net_ptr[6] = p->requestToNet7;
	m_net_spm_ptr[6] = p->requestToSPM7;
	m_net_net_ptr[7] = p->requestToNet8;
	m_net_spm_ptr[7] = p->requestToSPM8;
	m_net_net_ptr[8] = p->requestToNet9;
	m_net_spm_ptr[8] = p->requestToSPM9;
	m_net_net_ptr[9] = p->requestToNet10;
	m_net_spm_ptr[9] = p->requestToSPM10;
	m_net_net_ptr[10] = p->requestToNet11;
	m_net_spm_ptr[10] = p->requestToSPM11;
	m_net_net_ptr[11] = p->requestToNet12;
	m_net_spm_ptr[11] = p->requestToSPM12;
	m_net_net_ptr[12] = p->requestToNet13;
	m_net_spm_ptr[12] = p->requestToSPM13;
	m_net_net_ptr[13] = p->requestToNet14;
	m_net_spm_ptr[13] = p->requestToSPM14;
	m_net_net_ptr[14] = p->requestToNet15;
	m_net_spm_ptr[14] = p->requestToSPM15;
	m_net_net_ptr[15] = p->requestToNet16;
	m_net_spm_ptr[15] = p->requestToSPM16;
	m_net_net_ptr[16] = p->requestToNet17;
	m_net_spm_ptr[16] = p->requestToSPM17;
	m_net_net_ptr[17] = p->requestToNet18;
	m_net_spm_ptr[17] = p->requestToSPM18;
	m_net_net_ptr[18] = p->requestToNet19;
	m_net_spm_ptr[18] = p->requestToSPM19;
	m_net_net_ptr[19] = p->requestToNet20;
	m_net_spm_ptr[19] = p->requestToSPM20;
	m_net_net_ptr[20] = p->requestToNet21;
	m_net_spm_ptr[20] = p->requestToSPM21;
	m_net_net_ptr[21] = p->requestToNet22;
	m_net_spm_ptr[21] = p->requestToSPM22;
	m_net_net_ptr[22] = p->requestToNet23;
	m_net_spm_ptr[22] = p->requestToSPM23;
	m_net_net_ptr[23] = p->requestToNet24;
	m_net_spm_ptr[23] = p->requestToSPM24;
	m_net_net_ptr[24] = p->requestToNet25;
	m_net_spm_ptr[24] = p->requestToSPM25;
	m_net_net_ptr[25] = p->requestToNet26;
	m_net_spm_ptr[25] = p->requestToSPM26;
	m_net_net_ptr[26] = p->requestToNet27;
	m_net_spm_ptr[26] = p->requestToSPM27;
	m_net_net_ptr[27] = p->requestToNet28;
	m_net_spm_ptr[27] = p->requestToSPM28;
	m_net_net_ptr[28] = p->requestToNet29;
	m_net_spm_ptr[28] = p->requestToSPM29;
	m_net_net_ptr[29] = p->requestToNet30;
	m_net_spm_ptr[29] = p->requestToSPM30;
	m_net_net_ptr[30] = p->requestToNet31;
	m_net_spm_ptr[30] = p->requestToSPM31;
	m_net_net_ptr[31] = p->requestToNet32;
	m_net_spm_ptr[31] = p->requestToSPM32;

    numSPMs = p->num_spms;
    addToGovernor(p->gov_type);
    block_till_cycle = 0;
    block_network_for_cycles = 0;
}

BaseMasterPort &
PMMU::getMasterPort(const std::string &if_name, PortID idx)
{
    if (if_name == "spm_m_side") {
        return *spmMasterPort;
    }
    else {
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort &
PMMU::getSlavePort(const std::string &if_name, PortID idx)
{
    if (if_name == "spm_s_side") {
        return *spmSlavePort;
    }
    else {
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
PMMU::addToGovernor(std::string gov_type)
{
    if (gov_type.compare("Random") == 0) {
        dynamic_cast<RandomSPM*>(my_governor_ptr)->addPMMU(this);
    }
    else if (gov_type.compare("Greedy") == 0) {
        dynamic_cast<GreedySPM*>(my_governor_ptr)->addPMMU(this);
    }
    else if (gov_type.compare("GuaranteedGreedy") == 0) {
        dynamic_cast<GuaranteedGreedySPM*>(my_governor_ptr)->addPMMU(this);
    }
}

void
PMMU::initNetQueues()
{
    MachineType machine_type = string_to_MachineType("PMMU");
    int base M5_VAR_USED = MachineType_base_number(machine_type);

    //assert(m_responseFromDir_ptr != NULL);
    //m_net_ptr->setFromNetQueue(m_version + base, m_responseFromDir_ptr->getOrdered(), 1,
    //                                 "response", m_responseFromDir_ptr);
    //assert(m_requestToDir_ptr != NULL);
    //m_net_ptr->setToNetQueue(m_version + base, m_requestToDir_ptr->getOrdered(), 0,
    //                                 "request", m_requestToDir_ptr);

    assert(m_responseToNetwork_ptr != NULL);
    m_net_ptr->setToNetQueue(m_version + base, m_responseToNetwork_ptr->getOrdered(), 0,
                                     "response", m_responseToNetwork_ptr);
    assert(m_requestToNetwork_ptr != NULL);
    m_net_ptr->setToNetQueue(m_version + base, m_requestToNetwork_ptr->getOrdered(), 1,
                                     "request", m_requestToNetwork_ptr);

    assert(m_requestToSPM_ptr != NULL);
    m_net_ptr->setFromNetQueue(m_version + base, m_requestToSPM_ptr->getOrdered(), 1,
                                     "request", m_requestToSPM_ptr);
    assert(m_responseToSPM_ptr!= NULL);
    m_net_ptr->setFromNetQueue(m_version + base, m_responseToSPM_ptr->getOrdered(), 0,
                                     "response", m_responseToSPM_ptr);


    for (unsigned i = 0; i < NUM_NETWORKS; i++) {
    	stringstream req_str;
    	req_str << "request_" << i;
    	DPRINTF(fiveG, "netId: %d req_str: %s\n", i, req_str.str());
    	assert(m_net_net_ptr[i] != NULL);
    	m_net_ptr->setToNetQueue(m_version + base, m_net_net_ptr[i]->getOrdered(), (i + 2),
    			req_str.str(), m_net_net_ptr[i]);
    	assert(m_net_spm_ptr[i] != NULL);
    	m_net_ptr->setFromNetQueue(m_version + base, m_net_spm_ptr[i]->getOrdered(), (i + 2),
    			req_str.str(), m_net_spm_ptr[i]);
    }
}

void
PMMU::init()
{
    //(*m_mandatoryQueue_ptr).setConsumer(this);
    //(*m_responseFromDir_ptr).setConsumer(this);

    (*m_responseFromSPM_ptr).setConsumer(this);
    (*m_requestFromSPM_ptr).setConsumer(this);
    (*m_responseToSPM_ptr).setConsumer(this);
    (*m_requestToSPM_ptr).setConsumer(this);

    for (unsigned i = 0; i < NUM_NETWORKS; i++) {
    	(*m_net_spm_ptr[i]).setConsumer(this);
    }

    AbstractController::init();
    resetStats();
}

void
PMMU::setSPM(SPM *_spm)
{
    assert(!my_spm_ptr);
    my_spm_ptr = _spm;
}

void
PMMU::regStats()
{
    AbstractController::regStats();
}

void
PMMU::collateStats()
{

}

int
PMMU::getNumControllers()
{
    return m_num_controllers;
}

MessageBuffer*
PMMU::getMandatoryQueue() const
{
    return NULL;
}

MessageBuffer*
PMMU::getMemoryQueue() const
{
    return NULL;
}

Sequencer*
PMMU::getCPUSequencer() const
{
    return NULL;
}

GPUCoalescer*
PMMU::getGPUCoalescer() const
{
    return NULL;
}

void
PMMU::print(ostream& out) const
{
    out << "[PMMU " << m_version << "]";
}

void PMMU::resetStats()
{

}

void
PMMU::recordCacheTrace(int cntrl, CacheRecorder* tr)
{
}

AccessPermission
PMMU::getAccessPermission(const Addr& param_addr)
{
    return AccessPermission_NotPresent;
}

void
PMMU::functionalRead(const Addr& param_addr, Packet* param_pkt)
{
    panic("PMMU does not support functional read.");
}

int
PMMU::functionalWrite(const Addr& param_addr, Packet* param_pkt)
{
    panic("PMMU does not support functional write.");
}

int
PMMU::functionalWriteBuffers(PacketPtr& pkt)
{
    int num_functional_writes = 0;
    //num_functional_writes += m_responseFromDir_ptr->functionalWrite(pkt);
    //num_functional_writes += m_requestToDir_ptr->functionalWrite(pkt);
    //num_functional_writes += m_mandatoryQueue_ptr->functionalWrite(pkt);
    return num_functional_writes;
}

int
PMMU::addATTMappingsVAddress(Addr start_v_page_addr,
                             MachineID host_node,
                             Addr start_p_spm_addr,
                             int num_pages,
                             FuncPageTable *pt,
                             AllocationModes alloc_mode,
                             bool wait_for_signal,
                             NodeID signaler)
{
    int added_pages = 0;
    Addr next_p_spm_addr = start_p_spm_addr;
    for (int v_page_index = 0; v_page_index < num_pages; v_page_index++) {

        Addr v_page_addr = start_v_page_addr + v_page_index*getPageSizeBytes();

        DPRINTF(ATT, "Node %d: Adding ATT mapping for virtual address %x on node %d, spm address %d\n",
                getNodeID(), v_page_addr, host_node, next_p_spm_addr);

        // update ATT
        if (my_att.addMapping(v_page_addr, host_node, next_p_spm_addr)) {
        	if (getGovernor()->global_att.addMapping(v_page_addr, host_node, next_p_spm_addr)) {
        		getGovernor()->global_att.validateATTEntry(getGovernor()->global_att.getMapping(v_page_addr));
        	}
            added_pages++;
            triggerPageAlloc(v_page_addr, host_node, next_p_spm_addr, pt, alloc_mode, wait_for_signal, signaler);
            next_p_spm_addr += getPageSizeBytes();
            if (!wait_for_signal && alloc_mode == UNINITIALIZE) {
                my_att.validateATTEntry(my_att.getMapping(v_page_addr));
            }
        }
    }
    return added_pages;
}

void
PMMU::triggerPageAlloc(Addr v_page_addr,
                       MachineID host_node,
                       Addr p_spm_addr,
                       FuncPageTable *pt,
                       AllocationModes alloc_mode,
                       bool wait_for_signal,
                       NodeID signaler)
{
    Addr p_page_addr;
    bool translated = pt->translate(v_page_addr, p_page_addr);
    if (!translated)
        panic("Virtual page doesn't exist in page table!.\n");

    RequestPtr alloc_req = new Request(p_page_addr, getPageSizeBytes(), Request::PHYSICAL, Request::funcMasterId);
    PacketPtr alloc_pkt = new Packet(alloc_req, MemCmd::ReadReq, getPageSizeBytes());
    alloc_pkt->allocate();

    alloc_pkt->govInfo.setAddresses(p_spm_addr, p_page_addr, v_page_addr);
    alloc_pkt->govInfo.markIncomplete();
    alloc_pkt->govInfo.makeAllocate();
    alloc_pkt->govInfo.setAllocationMode(alloc_mode);

    alloc_pkt->spmInfo.setOrigin(m_machineID.num);

    alloc_pkt->govInfo.shouldNotSignal();
    alloc_pkt->govInfo.setSignaler(signaler);
    wait_for_signal ? alloc_pkt->govInfo.shouldWait() : alloc_pkt->govInfo.shouldNotWait();

    alloc_pkt->govInfo.setStallStatus(alloc_pkt->govInfo.hasSignaler() ||
                                     (alloc_pkt->govInfo.getAllocationMode() == COPY));

    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_Type = SPMRequestType_ALLOC;
    msg->m_PktPtr = alloc_pkt;
    (msg->m_Destination).add(host_node);
    msg->m_Requestor = m_machineID;
    msg->m_MessageSize = MessageSizeType_Control;

    if (host_node.num == m_machineID.num) {
        m_requestToSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else {
        m_requestToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    if (alloc_pkt->govInfo.shouldStall()) {
        // block the spm's cpu-side port
        my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
        // to make sure we don't unblock the port unless all pages are allocated
        pending_gov_reqs++;
    }

    DPRINTF(PMMU, "Node %d: Allocating a page on node %d, spm address %d, should wait %d, for node %d\n",
                   getNodeID(), host_node.num, p_spm_addr, wait_for_signal, signaler);
}

void
PMMU::trigger_spm_allocate_dma(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params)
{
	Addr p_page_addr;
    bool translated = pt->translate(startVAAddr, p_page_addr);
    if (!translated)
        panic("Virtual page doesn't exist in page table!.\n");


    RequestPtr alloc_req = new Request(p_page_addr, size, Request::PHYSICAL, Request::funcMasterId);
    PacketPtr alloc_pkt = new Packet(alloc_req, MemCmd::ReadReq, size);
    alloc_pkt->allocate();

	DPRINTF(PMMU, "%s Allocating startVAAddr: %x endVAAddr: %x startSPMAddr: %x endSPMAddr: %x p_page_addr: %x pktAddr: %x reqPAddr: %x\n", __func__, startVAAddr,
			endVAAddr, startSPMAddr, endSPMAddr, p_page_addr, alloc_pkt->getAddr(), alloc_pkt->req->getPaddr());

    alloc_pkt->govInfo.setAddresses(startSPMAddr, p_page_addr, startVAAddr);
    alloc_pkt->govInfo.markIncomplete();
    alloc_pkt->govInfo.makeAllocate();

    uint64_t alloc_mode = (params >> 0) & 0x000000000000000F;
    alloc_pkt->govInfo.setAllocationMode(static_cast<AllocationModes>(alloc_mode));
    if (alloc_pkt->govInfo.getAllocationMode() == COPY_MARK) {
    	alloc_pkt->govInfo.shouldMarkinVAM = true;
    }
    else {
    	alloc_pkt->govInfo.shouldMarkinVAM = false;
    }

    alloc_pkt->spmInfo.setOrigin(m_machineID.num);

    alloc_pkt->govInfo.shouldNotSignal();
    alloc_pkt->govInfo.setSignaler(signaler);

    my_spm_ptr->triggerPageAllocEpiphany(alloc_pkt);

}

void
PMMU::trigger_spm_stack_allocate(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params)
{
	assert ((startVAAddr + size - 1) == endVAAddr);
	my_spm_ptr->triggerStackAllocEpiphany(startVAAddr, endVAAddr, size);
}

void
PMMU::trigger_spm_stack_deallocate(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params)
{
	assert ((startVAAddr + size - 1) == endVAAddr);
	my_spm_ptr->triggerStackDeallocEpiphany(startVAAddr, endVAAddr, size, params);
}

void
PMMU::trigger_spm_copy(Addr spm_src_start, Addr spm_src_end, Addr spm_dest_start, Addr spm_dest_end, MachineID host_node,
		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params)
{
	my_spm_ptr->triggerSPMCopy(spm_src_start, spm_src_end, spm_dest_start, spm_dest_end, size);

	uint64_t blockCycles = 10; // Latency for SPM_MEM_COPY
	block_till_cycle = ticksToCycles(curTick()) + blockCycles;
	scheduleEvent(Cycles(blockCycles));
	my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
}

int
PMMU::removeATTMappingsVAddress(Addr start_v_page_addr,
                                int num_pages,
                                FuncPageTable *pt,
                                DeallocationModes dealloc_mode,
                                bool signal,
                                NodeID signalee)
{
    int removed_pages = 0;
    for (int v_page_index = 0; v_page_index < num_pages; v_page_index++) {
        Addr v_page_addr = start_v_page_addr + v_page_index*getPageSizeBytes();

        ATTEntry* mapping_info =  my_att.getMapping(v_page_addr);

        // if we havn't allocated this page (=there was no space), then nothing needs to be removed
        if (mapping_info) {

            MachineID dest_node = mapping_info->destination_node;
            Addr spm_slot_addr = mapping_info->spm_slot_addr;

            DPRINTF(ATT, "Node %d: Removing ATT mapping for virtual address %x on node %d, spm address %d\n",
                    getNodeID(), v_page_addr, mapping_info->destination_node, mapping_info->spm_slot_addr);

            // update ATT
            if (my_att.removeMapping(v_page_addr)) {
                removed_pages++;
                triggerPageDeAlloc(v_page_addr, dest_node, spm_slot_addr, pt, dealloc_mode, signal, dest_node.num);
            }
        }
    }
    return removed_pages;
}

int
PMMU::removeATTMappingsSPMAddress(MachineID host_node,
                                  Addr p_spm_addr,
                                  int num_pages,
                                  FuncPageTable *pt,
                                  DeallocationModes dealloc_mode,
                                  bool signal,
                                  NodeID signalee)
{
    for (int i = 1; i <= num_pages; i++) {
      Addr start_v_page_addr = my_att.slot2Addr(host_node, p_spm_addr);
      removeATTMappingsVAddress(start_v_page_addr, 1, pt, dealloc_mode, signal);
    }

    return 1;
}

void
PMMU::triggerPageDeAlloc(Addr v_page_addr,
                         MachineID host_node,
                         Addr p_spm_addr,
                         FuncPageTable *pt,
                         DeallocationModes dealloc_mode,
                         bool signal,
                         NodeID signalee)
{
    Addr p_page_addr;
    bool translated = pt->translate(v_page_addr, p_page_addr);
    if (!translated)
        panic("Virtual page doesn't exist in page table!.\n");

    RequestPtr dealloc_req = new Request(p_page_addr, getPageSizeBytes(), Request::PHYSICAL, Request::funcMasterId);
    PacketPtr dealloc_pkt = new Packet(dealloc_req, MemCmd::WriteReq, getPageSizeBytes());
    dealloc_pkt->allocate();

    dealloc_pkt->spmInfo.setOrigin(m_machineID.num);

    dealloc_pkt->govInfo.setAddresses(p_spm_addr, p_page_addr, v_page_addr);
    dealloc_pkt->govInfo.markIncomplete();
    dealloc_pkt->govInfo.makeDeallocate();
    dealloc_pkt->govInfo.setDeallocationMode(dealloc_mode);

    dealloc_pkt->govInfo.setSignalee(signalee);
    signal ? dealloc_pkt->govInfo.shouldSignal() : dealloc_pkt->govInfo.shouldNotSignal();
    dealloc_pkt->govInfo.shouldNotWait();

    dealloc_pkt->govInfo.setStallStatus(dealloc_pkt->govInfo.getDeallocationMode() == WRITE_BACK);

    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_Type = SPMRequestType_DEALLOC;
    msg->m_PktPtr = dealloc_pkt;
    (msg->m_Destination).add(host_node);
    msg->m_Requestor = m_machineID;
    msg->m_MessageSize = MessageSizeType_Control;

    if (host_node.num == m_machineID.num) {
        m_requestToSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else {
        m_requestToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    if (dealloc_pkt->govInfo.shouldStall()) {
        // block the spm's cpu-side port
        my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
        // to make sure we don't unblock the port unless all pages are deallocated
        pending_gov_reqs++;
    }

    DPRINTF(PMMU, "Node %d: Deallocating a page from node %d, spm address %d, should signal %d, node %d\n",
                   getNodeID(), host_node, p_spm_addr, signal, signalee);
}

void
PMMU::trigger_spm_deallocate_dma(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params)
{
	Addr p_page_addr;
    bool translated = pt->translate(startVAAddr, p_page_addr);
    if (!translated)
        panic("Virtual page doesn't exist in page table!.\n");


    RequestPtr dealloc_req = new Request(p_page_addr, size, Request::PHYSICAL, Request::funcMasterId);
    PacketPtr dealloc_pkt = new Packet(dealloc_req, MemCmd::WriteReq, size);
    dealloc_pkt->allocate();

    dealloc_pkt->govInfo.setAddresses(startSPMAddr, p_page_addr, startVAAddr);
    dealloc_pkt->govInfo.markIncomplete();
    dealloc_pkt->govInfo.makeDeallocate();

    uint64_t dealloc_mode = (params >> 0) & 0x000000000000000F;
    dealloc_pkt->govInfo.setDeallocationMode(static_cast<DeallocationModes>(dealloc_mode));
    if (dealloc_pkt->govInfo.getDeallocationMode() == WRITE_BACK_MARK) {
    	dealloc_pkt->govInfo.shouldMarkinVAM = true;
    }
    else {
    	dealloc_pkt->govInfo.shouldMarkinVAM = false;
    }

    dealloc_pkt->spmInfo.setOrigin(m_machineID.num);

    dealloc_pkt->govInfo.shouldNotSignal();
    dealloc_pkt->govInfo.setSignaler(signaler);

	DPRINTF(PMMU, "%s Deallocating startVAAddr: %x endVAAddr: %x startSPMAddr: %x endSPMAddr: %x p_page_addr: %x pktAddr: %x reqPAddr: %x\n", __func__, startVAAddr,
			endVAAddr, startSPMAddr, endSPMAddr, p_page_addr, dealloc_pkt->getAddr(), dealloc_pkt->req->getPaddr());

    my_spm_ptr->triggerPageDeallocEpiphany(dealloc_pkt);
}

void
PMMU::changeATTMappingVAddress(Addr start_v_page_addr,
                               MachineID future_host_node,
                               Addr future_start_p_spm_addr,
                               int num_pages,
                               bool signal,
                               NodeID signalee)
{
    for (int i = 1; i <= num_pages; i++) {
        ATTEntry* currentMapping = my_att.getMapping(start_v_page_addr);
        MachineID current_node = currentMapping->destination_node;
        Addr current_start_spm_p_addr = currentMapping->spm_slot_addr;
        changeATTMappingSPMAddress(current_node,
                                   current_start_spm_p_addr,
                                   1,
                                   future_host_node,
                                   future_start_p_spm_addr,
                                   signal,
                                   signalee);
    }
}

void
PMMU::changeATTMappingSPMAddress(MachineID current_host_node,
                                 Addr current_start_p_spm_addr,
                                 int total_num_pages,
                                 MachineID future_host_node,
                                 Addr future_start_p_spm_addr,
                                 bool signal,
                                 NodeID signalee)
{
    for(int i=0; i<total_num_pages; i++) {
        // change ATT mapping first
        ATTEntry* mapping = my_att.changeMapping(current_host_node, current_start_p_spm_addr,
                                                 future_host_node, future_start_p_spm_addr);
        assert (mapping);
        DPRINTF(ATT, "Node %d: Changing ATT mapping "
                     "from node %d, spm address %d to node %d, spm address %d\n",
                     getNodeID(),
                     current_host_node, current_start_p_spm_addr,
                     future_host_node, future_start_p_spm_addr);
        // TODO: invalidate mapping during relocation and validate it again?

        triggerPageRelocation(current_host_node,
                              current_start_p_spm_addr,
                              future_host_node,
                              future_start_p_spm_addr,
                              signal,
                              current_host_node.num);
    }
}

void
PMMU::triggerPageRelocation(MachineID current_host_node,
                            Addr current_p_spm_addr,
                            MachineID future_host_node,
                            Addr future_p_spm_addr,
                            bool signal,
                            NodeID signalee)
{
    RequestPtr relocation_req = new Request(0, getPageSizeBytes(), Request::PHYSICAL, Request::funcMasterId);
    PacketPtr relocate_pkt = new Packet(relocation_req, MemCmd::ReadReq, getPageSizeBytes());
    relocate_pkt->setAddr(current_p_spm_addr);
    relocate_pkt->allocate();

    relocate_pkt->spmInfo.setOrigin(m_machineID.num);

    relocate_pkt->govInfo.setSPMAddress(current_p_spm_addr);
    relocate_pkt->govInfo.setFutureHost(future_host_node.num);
    relocate_pkt->govInfo.setFutureSPMAddress(future_p_spm_addr);

    relocate_pkt->govInfo.markIncomplete();
    relocate_pkt->govInfo.makeRelocationRead();

    relocate_pkt->govInfo.setSignalee(signalee);
    signal ? relocate_pkt->govInfo.shouldSignal() : relocate_pkt->govInfo.shouldNotSignal();
    relocate_pkt->govInfo.shouldNotWait();

    relocate_pkt->govInfo.setStallStatus(true);

    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_Type = SPMRequestType_RELOCATE_READ;
    msg->m_PktPtr = relocate_pkt;
    (msg->m_Destination).add(current_host_node);
    msg->m_Requestor = m_machineID;
    msg->m_MessageSize = MessageSizeType_Control;

    if (current_host_node.num == m_machineID.num) {
        m_requestToSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else {
        m_requestToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    if (relocate_pkt->govInfo.shouldStall()) {
        // block the spm's cpu-side port
        my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
        // to make sure we don't unblock the port unless all pages are relocated
        pending_gov_reqs++;
    }

    DPRINTF(PMMU, "Node %d: Relocating(read) a page from node %d, spm address %d, "
                  "to node %d, spm address %d, should signal %d, node %d\n",
                   getNodeID(), current_host_node, current_p_spm_addr,
                   future_host_node, future_p_spm_addr, signal, signalee);
}

void
PMMU::continuePageRelocationRequest(PacketPtr relocation_pkt)
{
    relocation_pkt->cmd = MemCmd::WriteReq;
    relocation_pkt->setAddr(relocation_pkt->govInfo.getFutureSPMAddress());
//    relocate_pkt->govInfo.spm_p_addr = relocate_pkt->govInfo.future_spm_p_addr;

    relocation_pkt->govInfo.markIncomplete();
    relocation_pkt->govInfo.makeRelocationWrite();

    relocation_pkt->govInfo.shouldNotWait();

    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_Type = SPMRequestType_RELOCATE_WRITE;
    msg->m_PktPtr = relocation_pkt;
    MachineID future_host_node;
    future_host_node.num = relocation_pkt->govInfo.getFutureHost();
    future_host_node.type = MachineType_PMMU;
    (msg->m_Destination).add(future_host_node);
    msg->m_Requestor = m_machineID;
    msg->m_MessageSize = MessageSizeType_Data;

    if (future_host_node.num == m_machineID.num) {
        m_requestToSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else {
        m_requestToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    DPRINTF(PMMU, "Node %d: Relocating(write) a page to node %d, spm address %d requested by node %d\n",
                   getNodeID(), relocation_pkt->govInfo.getFutureHost(),
                   relocation_pkt->govInfo.getFutureSPMAddress(), relocation_pkt->spmInfo.getOrigin());
}

void
PMMU::setFreePages(Addr start_p_spm_addr, int num_pages)
{
    int ind = start_p_spm_addr / getPageSizeBytes();
    int ctr;
    for (ctr = 0; ctr < num_pages; ctr++) {
        assert (ctr+ind < getSPMSizePages());
        my_spm_ptr->spmSlots[ctr+ind].setFree();
    }
}

void
PMMU::setUsedPages(Addr start_p_spm_addr, int num_pages,
                   DataImportance data_importance,
                   ThreadPriority app_priority,
                   NodeID owner)
{
    int ind = start_p_spm_addr / getPageSizeBytes();
    int ctr;
    for (ctr = 0; ctr < num_pages; ctr++) {
        assert (ctr+ind < getSPMSizePages());
        my_spm_ptr->spmSlots[ctr+ind].setUsed();
        my_spm_ptr->spmSlots[ctr+ind].setImportance(data_importance);
        my_spm_ptr->spmSlots[ctr+ind].setPriority(app_priority);
        my_spm_ptr->spmSlots[ctr+ind].setOwner(owner);
    }
}

bool
PMMU::sendRelocationDone(PacketPtr pkt)
{
    bool signal_required = pkt->govInfo.hasSignalee();

    MachineID dest;
    dest.num = pkt->govInfo.getSignalee();
    dest.type = MachineType_PMMU;

    if (signal_required){

        DPRINTF(PMMU, "Node %d: SPMResponseType_RELOCATION_DONE to node %d \n",
                getNodeID(), pkt->govInfo.getSignalee());

        //SPMResponseMsg *msg = new SPMResponseMsg(clockEdge());
        std::shared_ptr<SPMResponseMsg> msg = std::make_shared<SPMResponseMsg>(clockEdge());

        pkt->spmInfo.setOrigin(getNodeID());
        msg->m_PktPtr = pkt;
        (msg->m_Destination).add(dest);
        msg->m_Sender = m_machineID;
        msg->m_Type = SPMResponseType_RELOCATION_DONE;
        msg->m_MessageSize = MessageSizeType_Control;

        m_responseToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    return signal_required;
}

void
PMMU::fireupPendingAllocs(PacketPtr pkt)
{
    // for all of the local alloc reqs, if their current_user, matches previous_owner, then make them active

    NodeID signalee = pkt->spmInfo.getOrigin();
    Addr p_spm_addr = pkt->govInfo.getSPMAddress();

    unsigned int size = m_requestToSPM_ptr->getSize(curTick());
    unsigned int it = 0;

    unsigned int success = 0;
    for (it=0; (it < size); it++){

        SPMRequestMsg* msg = const_cast<SPMRequestMsg *>(dynamic_cast<const SPMRequestMsg *>(m_requestToSPM_ptr->peekNthMsg(it)));

        if (msg->m_PktPtr->govInfo.getSignaler() == signalee &&
            msg->m_PktPtr->govInfo.getSPMAddress() == p_spm_addr) {
            msg->m_PktPtr->govInfo.shouldNotWait();
            success++;
        }
    }

    DPRINTF(PMMU, "Node %d: Firing up %d pending allocate(s) triggered by node %d\n", getNodeID(), success, signalee);

    delete pkt->req;
    delete pkt;
}

const SPMRequestMsg*
PMMU::getNextReadyRequestMsg(MessageBuffer *mb) const
{

    unsigned int size = mb->getSize(curTick());
    unsigned int it = 0;
    const SPMRequestMsg* msg = NULL;

    for (it=0; (it < size) && mb->isReady(curTick()); it++){

        msg = (dynamic_cast<const SPMRequestMsg *>(((*mb)).peek()));
        MsgPtr msg_ptr = mb->peekMsgPtr();

        assert(msg->m_PktPtr->isRequest());

        if (!msg->m_PktPtr->govInfo.hasSignaler()) {
            return (msg);
        }

//      Tick enq_time = 0;
//      enq_time = msg_ptr->getLastEnqueueTime();

        mb->dequeue(curTick());
        mb->enqueue(msg_ptr, clockEdge(), cyclesToTicks(Cycles(1)));

//      msg_ptr->setLastEnqueueTime(enq_time);

    }

    DPRINTF(PMMU, "Node %d: Ready request not found in %s\n", getNodeID(), mb->name());
    return nullptr;
}

void
PMMU::finalizeLocalGOVReq(PacketPtr gov_pkt)
{

    if (gov_pkt->govInfo.shouldStall()) {
        pending_gov_reqs--;
    }
    if (pending_gov_reqs == 0 && my_spm_ptr->isBlocked()) {
        my_spm_ptr->clearBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
    }

    Addr start_v_page_addr = gov_pkt->govInfo.getVirtualAddress();

    if (gov_pkt->govInfo.isAllocate()){
        // if we've not been kicked out by the owner while we were busy allocating the slot on-chip
        if (my_att.hasMapping(start_v_page_addr)){

            DPRINTF(PMMU, "Node %d: Finalizing allocation\n", getNodeID());

            ATTEntry *translation = my_att.getMapping(start_v_page_addr);
            assert (translation);

            my_att.validateATTEntry(translation);
        }
    }
    else if (gov_pkt->govInfo.isDeallocate()) {
        DPRINTF(PMMU, "Node %d: Finalizing deallocation\n", getNodeID());
    }
    else if (gov_pkt->govInfo.isRelocationWrite()) {
        DPRINTF(PMMU, "Node %d: Finalizing relocation\n", getNodeID());
    }
    else {
        panic("Unknown packet type received!");
    }

    bool signal_required = sendRelocationDone(gov_pkt);

    if (!signal_required){
//      pkt->deleteData(); // FIXME: deleteData() should be called?
        delete gov_pkt->req;
        delete gov_pkt;
    }
}

////////////////////
//
// Message Processing helpers
//
////////////////////

bool
PMMU::processRemoteRequestMsg(bool *sending_mem_reqs_allowed)
{
    if (*sending_mem_reqs_allowed) {
        const SPMRequestMsg* in_msg_ptr = getNextReadyRequestMsg (m_requestToSPM_ptr);

        if(in_msg_ptr){

            PacketPtr reqPacket = in_msg_ptr->m_PktPtr;
            assert(reqPacket->isRequest());

            if ((in_msg_ptr->getType() == SPMRequestType_ALLOC) && (in_msg_ptr->m_PktPtr->govInfo.hasSignaler())) {
                // do nothing
                DPRINTF(PMMU, "Node %d: SPMRequestType_ALLOC should wait for node %d\n",
                        getNodeID(), in_msg_ptr->m_PktPtr->govInfo.getSignaler());
                return false;
            }
            else  if (in_msg_ptr->getType() == SPMRequestType_ALLOC) {
                DPRINTF(PMMU, "Node %d: SPMRequestType_ALLOC from node %d\n",
                        getNodeID(), reqPacket->spmInfo.getOrigin());
                if (reqPacket->govInfo.getAllocationMode() == UNINITIALIZE) {
                    reqPacket->makeResponse();
                    reqPacket->govInfo.markComplete();
                    generateGovRespMsg(reqPacket);
                }
                else {
                    // we shouldn't forward any other memory request to spm
                    my_spm_ptr->conditionalBlocking(BaseSPM::Blocked_MaxPendingReqs);
                    *sending_mem_reqs_allowed = false;
                    spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
                }
            }
            else if (in_msg_ptr->getType() == SPMRequestType_DEALLOC) {
                DPRINTF(PMMU, "Node %d: SPMRequestType_DEALLOC from node %d\n",
                        getNodeID(), reqPacket->spmInfo.getOrigin());
                if (reqPacket->govInfo.getDeallocationMode() == DISCARD) {
                    reqPacket->makeResponse();
                    reqPacket->govInfo.markComplete();
                    generateGovRespMsg(reqPacket);
                }
                else {
                    // we shouldn't forward any other memory request to spm
                    my_spm_ptr->conditionalBlocking(BaseSPM::Blocked_MaxPendingReqs);
                    *sending_mem_reqs_allowed = false;
                    spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
                }
            }
            else if (in_msg_ptr->getType() == SPMRequestType_RELOCATE_READ) {
                DPRINTF(PMMU, "Node %d: SPMRequestType_RELOCATE_READ from node %d\n",
                        getNodeID(), reqPacket->spmInfo.getOrigin());
                spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
            }
            else if (in_msg_ptr->getType() == SPMRequestType_RELOCATE_WRITE) {
                DPRINTF(PMMU, "Node %d: SPMRequestType_RELOCATE_WRITE from node %d\n",
                        getNodeID(), reqPacket->spmInfo.getOrigin());
                spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
            }
            else {
                DPRINTF(PMMU, "Node %d: SPMRequestType_READ/WRITE from node %d\n",
                        getNodeID(), reqPacket->spmInfo.getOrigin());

                // spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
                // Epiphany: accessing remote SPM, reading data, and sending it back through the network
                Cycles lat;
                my_spm_ptr->satisfySPMEpiphanyAccess(reqPacket, &lat, false);
                if (reqPacket->isWrite()) {
                	DPRINTF(fiveG, "Remote request packet is write addr: %x\n", reqPacket->getAddr());
                	delete reqPacket->req;
                	delete reqPacket;
                }
                else {
                	reqPacket->saveCmd();
                	reqPacket->makeResponse();
                	generateAccessRespMsg(reqPacket);
                }
            }

            m_requestToSPM_ptr->dequeue(clockEdge());
            return true;
        }
    }
    return false;
}

bool
PMMU::processRemoteRequestMsg_Networks(int netId)
{

	const SPMRequestMsg* in_msg_ptr = getNextReadyRequestMsg (m_net_spm_ptr[netId]);
    PacketPtr reqPacket = in_msg_ptr->m_PktPtr;
    assert(reqPacket->isRequest());

    DPRINTF(PMMU, "Node %d: SPMRequestType_READ/WRITE from node %d\n",
            getNodeID(), reqPacket->spmInfo.getOrigin());

    // spmMasterPort->schedTimingReq(reqPacket, curTick()+1);
    // Epiphany: accessing remote SPM, reading data, and sending it back through the network
    Cycles lat;
    my_spm_ptr->satisfySPMEpiphanyAccess(reqPacket, &lat, false);
    if (reqPacket->isWrite()) {
    	DPRINTF(fiveG, "Remote request packet is write addr: %x\n", reqPacket->getAddr());
    	delete reqPacket->req;
    	delete reqPacket;
    }
    else {
    	DPRINTF(fiveG, "Remote request packet is write addr: %x isRead: %d\n", reqPacket->getAddr(), reqPacket->isRead());
    	assert (0); // Can there be a remote read request in 5G?
    	reqPacket->saveCmd();
    	reqPacket->makeResponse();
    	generateAccessRespMsg(reqPacket);
    }
    m_net_spm_ptr[netId]->dequeue(clockEdge());
    return true;
}

bool
PMMU::processLocalRequestMsg(bool *sending_mem_reqs_allowed)
{
    const SPMRequestMsg* in_msg_ptr = dynamic_cast<const SPMRequestMsg *>(((*m_requestFromSPM_ptr)).peek());
    assert(in_msg_ptr != NULL);
    MsgPtr msg_ptr = m_requestFromSPM_ptr->peekMsgPtr();

    PacketPtr req_packet = in_msg_ptr->m_PktPtr;

    if (in_msg_ptr->m_PktPtr->spmInfo.isLocal()) {
        assert(req_packet->needsResponse());
        req_packet->saveCmd();
        req_packet->makeResponse(); // This should be makeResponse because we don't need to fwd it to main memory
        DPRINTF(PMMU, "Trace: %s Received: %x Sending back to local SPM\n", __func__, req_packet->req->getVaddr());
        DPRINTF(SPM, "%s req_packet.addr %x req_packet.isLLSC %d req_packet.origCmd.isLLSC %d\n", __func__, req_packet->getAddr(), req_packet->isLLSC(), req_packet->origCmd.isLLSC());
        spmSlavePort->schedTimingResp(req_packet, curTick()+1);
        m_requestFromSPM_ptr->dequeue(clockEdge());
    }
    else if (in_msg_ptr->m_PktPtr->spmInfo.isRemote()){
    	DPRINTF(PMMU, "Trace: %s Enqueue remote request: %x\n", __func__, req_packet->req->getVaddr());
        m_requestFromSPM_ptr->dequeue(clockEdge());
        m_requestToNetwork_ptr->enqueue(msg_ptr, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else if (in_msg_ptr->m_PktPtr->spmInfo.isOffchip() && *sending_mem_reqs_allowed){
        my_spm_ptr->conditionalBlocking(BaseSPM::Blocked_MaxPendingReqs);

        assert(req_packet->needsResponse());
        req_packet->saveCmd();
        req_packet->makeResponse(); // This should be makeShallowResponse because we need to fwd it to main memory
        DPRINTF(PMMU, "Trace: %s Received: %x Sending back to local SPM for it to send to memory \n", __func__, req_packet->req->getVaddr());
        spmSlavePort->schedTimingResp(req_packet, curTick()+1);
        m_requestFromSPM_ptr->dequeue(clockEdge());
    }
    return true;
}

bool
PMMU::processRemoteResponseMsg()
{
    const SPMResponseMsg* in_msg_ptr = (dynamic_cast<const SPMResponseMsg *>(((*m_responseToSPM_ptr)).peek()));
    assert(in_msg_ptr != NULL);

    if (in_msg_ptr->getType() == SPMResponseType_RELOCATION_DONE) {
        fireupPendingAllocs(in_msg_ptr->m_PktPtr);
    }
    else {
        PacketPtr resp_packet = in_msg_ptr->m_PktPtr;
        assert(resp_packet->isResponse());

        if (in_msg_ptr->getType() == SPMResponseType_GOV_ACK) {
            finalizeLocalGOVReq(resp_packet);
        }
        else {
            // the packet is already complete and can be sent to spm
            DPRINTF(PMMU, "Node %d: SPMResponseType_DATA/WRITE_ACK from node %d\n",
                    getNodeID(), in_msg_ptr->getSender().num);
            // spmSlavePort->schedTimingResp(resp_packet, curTick()+1);
            my_spm_ptr->satisfyRemoteSPMEpiphanyAccess(resp_packet);
        }
    }

    m_responseToSPM_ptr->dequeue(clockEdge());
    return true;
}

bool
PMMU::processLocalResponseMsg()
{
    const SPMResponseMsg* in_msg_ptr = dynamic_cast<const SPMResponseMsg *>(((*m_responseFromSPM_ptr)).peek()); // making a copy because peek returns const message but we need non-const to modify and enqueue
    assert(in_msg_ptr != NULL);
    MsgPtr msg_ptr = m_responseFromSPM_ptr->peekMsgPtr();

    PacketPtr resp_packet = in_msg_ptr->m_PktPtr;

    // local gov_ack
    if (in_msg_ptr->getType() == SPMResponseType_GOV_ACK && in_msg_ptr->m_PktPtr->spmInfo.getOrigin() == getNodeID()) {
        DPRINTF(PMMU, "Node %d: SPMResponseType_GOV_ACK\n", getNodeID());

        finalizeLocalGOVReq(resp_packet);
        m_responseFromSPM_ptr->dequeue(clockEdge());
    }
    else if (in_msg_ptr->getType() == SPMResponseType_RELOCATION_HALFWAY) { // first part of relocation
        DPRINTF(PMMU, "Node %d: SPMResponseType_RELOCATION_HALFWAY for node %d\n",
                getNodeID(), resp_packet->spmInfo.getOrigin());

        continuePageRelocationRequest(resp_packet);
        m_responseFromSPM_ptr->dequeue(clockEdge());
    }
    else { // remote gov_ack or remote access
        m_responseFromSPM_ptr->dequeue(clockEdge());
        m_responseToNetwork_ptr->enqueue(msg_ptr, clockEdge(), cyclesToTicks(Cycles(1)));
    }

    return true;
}

////////////////////
//
// Message Processing
//
////////////////////

void
PMMU::wakeup()
{
    // TODO: memory leak?

//  int counter = 0;
	DPRINTF(fiveG, "PMMU woke up: %lld\n", ticksToCycles(1000));
	if (block_till_cycle == ticksToCycles(curTick())) {
		DPRINTF(fiveG, "Releasing the Epiphany Block\n");
		my_spm_ptr->clearBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
	}

    bool sending_mem_reqs_allowed = my_spm_ptr->acceptingMemReqs();
    bool remote_response_msg_processed = false;
    bool remote_request_msg_stopped = false;
    bool local_request_msg_processed = false;
    bool local_response_msg_processed = false;

//  while (counter < m_transitions_per_cycle      &&
//         (((*m_requestToSPM_ptr).isReady()  && my_spm_ptr->acceptingMemReqs() )||
//         (*m_responseToSPM_ptr).isReady()  ||
//         (*m_requestFromSPM_ptr).isReady() ||
//         (*m_responseFromSPM_ptr).isReady())) {

        // case I: we have remote responses incoming from the network waiting
        if ((*m_responseToSPM_ptr).isReady(curTick())) {
            remote_response_msg_processed = processRemoteResponseMsg();

//          counter++;
//          continue;
        }

        // case II: we have remote requests incoming from the network waiting
        // send packet to spm to read/write via sendTimingReq()
        if ((*m_requestToSPM_ptr).isReady(curTick())) {
            remote_request_msg_stopped = processRemoteRequestMsg(&sending_mem_reqs_allowed);

//          counter++;
//          continue;
        }



        // 5G: iterate over multiple networks
        for (unsigned i = 0; i < NUM_NETWORKS; i++) {
        	if ((*m_net_spm_ptr[i]).isReady(curTick())) {
        		processRemoteRequestMsg_Networks(i);
        	}
        }

        // case III: we have requests from the spm (cpu) waiting
        if ((*m_requestFromSPM_ptr).isReady(curTick()) ) {
            local_request_msg_processed = processLocalRequestMsg(&sending_mem_reqs_allowed);

//          counter++;
//          continue;
        }

        // case IV: we have response from the local spm waiting to be forwarded to remote nodes
        // simply forward this to original requester node on network via m_responseToNetwork_ptr
        if ((*m_responseFromSPM_ptr).isReady(curTick())) {
            local_response_msg_processed = processLocalResponseMsg();

//          counter++;
//          continue;
        }
//  } // end while

    // reschedule myself when i can't process all messages
    if ( (m_requestToSPM_ptr->isReady(curTick())    && !remote_request_msg_stopped)  ||
         (m_responseToSPM_ptr->isReady(curTick())   && remote_response_msg_processed) ||
         (m_requestFromSPM_ptr->isReady(curTick())  && local_request_msg_processed)   ||
         (m_responseFromSPM_ptr->isReady(curTick()) && local_response_msg_processed)
        ) {

        scheduleEvent(Cycles(1));
    }

}

////////////////////
//
// SPM-Side Req/Resp Handlers
//
////////////////////

bool
PMMU::generateAccessReqMsg(PacketPtr pkt)
{
    assert(pkt->isRequest());

    if (pkt->req->isCondSwap()) {
    	DPRINTF(SPM_DEBUG, "pkt.addr %x request is CondSwap()\n", pkt->getAddr());
    }
    else if (pkt->cmd == MemCmd::SwapReq) {
    	DPRINTF(SPM_DEBUG, "pkt.addr %x cmd is SwapReq\n", pkt->getAddr());
    }
    else if (pkt->isLLSC() && pkt->isWrite()) {
    	DPRINTF(SPM_DEBUG, "pkt.addr %x is LLSC() and isWrite()\n", pkt->getAddr());
    }
    else if (pkt->isLLSC() && pkt->isRead()) {
    	DPRINTF(SPM_DEBUG, "pkt.addr %x is LLSC() and isRead()\n", pkt->getAddr());
    }

    // enhance packet with spm stuff
    pkt->spmInfo.setOrigin(getNodeID());
    Addr req_v_page_addr = spmPageAlign(pkt->req->getVaddr());
    bool att_hit = my_att.isHit(req_v_page_addr);
    bool global_att_hit = getGovernor()->global_att.isHit(req_v_page_addr);
    if (global_att_hit) {
        // DPRINTF (ATT, "Node %d, Global ATT hit for virtual page: %x\n", getNodeID(), pkt->req->getVaddr());
    }

    ATTEntry *translation = NULL;
    Addr spm_p_addr = 0;

    if (att_hit) {

        translation = my_att.getMapping(req_v_page_addr);
        spm_p_addr = translation->spm_slot_addr | pkt->getOffset(getPageSizeBytes());  //TODO:  needs to be block size
        pkt->setAddr(spm_p_addr); // modify packet to indicate physical spm location
        DPRINTF (ATT, "Node %d, ATT hit for virtual page: %x spm_p_addr %x pkt.isLLSC %d\n", getNodeID(), pkt->req->getVaddr(), spm_p_addr, pkt->isLLSC());
        DPRINTF(PMMU, "Trace: %s Received: %x ATT hit\n", __func__, pkt->req->getVaddr());
        // if on local spm
        if (translation->destination_node.num == getNodeID()) {
            pkt->spmInfo.makeLocal();
        }
        else {  // else it's remote
            pkt->spmInfo.makeRemote();
        }
    }
    else if (global_att_hit) {
        translation = getGovernor()->global_att.getMapping(req_v_page_addr);
        spm_p_addr = translation->spm_slot_addr | pkt->getOffset(getPageSizeBytes());  //TODO:  needs to be block size
        pkt->setAddr(spm_p_addr); // modify packet to indicate physical spm location
        DPRINTF (ATT, "Node %d, Global ATT hit for virtual page: %x in Node %d spm_p_addr %x pkt.isWrite %d\n", getNodeID(), pkt->req->getVaddr(), translation->destination_node.num, spm_p_addr, pkt->isWrite());

        // if on local spm
        if (translation->destination_node.num == getNodeID()) {
            pkt->spmInfo.makeLocal();
        }
        else {  // else it's remote
            pkt->spmInfo.makeRemote();
        }
    }
    else {
        DPRINTF (ATT, "Node %d, ATT miss for virtual page: %x\n", getNodeID(), pkt->req->getVaddr());
        // otherwise it should be accessed from off-chip mem
        DPRINTF(PMMU, "Trace: %s Received: %x Sent to off-chip\n", __func__, pkt->req->getVaddr());
        pkt->spmInfo.makeOffchip();
    }

    // create message
    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_PktPtr = pkt;
    msg->m_Requestor = getMachineID();
    msg->m_Type = pkt->isRead() ? SPMRequestType_READ : SPMRequestType_WRITE;
    msg->m_MessageSize = MessageSizeType_Data;
    if (att_hit)
    	(msg->m_Destination).add(att_hit ? translation->destination_node : getMachineID());
    else if (global_att_hit)
    	(msg->m_Destination).add(global_att_hit ? translation->destination_node : getMachineID());
    else
    	(msg->m_Destination).add(att_hit ? translation->destination_node : getMachineID());
    msg->m_Addr = pkt->spmInfo.isOffchip() ? pkt->req->getVaddr() : spm_p_addr;

    // TODO: Raghu: Fix it
    DPRINTF (SPM, "%s m_PktPtr.addr %x m_PktPtr.isLLSC %d pkt.isLLSC %d\n", __func__, msg->m_PktPtr->getAddr(), msg->m_PktPtr->isLLSC(), pkt->isLLSC());

    m_requestFromSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    return true;
}

bool
PMMU::generateRemoteReqMsgEpiphany(PacketPtr pkt, unsigned id)
{
    assert(pkt->isRequest());
    assert(getNodeID() != id);
    MachineID dest_node;
    dest_node.type = MachineType_PMMU;
    dest_node.num = id;

    // enhance packet with spm stuff
    pkt->spmInfo.setOrigin(getNodeID());
    pkt->spmInfo.makeRemote();

    // create message
    std::shared_ptr<SPMRequestMsg> msg = std::make_shared<SPMRequestMsg>(clockEdge());
    msg->m_PktPtr = pkt;
    msg->m_Requestor = getMachineID();
    msg->m_Type = pkt->isRead() ? SPMRequestType_READ : SPMRequestType_WRITE;
    msg->m_MessageSize = MessageSizeType_Data;
    (msg->m_Destination).add(dest_node);
    msg->m_Addr = pkt->req->getVaddr();
    DPRINTF (SPM, "%s m_PktPtr.addr %x m_PktPtr.isLLSC %d pkt.isLLSC %d\n", __func__, msg->m_PktPtr->getAddr(), msg->m_PktPtr->isLLSC(), pkt->isLLSC());

    /*
    m_requestFromSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));

    m_requestFromSPM_ptr->dequeue(clockEdge());
    */

    // 5G
    // m_requestToNetwork_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    unsigned netId = getBankNetworkId(pkt->req->getVaddr());
    // unsigned netId = 0;
    if (block_network_for_cycles == 0) {
    	assert (m_net_net_ptr[netId]->getSize(curTick()) == 0); // A message already in the buffer; not allowed in 5G
    	DPRINTF (PMMU, "%s m_PktPtr.addr %x m_PktPtr.isLLSC %d pkt.isLLSC %d block_network_for_cycles: %lld netId: %d\n", __func__, msg->m_PktPtr->getAddr(), msg->m_PktPtr->isLLSC(), pkt->isLLSC(), block_network_for_cycles, netId);
    	m_net_net_ptr[netId]->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    }
    else {
    	assert (m_net_net_ptr[netId]->getSize(curTick()) == 0); // A message already in the buffer; not allowed in 5G
    	DPRINTF (PMMU, "%s m_PktPtr.addr %x m_PktPtr.isLLSC %d pkt.isLLSC %d block_network_for_cycles: %lld netId: %d\n", __func__, msg->m_PktPtr->getAddr(), msg->m_PktPtr->isLLSC(), pkt->isLLSC(), block_network_for_cycles, netId);
    	m_net_net_ptr[netId]->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(block_network_for_cycles)));
    	// block_network_for_cycles = 0;
    }

    return true;
}

Addr
PMMU::getBankNetworkId(Addr addr)
{
	// 5G: TODO: fill this up
	int core_id = getNodeID();
	int vnet_id;

	Addr addr_temp;
	if(core_id <= 4) {
		addr_temp = addr >> (2 + 4);
		vnet_id = addr_temp & 0x7;
		printf("src = %d, dest_addr = %#x, vnet_id = %d\n", core_id, addr, vnet_id);
		return addr_temp & 0x1f;
	}
	return 31;
}

bool
PMMU::generateAccessRespMsg(PacketPtr pkt)
{
    // enhance packet with spm stuff
    // none!

    // create message
    std::shared_ptr<SPMResponseMsg> msg = std::make_shared<SPMResponseMsg>(clockEdge());
    msg->m_PktPtr = pkt; // pkt should already be a response packet from spm
    msg->m_Sender = getMachineID();
    MachineID req_owner;
    req_owner.num = pkt->spmInfo.getOrigin();
    req_owner.type = MachineType_PMMU;
    (msg->m_Destination).add(req_owner);
    msg->m_Type = pkt->isRead() ? SPMResponseType_DATA : SPMResponseType_WRITE_ACK;
    msg->m_MessageSize = (msg->m_Type == SPMResponseType_DATA) ? MessageSizeType_Data : MessageSizeType_Control;
    m_responseFromSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    return true;
}

bool
PMMU::generateGovRespMsg(PacketPtr pkt)
{
    // enhance packet with spm stuff
    // none!

    // create message
    std::shared_ptr<SPMResponseMsg> msg = std::make_shared<SPMResponseMsg>(clockEdge());
    msg->m_PktPtr = pkt; // pkt should already be a response packet from spm
    msg->m_Sender = getMachineID();
    MachineID req_owner;
    req_owner.num = pkt->spmInfo.getOrigin();
    req_owner.type = MachineType_PMMU;
    (msg->m_Destination).add(req_owner);

    if (pkt->govInfo.isComplete()) {
        msg->m_Type = SPMResponseType_GOV_ACK;
        if (pkt->govInfo.isDeallocate() && !pkt->govInfo.hasSignalee()) {
            setFreePages(pkt->govInfo.getSPMAddress(), 1); // TODO: Always one slot?
        }
    }
    else if (pkt->govInfo.isRelocationRead()) {
        msg->m_Type = SPMResponseType_RELOCATION_HALFWAY;
        // TODO: making the page free overrides the fact that already set it to occupied for the next user in the gov
        if (!pkt->govInfo.hasSignalee()) {
            setFreePages(pkt->govInfo.getSPMAddress(), 1); // TODO: Always one slot?
        }
    }
    else if (pkt->govInfo.isRelocationWrite()) {
        msg->m_Type = SPMResponseType_GOV_ACK;
    }
    else {
        panic("SPM governor request returned incomplete");
    }

    msg->m_MessageSize = MessageSizeType_Control;
    m_responseFromSPM_ptr->enqueue(msg, clockEdge(), cyclesToTicks(Cycles(1)));
    return true;
}

bool
PMMU::recvSPMTimingReq(PacketPtr pkt)
{
    return generateAccessReqMsg(pkt);
}


bool
PMMU::recvSPMTimingResp(PacketPtr pkt)
{
    if (pkt->govInfo.isGovReq()) {
        return generateGovRespMsg(pkt);
    }
    else {
        return generateAccessRespMsg(pkt);
    }
}

////////////////////
//
// SPMSideSlavePort
//
////////////////////

PMMU::SPMSideSlavePort::SPMSideSlavePort(const std::string &_name, PMMU *_pmmu, PortID id)
    : QueuedSlavePort(_name, NULL, _respQueue, id),
      _respQueue(*_pmmu, *this),
      my_pmmu(_pmmu)
{
}

bool
PMMU::SPMSideSlavePort::recvTimingReq(PacketPtr pkt)
{
    /*
     * This is called by local spm when there is a new mem request from local cpu
     */
	DPRINTF(PMMU, "Trace: %s Received: %x\n", __func__, pkt->req->getVaddr());
    return my_pmmu->recvSPMTimingReq(pkt);
}

void
PMMU::SPMSideSlavePort::recvFunctional(PacketPtr pkt)
{
    //@TODO:  implement - see rubyport.cc version
}

////////////////////
//
// SPMSideMasterPort
//
////////////////////

PMMU::SPMSideMasterPort::SPMSideMasterPort(const std::string &_name, PMMU *_pmmu, PortID id)
    : QueuedMasterPort(_name, _pmmu, _reqQueue, _snoopRespQueue, id),
      _reqQueue(*_pmmu, *this),
      _snoopRespQueue(*my_pmmu, *this, ""),
      my_pmmu(_pmmu)
{
}

bool
PMMU::SPMSideMasterPort::recvTimingResp(PacketPtr pkt)
{
    /*
     * This is called by local spm in response to a request made by a remote pmmu
     */
    return my_pmmu->recvSPMTimingResp(pkt);
}

void
PMMU::incrStats(SpmStats stat)
{
	spm_accesses++;
	if (stat == SPM_STACK_HIT) {
		spm_stack_hits++;
		spm_hits++;
	}
	else if (stat == SPM_HIT)
		spm_hits++;
	else if (stat == SPM_MISS)
		spm_misses++;
}

void
PMMU::blockEpiphany(uint64_t blockCycles, uint64_t block_mode)
{
	if (static_cast<BlockModes>(block_mode) == BLOCK_FOR_X_CYCLES) {
		block_till_cycle = ticksToCycles(curTick()) + blockCycles;
		scheduleEvent(Cycles(blockCycles));
		my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
	}
	else if (static_cast<BlockModes>(block_mode) == BLOCK_TILL_X_CYCLES) {
		DPRINTF(MARKER, "---- marker for BLOCK_TILL_X_CYCLES ---- blockCycles: %lld cpuid: %d curCycle: %lld\n", blockCycles, getNodeID(), ticksToCycles(curTick()));
		assert (blockCycles > ticksToCycles(curTick()));
		block_till_cycle = blockCycles;
		blockCycles = blockCycles - ticksToCycles(curTick());
		scheduleEvent(Cycles(blockCycles));
		my_spm_ptr->setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
	}
	else if (static_cast<BlockModes>(block_mode) == BLOCK_INJECTION_X_CYCLES) {
		block_network_for_cycles = blockCycles;
	}
	else if (static_cast<BlockModes>(block_mode) == PRINT_MARKER_START) {
		execTimeList.insert(std::make_pair(blockCycles, ticksToCycles(curTick())));
	}
	else if (static_cast<BlockModes>(block_mode) == PRINT_MARKER_END) {
		std::map<uint64_t, uint64_t>::iterator it;
		it = execTimeList.find(blockCycles);
		if (it != execTimeList.end()) {
			DPRINTF(MARKER, "---- marker exec time ---- %lld\n", (ticksToCycles(curTick()) - it->second));
			execTimeList.erase(it);
		}
		else {
			// panic ("Marker was not initialized\n");
			DPRINTF(MARKER, "---- marker: start not called ---- %lld\n", blockCycles);
		}
	}
	else if (static_cast<BlockModes>(block_mode) == PRINT_MARKER) {
		DPRINTF(MARKER, "---- marker ---- %lld\n", blockCycles);
	}
	else {
		assert (0);
	}
	DPRINTF(fiveG, "Set Epiphany Block blockCycles: %lld\n", blockCycles);
}
