#ifndef SPM_API_H_
#define SPM_API_H_

#include "spm_types.h"
#include "../../../../util/m5/m5op.h"

/*****************/

#define SPM_ARRAY_ALLOC(ARRAY_NAME, LENGTH, TYPE, ALLOC_MODE, DATA_IMPORTANCE, APP_PRIORITY) \
            SPM_ALLOC ((uint64_t)&ARRAY_NAME[0], \
                       (uint64_t)(&ARRAY_NAME[LENGTH-1] + sizeof(TYPE) - 1), \
                       ALLOC_MODE, DATA_IMPORTANCE, APP_PRIORITY);

#define SPM_ARRAY_FREE(ARRAY_NAME, LENGTH, TYPE, DEALLOC_MODE) \
            SPM_FREE  ((uint64_t)&ARRAY_NAME[0], \
                       (uint64_t)(&ARRAY_NAME[LENGTH-1] + sizeof(TYPE) - 1), \
                       DEALLOC_MODE);

/*****************/

static inline void SPM_ALLOC(uint64_t start_v_address,
               uint64_t end_v_address,
               AllocationModes alloc_mode,
               DataImportance d_importance,
               ThreadPriority app_priority)
{
    uint64_t modes = 0x0000000000000000;
    modes = modes | (uint64_t)alloc_mode;
    modes = modes | (uint64_t)d_importance << 4;
    modes = modes | (uint64_t)app_priority << 8;
    m5_spm_alloc(start_v_address, end_v_address, modes);
}

static inline void SPM_FREE(uint64_t start_v_address,
              uint64_t end_v_address,
              DeallocationModes dealloc_mode)
{
    uint64_t modes = 0x0000000000000000;
    modes = modes | (uint64_t)dealloc_mode;
    m5_spm_free(start_v_address, end_v_address, modes);
}

#endif /* SPM_API_H_ */
