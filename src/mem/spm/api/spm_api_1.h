#include <spm_api.h>

#define STACK_BASE __builtin_frame_address(0)

uint8_t **local_spm;

void spm_initialize(int num_threads)
{
	// VA range: 0x0 to 0xffffffff
	unsigned i;
	local_spm = (uint8_t **) malloc(sizeof(uint8_t*) * num_threads);
	// local_spm = new uint8_t[num_threads];
	uint64_t begin = 0xefffffff;
	for (i = 0; i < num_threads; i++) {
		// local_spm[i] = (uint8_t *) malloc(sizeof(uint8_t));
		begin -= 0x8000;
		local_spm[i] = begin + 1;
		//printf ("At spm_initialize local_spm[%d]: %x\n", i, local_spm[i]);
	}
}


static inline void spm_allocate(uint64_t startVAAddr, uint64_t startSPMAddr, unsigned size,
		AllocationModes alloc_mode)
{
	// uint64_t endVAAddr = startVAAddr + size - 1;
    // uint64_t modes = 0x0000000000000000;
    // modes = modes | (uint64_t)alloc_mode;
	// uint64_t modes = (uint64_t)alloc_mode;
	if (alloc_mode == UNINITIALIZE) {
		return;
	}
    uint64_t dummy;
    // printf ("startSPMAddr = %x\n", (unsigned long)startSPMAddr);
    m5_spm_alloc(startVAAddr, (uint64_t)size, dummy);
    m5_spm_alloc(startSPMAddr, alloc_mode, dummy);
}

static inline void spm_deallocate(uint64_t startVAAddr, uint64_t startSPMAddr, unsigned size,
              DeallocationModes dealloc_mode)
{
	// uint64_t endVAAddr = startVAAddr + size - 1;
    // uint64_t modes = 0x0000000000000000;
    // modes = modes | (uint64_t)dealloc_mode;
	// printf ("startVAAddr: %x startSPMAddr: %x\n", startVAAddr, startSPMAddr);
    uint64_t dummy;
    m5_spm_free(startVAAddr, size, dummy);
    m5_spm_free(startSPMAddr, dealloc_mode, dummy);
}

static inline void spm_block(uint64_t block_cycles, BlockModes block_mode)
{
	uint64_t dummy;
	m5_spm_block(block_cycles, block_mode, dummy);
}

/*
static inline void spm_allocate(uint64_t startVAAddr, uint64_t startSPMAddr, unsigned size,
               AllocationModes alloc_mode)
{
	uint64_t endVAAddr = startVAAddr + size - 1;
    uint64_t modes = 0x0000000000000000;
    modes = modes | (uint64_t)alloc_mode;
    uint64_t dummy;
    m5_spm_alloc(startVAAddr, (uint64_t)size, dummy);
    m5_spm_alloc(startSPMAddr, modes, dummy);
}

static inline void spm_deallocate(uint64_t startVAAddr, uint64_t startSPMAddr, unsigned size,
              DeallocationModes dealloc_mode)
{
	uint64_t endVAAddr = startVAAddr + size - 1;
    uint64_t modes = 0x0000000000000000;
    modes = modes | (uint64_t)dealloc_mode;
    uint64_t dummy;
    m5_spm_free(startVAAddr, (uint64_t)size, dummy);
    m5_spm_free(startSPMAddr, modes, dummy);
}
*/
