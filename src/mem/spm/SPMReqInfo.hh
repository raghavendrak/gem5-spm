#ifndef __SPMReqInfo_HH__
#define __SPMReqInfo_HH__

#include <limits>
#include "mem/ruby/common/TypeDefines.hh"

#define UINT_MAXIMUM std::numeric_limits<unsigned int>::max()

class SPMReqInfo
{
  friend class Packet;

  private:

    // List of all data locations associated with a request packet.
    enum PageLocation
    {
        Local_SPM = 0,
        Remote_SPM,
        Off_Chip,
        NUM_MEM_LOC
    };

    PageLocation loc;
    NodeID requesting_node;
    NodeID holder_node;

  public:

    SPMReqInfo() : loc(NUM_MEM_LOC),
                   requesting_node(UINT_MAXIMUM),
                   holder_node(UINT_MAXIMUM) {}

    const char *toString()
    {
        static const char * LocationStrings[] = { "Local SPM",
                                                  "Remote SPM",
                                                  "Off-Chip",
                                                  "Uninitialized" };
        return LocationStrings[loc];
    }

    void makeLocal()   { loc = Local_SPM; }
    void makeRemote()  { loc = Remote_SPM; }
    void makeOffchip() { loc = Off_Chip; }

    bool isLocal() const                 { return loc == Local_SPM; }
    bool isRemote() const                { return loc == Remote_SPM; }
    bool isOffchip() const               { return loc == Off_Chip; }
    PageLocation getPageLocation() const { return loc; }

    void setOrigin (NodeID _requesting_node) { requesting_node = _requesting_node; }
    void setHolder (NodeID _holder_node)     { holder_node = _holder_node; }

    NodeID getOrigin ()
    {
        assert(requesting_node != UINT_MAXIMUM);
        return requesting_node;
    }

    NodeID getHolder ()
    {
        assert(holder_node != UINT_MAXIMUM);
        return holder_node;
    }

    bool operator==(SPMReqInfo c2) const { return (loc == c2.loc); }
    bool operator!=(SPMReqInfo c2) const { return (loc != c2.loc); }
};

#endif //__SPMReqInfo_HH__
