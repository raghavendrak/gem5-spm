/*
 * Copyright (c) 2010-2015 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2002-2005 The Regents of The University of Michigan
 * Copyright (c) 2010,2015 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Erik Hallnor
 *          Dave Greene
 *          Nathan Binkert
 *          Steve Reinhardt
 *          Ron Dreslinski
 *          Andreas Sandberg
 */

/**
 * @file
 * SPM definitions.
 */
#include <vector>
#include "cpu/base.hh"
#include "cpu/thread_context.hh"
#include "sim/system.hh"

#include "base/misc.hh"
#include "base/types.hh"
#include "debug/SPM.hh"
#include "debug/SPMPort.hh"
#include "mem/spm/spm_class/spm.hh"
#include "mem/spm/pmmu.hh"
#include "sim/sim_exit.hh"
#include "debug/HM.hh"

SPM::SPM(const SPMParams *p)
    : BaseSPM(p, p->system->cacheLineSize()),
      pendingReqs(0)
{
    cpuSidePort = new CpuSidePort(p->name + ".cpu_side", this,
                                  "CpuSidePort");
    memSidePort = new MemSidePort(p->name + ".mem_side", this,
                                  "MemSidePort");

    pmmuSlavePort = new PMMUSideSlavePort(p->name + ".pmmu_s_side", this, 1);
    pmmuMasterPort = new PMMUSideMasterPort(p->name + ".pmmu_m_side", this, 0);
}

void
SPM::init()
{
    myPMMU = dynamic_cast<PMMU*>(pmmuSlavePort->getMasterPort().getOwner());
    assert(myPMMU);

    myPMMU->setSPM(this);
    pageSizeBytes = myPMMU->getPageSizeBytes();
    spmSlots = new SPMPage[size/pageSizeBytes];
    myVAM = new VAM(myPMMU->getNodeID(), size, myPMMU->numSPMs);
    data = new uint8_t[size];
    //printf ("data = %x\n", data);
}

SPM::~SPM()
{
    delete cpuSidePort;
    delete memSidePort;

    delete [] spmSlots;
    delete [] data;
}

void
SPM::regStats()
{
    BaseSPM::regStats();
}

/////////////////////////////////////////////////////
//
// Access path: requests coming in from the CPU side
//
/////////////////////////////////////////////////////

class ForwardResponseRecord : public Packet::SenderState
{
  public:

    ForwardResponseRecord() {}
};

void
SPM::doWritebacks(PacketList& writebacks, Tick forward_time)
{
    panic("doWritebacks()");
}

void
SPM::doWritebacksAtomic(PacketList& writebacks)
{
    panic("doWritebacksAtomic()");
}


void
SPM::recvTimingSnoopResp(PacketPtr pkt)
{
    panic("recvTimingSnoopResp()");
}

Tick
SPM::recvAtomic(PacketPtr pkt)
{
    panic("recvAtomic()");
    return 0;
}


void
SPM::functionalAccess(PacketPtr pkt, bool fromCpuSide)
{
    if (system->bypassCaches()) {
        // Packets from the memory side are snoop request and
        // shouldn't happen in bypass mode.
        assert(fromCpuSide);

        // The spm should be flushed if we are in spm bypass mode,
        // so we don't need to check if we need to update anything.
        memSidePort->sendFunctional(pkt);
        return;
    }

    bool done = cpuSidePort->checkFunctional(pkt)
        || memSidePort->checkFunctional(pkt);

    // We're leaving the spm, so pop spm->name() label
    pkt->popLabel();

    if (done) {
        pkt->makeResponse();
    } else {
        // if it came as a request from the CPU side then make sure it
        // continues towards the memory side
        if (fromCpuSide) {
            memSidePort->sendFunctional(pkt);
        } else if (forwardSnoops && cpuSidePort->isSnooping()) {
            // if it came from the memory side, it must be a snoop request
            // and we should only forward it if we are forwarding snoops
            cpuSidePort->sendFunctionalSnoop(pkt);
        }
    }
}

/////////////////////////////////////////////////////
//
// Snoop path: requests coming in from the memory side
//
/////////////////////////////////////////////////////

void
SPM::recvTimingSnoopReq(PacketPtr pkt)
{
    return;
    panic("recvTimingSnoopReq()");
}

bool
SPM::CpuSidePort::recvTimingSnoopResp(PacketPtr pkt)
{
    // Express snoop responses from master to slave, e.g., from L1 to L2
    spm->recvTimingSnoopResp(pkt);
    return true;
}

Tick
SPM::recvAtomicSnoop(PacketPtr pkt)
{
    panic("recvAtomicSnoop()");
    return 0;
}

///////////////
//
// CpuSidePort
//
///////////////

AddrRangeList
SPM::CpuSidePort::getAddrRanges() const
{
    return spm->getAddrRanges();
}

bool
SPM::CpuSidePort::recvTimingReq(PacketPtr pkt)
{
    DPRINTF(SPM, "Trace: %s Received: %x size: %d\n", __func__, pkt->req->getVaddr(), pkt->getSize());
	assert(!spm->system->bypassCaches());

    bool success = false;

    // always let inhibited requests through, even if blocked,
    // ultimately we should check if this is an express snoop, but at
    // the moment that flag is only set in the spm itself
    if (pkt->isExpressSnoop()) {
        // do not change the current retry state
        bool M5_VAR_USED bypass_success = spm->recvTimingReq(pkt);
        assert(bypass_success);
        return true;
    } else if (blocked || mustSendRetry) {
        // either already committed to send a retry, or blocked
        success = false;
    } else {
        // pass it on to the spm, and let the spm decide if we
        // have to retry or not
        success = spm->recvTimingReq(pkt);
    }

    // remember if we have to retry
    mustSendRetry = !success;
    return success;
}

Tick
SPM::CpuSidePort::recvAtomic(PacketPtr pkt)
{
    return spm->recvAtomic(pkt);
}

void
SPM::CpuSidePort::recvFunctional(PacketPtr pkt)
{
    // functional request
    spm->functionalAccess(pkt, true);
}

SPM::
CpuSidePort::CpuSidePort(const std::string &_name, SPM *_spm,
                         const std::string &_label)
    : BaseSPM::SPMSlavePort(_name, _spm, _label), spm(_spm)
{
}

SPM*
SPMParams::create()
{
    return new SPM(this);
}
///////////////
//
// MemSidePort
//
///////////////

bool
SPM::MemSidePort::recvTimingResp(PacketPtr pkt)
{
    spm->recvTimingResp(pkt);
    return true;
}

// Express snooping requests to memside port
void
SPM::MemSidePort::recvTimingSnoopReq(PacketPtr pkt)
{
    // handle snooping requests
    spm->recvTimingSnoopReq(pkt);
}

Tick
SPM::MemSidePort::recvAtomicSnoop(PacketPtr pkt)
{
    return spm->recvAtomicSnoop(pkt);
}

void
SPM::MemSidePort::recvFunctionalSnoop(PacketPtr pkt)
{
    // functional snoop (note that in contrast to atomic we don't have
    // a specific functionalSnoop method, as they have the same
    // behaviour regardless)
    spm->functionalAccess(pkt, false);
}

SPM::
MemSidePort::MemSidePort(const std::string &_name, SPM *_spm,
                         const std::string &_label)
    : BaseSPM::SPMMasterPort(_name, _spm, _reqQueue, _snoopRespQueue),
      _reqQueue(*_spm, *this, _snoopRespQueue, _label),
      _snoopRespQueue(*_spm, *this, _label), spm(_spm)
{
}

/////////////////////////////////////////////////////
//
// Response handling: responses from the memory side
//
/////////////////////////////////////////////////////

void
SPM::recvTimingResp(PacketPtr pkt)
{
    if (pkt->spmInfo.isOffchip()) {
        // unblock the cpu port
    	DPRINTF(SPM, "Trace: from offchip %s pktAddr %x data: %d\n", __func__, pkt->getAddr(), pkt->getData());
        conditionalUnblocking(Blocked_MaxPendingReqs);
        sendTimingRespToCPU(pkt, responseLatency);
    }
    else if (pkt->govInfo.isAllocate()) {
        // unblock the cpu port
        conditionalUnblocking(Blocked_MaxPendingReqs);
        // satisfyPageAllocation (pkt);
        finishPageAllocEpiphany(pkt);
    }
    else if (pkt->govInfo.isDeallocate()) {
        // unblock the cpu port
        conditionalUnblocking(Blocked_MaxPendingReqs);
        // satisfyPageDeallocation(pkt);
        finishPageDeallocEpiphany(pkt);
    }
    else {
        panic ("Invalid packet type received from memory.");
    }
}

/////////////////////
//
// SPM-related methods
//
/////////////////////

bool
SPM::recvTimingReq(PacketPtr pkt)
{
    //@TODO: Only forward CPU's pkts
    assert(pkt->isRequest());

    //Addr v_addr = pkt->req->getVaddr();
    //Addr p_addr = pkt->req->getPaddr();
    //DPRINTF(SPM, "SPM sends request to PMMU for \tVA = %x \tPA = %x\n", v_addr, p_addr);

    //Forward everything to main memory
    //memSidePort->schedTimingReq(pkt, curTick()+1);
    //return true;


    if (pkt->req->getVaddr() >= myVAM->getStartAddr() && pkt->req->getVaddr() <= myVAM->getEndAddr()) {
    	// If the VA belongs to my SPM
        Cycles lat;
        satisfySPMEpiphanyAccess (pkt, &lat, false);
        // DPRINTF(SPM, "CPU <== SPM - VA = %x pkt.isLLSC %d\n", pkt->req->getVaddr(), pkt->isLLSC());
        DPRINTF(SPM, "Trace: Addr %x pkt.Write %d pkt.Read %d pkt.needsResponse %d is Hit in SPM\n", pkt->req->getVaddr(), pkt->isWrite(), pkt->isRead(), pkt->needsResponse());
        // pkt->retrieveCmd();
        incLocalHitCount(pkt); //stat-related
        sendTimingRespToCPU(pkt, lat);
        myPMMU->incrStats(SPM_HIT);
        return true;
    }
    else if (pkt->req->getVaddr() >= myVAM->getStartAddrRange() && pkt->req->getVaddr() <= myVAM->getEndAddrRange()) {
    	// If the VA belongs to a different SPM

    	unsigned id;
    	id = myVAM->getID(pkt->req->getVaddr());
    	DPRINTF(SPM, "Trace: Addr %x pkt.Write %d pkt.Read %d pkt.needsResponse %d is Hit in Remote SPM Cycle: %lld\n", pkt->req->getVaddr(), pkt->isWrite(), pkt->isRead(), pkt->needsResponse(), ticksToCycles(curTick()));

    	// If it is a write request; satisfy the access, and then generate the remote epiphany message - no ACK for write
    	if (pkt->isWrite()) {
    		// assert (!pkt->needsResponse());
    		PacketPtr new_pkt = new Packet(pkt);
    		assert (new_pkt->isWrite());
    		sendTimingRespToCPU(pkt, Cycles(1)); // FIXME: 5G, 1 cycle for write response

    		myPMMU->generateRemoteReqMsgEpiphany(new_pkt, id);
    	}
    	else {
    		myPMMU->generateRemoteReqMsgEpiphany(pkt, id);
    	}

		// memSidePort->schedTimingReq(pkt, curTick()+1);
       	myPMMU->incrStats(SPM_HIT);
		return true;
	}
    else if (myVAM->isOnStack(pkt->req->getVaddr())) {
        Cycles lat;
        satisfySPMEpiphanyAccess (pkt, &lat, true);
        DPRINTF(SPM, "Trace: Stack Addr %x pkt.Write %d pkt.needsResponse %d\n", pkt->req->getVaddr(), pkt->isLLSC(), pkt->needsResponse());
        incLocalHitCount(pkt);
        sendTimingRespToCPU(pkt, lat);
        myPMMU->incrStats(SPM_STACK_HIT);
        return true;
    }
    else if (myVAM->isOnGlobal(pkt->req->getVaddr())) {
        Cycles lat;
        satisfySPMEpiphanyAccess (pkt, &lat, false);
        DPRINTF(SPM, "Trace: Global Addr %x pkt.Write %d pkt.needsResponse %d\n", pkt->req->getVaddr(), pkt->isLLSC(), pkt->needsResponse());
        incLocalHitCount(pkt);
        sendTimingRespToCPU(pkt, lat);
        myPMMU->incrStats(SPM_STACK_HIT);
        return true;
    }
	else {
		DPRINTF(SPM, "Trace: Addr %x pkt.Write %d pkt.needsResponse %d pAddr: %x pktAddr: %x data: %d\n", pkt->req->getVaddr(), pkt->isWrite(), pkt->needsResponse(),
				pkt->req->getPaddr(), pkt->getAddr(), pkt->getData());
		pkt->spmInfo.makeOffchip();
		memSidePort->schedTimingReq(pkt, curTick()+1);
		myPMMU->incrStats(SPM_MISS);
		DPRINTF(HM, "%x\n", pkt->req->getVaddr());
		return true;
	}
    assert (0); // Can't reach this stage in Epiphany

    // Query PPMU for data holder or ask for performing a remote access
    DPRINTF(SPM, "Trace: %s Received: %x\n", __func__, pkt->req->getVaddr());
    pmmuMasterPort->schedTimingReq(pkt, curTick()+1); //stat-related

    return true;
}

bool
SPM::recvPMMUTimingReq(PacketPtr pkt)
{
    assert(pkt->isRequest());

    //case 1: page allocation
    if (pkt->govInfo.isAllocate()) {
        // forward it to main memory,
        // once response returned from memory we will fill the SPM
        initializePageAllocation(pkt);

        return true;
    }
    //case 2: page deallocation
    else if (pkt->govInfo.isDeallocate()) {
        // page number should be in the pkt
        // i) find the page
        // ii) call a pageDeallocation function
        initializePageDeallocation(pkt);

        return true;
    }
    //case 3: relocations
    //TODO: Consolidate this with the next case?
    else if (pkt->govInfo.isRelocationRead() || pkt->govInfo.isRelocationWrite()) {
        Cycles lat;
        satisfySPMAccess(pkt, &lat);

        pkt->saveCmd();
        pkt->makeResponse();
        pmmuSlavePort->schedTimingResp(pkt, clockEdge(lat));

        return true;
    }
    //case 4: remote requests
    else if (pkt->spmInfo.isRemote()) {
    	Addr v_addr = pkt->req->getVaddr();
    	Addr p_addr = pkt->req->getPaddr();
    	DPRINTF(SPM, "SPM sends request to PMMU for \tVA = %x \tPA = %x pkt.getAddr %x\n", v_addr, p_addr, pkt->getAddr());
    	assert(0);
        Cycles lat;
        satisfySPMAccess(pkt, &lat);

        pkt->saveCmd();
        pkt->makeResponse();
        pmmuSlavePort->schedTimingResp(pkt, clockEdge(lat));

        return true;
    }
    else {
        panic("Invalid request type received from PMMU");
    }
}

void
SPM::recvPMMUTimingResp(PacketPtr pkt)
{
    // response for a CPU request
    assert(pkt->isResponse());

    DPRINTF(SPM, "SPM <== PMMU - VA = %x\t - Data holder = %s pkt.addr %d pkt.isLLSC %d\n",
            pkt->req->getVaddr(), (pkt->spmInfo.toString()), pkt->getAddr(), pkt->isLLSC());

    bool is_error = pkt->isError();

    if (is_error) {
        DPRINTF(SPM, "SPM received packet with error for address %x (%s), "
                "cmd: %s\n", pkt->getAddr(), pkt->isSecure() ? "s" : "ns",
                pkt->cmdString());

        panic("Error packet received! We don't handle it.");
    }

    if (system->bypassCaches()) {
        memSidePort->sendTimingReq(pkt);
        return;
    }

    if (pkt->spmInfo.isLocal()) {

        Cycles lat;
        satisfySPMAccess (pkt, &lat);

        DPRINTF(SPM, "CPU <== SPM - VA = %x pkt.isLLSC %d\n", pkt->req->getVaddr(), pkt->isLLSC());

        // @TODO: temporary work-around for stats as hits should be counted for a req not resp
///     pkt->makeRequest(); //stat-related
        pkt->retrieveCmd();
        incLocalHitCount(pkt); //stat-related

        sendTimingRespToCPU(pkt, lat);

    } else if (pkt->spmInfo.isRemote()){

        DPRINTF(SPM, "CPU <== SPM - VA = %x\n", pkt->req->getVaddr());

        // TODO: temporary work-around for stats as hits should be counted for a req not resp
///     pkt->makeRequest(); //stat-related
        pkt->retrieveCmd();
        incRemoteHitCount(pkt); //stat-related

        sendTimingRespToCPU(pkt, responseLatency);

    } else if (pkt->spmInfo.isOffchip()) {
    	DPRINTF(SPM, "Trace: %s Received: %x Now sending it to main memory! cyclesToTicks(Cycles(1)): %d\n", __func__, pkt->req->getVaddr(), cyclesToTicks(Cycles(1)));

        //Block the cpu port if no buffer on mem side
//      conditionalBlocking(Blocked_MaxPendingReqs);
        assert (pendingReqs <= MAX_PENDING_REQS);

        // we need to make the packet a request packet first because
        // makeShallowResponse has been called on it by PMMU
///     pkt->makeRequest();
        pkt->retrieveCmd();
        incMissCount(pkt); //stat-related

        memSidePort->schedTimingReq(pkt, curTick()+1); //stat-related

    } else{
        panic("Invalid data holder!");
    }
}

bool
SPM::satisfySPMAccess(PacketPtr pkt, Cycles* lat)
{
    DPRINTF(SPM, "%s for %s VA = %x size %d pkt.addr %x pkt.isRead %d pkt.isWrite %d pkt.isLLSC %d pkt.origCmd.isLLSC %d\n", __func__,
            pkt->cmdString(), pkt->req->getVaddr(), pkt->getSize(), pkt->getAddr(), pkt->isRead(), pkt->isWrite(), pkt->isLLSC(), pkt->origCmd.isLLSC());

    unsigned int spm_page_index = pAddress2PageIndex(pkt->getAddr());
    assert (spm_page_index < size/pageSizeBytes);

    SPMPage *page =  &spmSlots[spm_page_index]; // TODO: we need to do some bookkeeping for accesses, latencies, etc

    if (pkt->isWrite()) {

        if (writeOK(pkt)) {
        	// pkt->req->setExtraData(1);
        	DPRINTF(SPM, "%s pkt.addr %x is LLSC() and isWrite()\n", __func__, pkt->getAddr());

        	pkt->writeDataToBlock(page->m_data, pageSizeBytes); // TODO: Fix me
        	*lat = page->getWriteSpeed();
///     incWriteEnergy(page->getWriteEnergy()*pkt->getSize()); //stat-related
        	page->refCount += 1;
        }

    } else if (pkt->isRead()) {

        if (pkt->origCmd.isLLSC() || pkt->isLLSC()) {
        	DPRINTF(SPM, "%s pkt.addr %x is LLSC() and isRead()\n", __func__, pkt->getAddr());
            trackLoadLocked(pkt);
        }

        pkt->setDataFromBlock(page->m_data, pageSizeBytes); // TODO: Fix me
        DPRINTF(SPM, "vaddr: %x spm_page_index: %d page m_data: %x page owner: %d\n", pkt->req->getVaddr(), spm_page_index, page->m_data[0], page->getOwner());
        *lat = page->getReadSpeed();
///     incReadEnergy(page->getReadEnergy()*pkt->getSize()); //stat-related
        page->refCount += 1;

    }

    return true;
}

bool
SPM::satisfySPMEpiphanyAccess(PacketPtr pkt, Cycles* lat, bool isOnStack)
{
    DPRINTF(SPM, "%s for %s VA = %x size %d pkt.addr %x pkt.isRead %d pkt.isWrite %d pkt.isLLSC %d pkt.origCmd.isLLSC %d\n", __func__,
            pkt->cmdString(), pkt->req->getVaddr(), pkt->getSize(), pkt->getAddr(), pkt->isRead(), pkt->isWrite(), pkt->isLLSC(), pkt->origCmd.isLLSC());

    uint64_t offset;
    if (isOnStack) {
    	Addr stackAddr = myVAM->getStackAddr(pkt->req->getVaddr());
    	offset = myVAM->getOffset(stackAddr);
    	assert (offset < size);
    }
    else {
    	if (myVAM->isOnGlobal(pkt->req->getVaddr())) {
        	Addr globalAddr = myVAM->getGlobalAddr(pkt->req->getVaddr());
        	offset = myVAM->getOffset(globalAddr);
        	assert (offset < size);
    	}
    	else {
    		offset = myVAM->getOffset(pkt->req->getVaddr());
    		assert (offset < size);
    	}
    }

    if (pkt->isWrite()) {
        if (writeOK(pkt)) {
        	DPRINTF(SPM, "%s pkt.addr %x is LLSC() and isWrite()\n", __func__, pkt->getAddr());

        	pkt->writeDataToBlock((data + offset), pkt->getSize()); // TODO: Fix me
        	*lat = Cycles(1);
        }
    } else if (pkt->isRead()) {
        if (pkt->origCmd.isLLSC() || pkt->isLLSC()) {
        	DPRINTF(SPM, "%s pkt.addr %x is LLSC() and isRead()\n", __func__, pkt->getAddr());
            trackLoadLocked(pkt);
        }
        pkt->setDataFromBlock((data + offset), pkt->getSize());
        *lat = Cycles(1);
    }
    return true;
}

bool
SPM::satisfyRemoteSPMEpiphanyAccess(PacketPtr pkt)
{
	assert(pkt->spmInfo.isRemote());
	assert(pkt->isResponse());
	DPRINTF(SPM, "%s addr: %x Cycles: %lld\n", __func__, pkt->req->getVaddr(), ticksToCycles(curTick()));
	/// pkt->makeRequest(); //stat-related
	pkt->retrieveCmd();
	incRemoteHitCount(pkt); //stat-related
	sendTimingRespToCPU(pkt, responseLatency);
	return true;
}

void
SPM::sendTimingRespToCPU(PacketPtr pkt, Cycles lat)
{
//    assert(pkt->isResponse());

    Tick completion_time;

    if (pkt->spmInfo.isLocal()) {
        completion_time = clockEdge(lat);
    }

    else if (pkt->spmInfo.isRemote()) {
        completion_time = clockEdge(lat);
    }
//  else if (pkt->data_holder == SPMDataLocation::Off_Chip){
    else{

        completion_time = clockEdge(lat);

        // @TODO: temporary work-around for stats as hit
        // should be counted for a req not resp
///     pkt->makeRequest(); //stat-related

        missLatency[pkt->cmdToIndex()][pkt->req->masterId()] +=
            completion_time - pkt->req->time(); //stat-related
    }

    // path for local/remote spm is already a response
    // path for off_chip is a request
    if (!pkt->isResponse())
        pkt->makeResponse();
    cpuSidePort->schedTimingResp(pkt, completion_time); //stat-related
    DPRINTF(SPM, "Sent response for address: %x\n", pkt->req->getVaddr());

    //delete pkt; //@TODO: Why can't we delete the packet here?
}

///////////////
//
// Alloc/Dealloc helper functions
//
///////////////

void
SPM::initializePageAllocation(PacketPtr pkt)
{
    //conditionalBlocking(Blocked_MaxPendingReqs);
    assert (pendingReqs <= MAX_PENDING_REQS);

    memSidePort->schedTimingReq(pkt, curTick()+1); //stat-related
}

void
SPM::satisfyPageAllocation(PacketPtr pkt)
{
    unsigned int spm_page_index = pAddress2PageIndex(pkt->govInfo.getSPMAddress());

    DPRINTF(SPM, "satisfyPageAllocation on SPM Page = %d\n", spm_page_index);

    SPMPage* page = &spmSlots[spm_page_index];
//  page->setOwner(pkt->origin); // we do it before allocation in governor
    pkt->writeData(page->m_data);
    //incWriteEnergy(page->getWriteEnergy()*(pageSizeBytes)); //stat-related
    page->refCount = 0;
    page->tickInserted =  curTick();

    //TODO: does it make sense?
    Tick page_write_latency = (pageSizeBytes/4)*page->getWriteSpeed();

    // return a response to PMMU
    pkt->govInfo.markComplete();
    pmmuSlavePort->schedTimingResp(pkt, curTick() + page_write_latency); //stat-related
}

void
SPM::triggerPageAllocEpiphany(PacketPtr pkt)
{
    //conditionalBlocking(Blocked_MaxPendingReqs);
	setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
    assert (pendingReqs <= MAX_PENDING_REQS);
    // DPRINTF(SPM, "Trigger page alloc in SPM for pktAddr: %x\n", pkt->getAddr());
    DPRINTF(SPM, "Trigger page allocation for SPM Addr: %x size: %d pktAddr: %x\n",
    		pkt->govInfo.getSPMAddress(), pkt->getSize(),
    		pkt->getAddr());
    if (pkt->govInfo.shouldMarkinVAM == true) {
    	Addr startVAAddr = pkt->govInfo.getVirtualAddress();
    	Addr endVAAddr = startVAAddr + pkt->getSize() - 1;
    	myVAM->push_global(startVAAddr, endVAAddr, pkt->govInfo.getSPMAddress(), pkt->getSize());
    }
    memSidePort->schedTimingReq(pkt, curTick()+1); //stat-related
}

void
SPM::finishPageAllocEpiphany(PacketPtr pkt)
{
    assert (isBlocked());
	clearBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);

	//printf ("pkt->govInfo.getSPMAddress() = %x pmmu_id: %d\n", pkt->govInfo.getSPMAddress(), myPMMU->getNodeID());
    uint64_t offset = myVAM->getOffset(pkt->govInfo.getSPMAddress());
    //printf ("data in finishP = %x offset = %d pmmu_id: %d\n", data, offset, myPMMU->getNodeID());
    pkt->writeDataToBlock((data + offset), pkt->getSize());
    DPRINTF(SPM, "Finished page allocation for SPM Addr: %x size: %d offset: %d data[0]: %d data[1]: %d pktAddr: %x pkt.data: %d\n",
    		pkt->govInfo.getSPMAddress(), pkt->getSize(),
    		offset, data[0], data[1], pkt->getAddr(), pkt->getData());

    delete pkt->req;
    delete pkt;
}

void
SPM::triggerSPMCopy(Addr spm_src_start, Addr spm_src_end, Addr spm_dest_start, Addr spm_dest_end, unsigned size)
{
	assert (spm_src_start >= myVAM->getStartAddr() && spm_src_end <= myVAM->getEndAddr());
	assert (spm_dest_start >= myVAM->getStartAddr() && spm_dest_start <= myVAM->getEndAddr());
	uint64_t offset_src = myVAM->getOffset(spm_src_start);
	uint64_t offset_dest = myVAM->getOffset(spm_dest_start);
	for (unsigned i = 0; i < size; i++) {
		data[offset_dest] = data[offset_src];
		offset_src++; offset_dest++;
	}
	// The copy latency is accounted for in the PMMU; example case is taken as 10 cycles; change it as per need
}

void
SPM::initializePageDeallocation (PacketPtr pkt)
{
    // conditionalBlocking(Blocked_MaxPendingReqs);
    assert (pendingReqs <= MAX_PENDING_REQS);

    unsigned int spm_page_index = pAddress2PageIndex(pkt->govInfo.getSPMAddress());

    DPRINTF(SPM, "initializePageDeallocation on SPM Page = %d\n", spm_page_index);

    // read the page copy it to the pkt
    SPMPage* page = &spmSlots[spm_page_index];
    pkt->setData(page->m_data);
    //incReadEnergy(page->getReadEnergy()*(pageSizeBytes)); //stat-related

    //TODO: does it make sense?
    Tick page_read_latency = (pageSizeBytes/4)*page->getReadSpeed();

    memSidePort->schedTimingReq(pkt, curTick() + page_read_latency); //stat-related
}

void
SPM::triggerPageDeallocEpiphany(PacketPtr pkt)
{
	assert (pendingReqs <= MAX_PENDING_REQS);

	setBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
    uint64_t offset = myVAM->getOffset(pkt->govInfo.getSPMAddress());
    pkt->setDataFromBlock((data + offset), pkt->getSize());
    DPRINTF(SPM, "Trigger page deallocation for SPM Addr: %x size: %d offset: %d data[0]: %d data[1]: %d pktAddr: %x pkt.data: %d\n",
    		pkt->govInfo.getSPMAddress(), pkt->getSize(),
    		offset, data[0], data[1], pkt->getAddr(), pkt->getData());

    if (pkt->govInfo.shouldMarkinVAM == true) {
    	Addr startVAAddr = pkt->govInfo.getVirtualAddress();
    	Addr endVAAddr = startVAAddr + pkt->getSize() - 1;
    	myVAM->erase_global(startVAAddr, endVAAddr, pkt->getSize());
    }

    memSidePort->schedTimingReq(pkt, curTick() + 1); // FIXME: page read latency?
}

void
SPM::finishPageDeallocEpiphany(PacketPtr pkt)
{
    assert (isBlocked());
	clearBlocked(BaseSPM::Blocked_Alloc_DeAlloc_Relocate);
	DPRINTF(SPM, "Finished page deallocation for pkt.addr: %x\n", pkt->getAddr());
    delete pkt->req;
    delete pkt;
}

void
SPM::satisfyPageDeallocation (PacketPtr pkt)
{
    // returning a response to PPMU
    pkt->govInfo.markComplete();
    pmmuSlavePort->schedTimingResp(pkt, curTick()+1); //stat-related
}

void
SPM::triggerStackAllocEpiphany(Addr startVAAddr, Addr endVAAddr, unsigned size)
{
	DPRINTF(SPM, "Allocate stack < %x %x >\n", startVAAddr, endVAAddr);
	// DPRINTF(HM, "Allocate stack < %x %x >\n", startVAAddr, endVAAddr);
	myVAM->push_stack(startVAAddr, endVAAddr, size);
}

void
SPM::triggerStackDeallocEpiphany(Addr startVAAddr, Addr endVAAddr, unsigned size, uint64_t params)
{
	DPRINTF(SPM, "Deallocate stack < %x %x >\n", startVAAddr, endVAAddr);
	if (params == 0) {
		myVAM->erase_stack(startVAAddr, endVAAddr, size);
	}
	else {
		myVAM->pop_stack();
	}
}

///////////////
//
// Blocking/Unblocking helper functions
//
///////////////

void
SPM::conditionalBlocking(BlockedCause cause)
{
    assert (pendingReqs < MAX_PENDING_REQS);
    pendingReqs++;
    if (pendingReqs == MAX_PENDING_REQS)
        setBlocked(cause);
}

void
SPM::conditionalUnblocking(BlockedCause cause)
{
    assert (pendingReqs <= MAX_PENDING_REQS);
    pendingReqs--;
    if (pendingReqs <= MAX_PENDING_REQS && isBlocked())
        clearBlocked(cause);
}

bool
SPM::acceptingMemReqs()
{
    assert(pendingReqs <= MAX_PENDING_REQS);
    return  (pendingReqs < MAX_PENDING_REQS);
}

/////////////////////
//
// PMMUSideSlavePort
//
/////////////////////

BaseSlavePort &
SPM::getSlavePort(const std::string &if_name, PortID idx)
{
    if (if_name == "pmmu_s_side") {
        return *pmmuSlavePort;
    } else {
        return BaseSPM::getSlavePort(if_name, idx);
    }
}

SPM::PMMUSideSlavePort::PMMUSideSlavePort(const std::string &_name, SPM *_spm, PortID id)
    : QueuedSlavePort(_name, NULL, _respQueue, id),
      _respQueue(*_spm, *this),
      my_spm(_spm)
{
    DPRINTF(SPM, "Created slave pmmuport on spm %s\n", _name);
}

bool
SPM::PMMUSideSlavePort::recvTimingReq(PacketPtr pkt)
{
    // this is called by the pmmu when SPM needs to fill a remote request or alloc/dealloc
    my_spm->recvPMMUTimingReq(pkt);
    return true;
}

void
SPM::PMMUSideSlavePort::recvFunctional(PacketPtr pkt)
{
    //@TODO:  implement - see rubyport.cc version
    panic("recvFunctional not implemented for PMMUSideSlavePort");
}

void
SPM::PMMUSideSlavePort::setBlocked()
{
    //@TODO:  implement
    panic("setBlocked not implemented for PMMUSideSlavePort");
}

void
SPM::PMMUSideSlavePort::clearBlocked()
{
    //@TODO:  implement
    panic("clearBlocked not implemented for PMMUSideSlavePort");
}

/////////////////////
//
// PMMUSideMasterPort
//
/////////////////////

BaseMasterPort &
SPM::getMasterPort(const std::string &if_name, PortID idx)
{
    if (if_name == "pmmu_m_side") {
        return *pmmuMasterPort;
    } else {
        return BaseSPM::getMasterPort(if_name, idx);
    }
}

SPM::PMMUSideMasterPort::PMMUSideMasterPort(const std::string &_name, SPM *_spm, PortID id)
    : QueuedMasterPort(_name, _spm, _reqQueue, _snoopRespQueue, id),
      _reqQueue(*_spm, *this),
      _snoopRespQueue(*_spm, *this, ""),
      my_spm(_spm)
{
    DPRINTF(SPM, "Created master pmmuport on spm %s\n", _name);
}

bool
SPM::PMMUSideMasterPort::recvTimingResp(PacketPtr pkt)
{
    // this is where SPM gets a reply from PMMU
    // regarding a request by its local CPU
    my_spm->recvPMMUTimingResp(pkt);
    return true;
}

// Raghu: SPM to support multi-threaded

void
SPM::trackLoadLocked(PacketPtr pkt)
{
    Request *req = pkt->req;
    Addr paddr = LockedAddr::mask(req->getPaddr());

    std::list<LockedAddr>::iterator i;

    for (i = lockedAddrList.begin(); i != lockedAddrList.end(); ++i) {
        if (i->matchesContext(req)) {
            DPRINTF(SPM, "Modifying lock record: context %d addr %#x\n",
                    req->contextId(), paddr);
            i->addr = paddr;
            return;
        }
    }

    // no record for this xc: need to allocate a new one
    DPRINTF(SPM, "Adding lock record: context %d addr %#x\n",
            req->contextId(), paddr);
    lockedAddrList.push_front(LockedAddr(req));
}


bool
SPM::checkLockedAddrList(PacketPtr pkt)
{
    Request *req = pkt->req;
    Addr paddr = LockedAddr::mask(req->getPaddr());
    bool isLLSC = pkt->origCmd.isLLSC() | pkt->isLLSC();

    bool allowStore = !isLLSC;

    std::list<LockedAddr>::iterator i = lockedAddrList.begin();

    if (isLLSC) {
        while (i != lockedAddrList.end()) {
            if (i->addr == paddr && i->matchesContext(req)) {
                DPRINTF(SPM, "StCond success: context %d addr %#x\n",
                        req->contextId(), paddr);
                allowStore = true;
                break;
            }
            i++;
        }
        req->setExtraData(allowStore ? 1 : 0);
    }
    // LLSCs that succeeded AND non-LLSC stores both fall into here:
    if (allowStore) {
        i = lockedAddrList.begin();
        while (i != lockedAddrList.end()) {
            if (i->addr == paddr) {
                DPRINTF(SPM, "Erasing lock record: context %d addr %#x\n",
                        i->contextId, paddr);
                // ThreadContext* ctx = system()->getThreadContext(i->contextId);
                // ctx->getCpuPtr()->wakeup(ctx->threadId());
                i = lockedAddrList.erase(i);
            } else {
                i++;
            }
        }
    }

    return allowStore;
}


