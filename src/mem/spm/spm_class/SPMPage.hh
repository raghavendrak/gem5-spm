#ifndef __MEM_SPMPAGE_HH__
#define __MEM_SPMPAGE_HH__

#include "base/types.hh"
#include "mem/ruby/common/TypeDefines.hh"
#include "mem/spm/api/spm_types.h"

#include <inttypes.h>

#include <cassert>
#include <iomanip>
#include <iostream>

//TODO: Unused for now -- just placeholder
enum SPMPageStatusBits {
    /** valid, readable */
    PageValid =          0x01,
    /** write permission */
    PageWritable =       0x02,
    /** read permission (yes, page can be valid but not readable) */
    PageReadable =       0x04,
    /** dirty (modified) */
    PageDirty =          0x08,
    /** page was referenced */
    PageReferenced =     0x10,
    /** page was a hardware prefetch yet unaccessed*/
    PageHWPrefetched =   0x20,
    /** page holds data from the secure memory space */
    PageSecure =         0x40
};

class SPMPage
{
  public:

    enum SPMSlotOccupancyStatus {
        FREE_SLOT = 0,
        USED_SLOT = 1,
        NUM_SLOT_OCCUPANCY
    };

    enum SPMSlotSpeedStatus {
        HIGH_SPEED,
        MEDIUM_SPEED,
        SLOW_SPEED,
        NUM_SLOT_SPEED
    };

    struct SlotStatus {
        SPMSlotOccupancyStatus occupancy;
        ThreadPriority priority;
        DataImportance importance;
        SPMSlotSpeedStatus speed;
        Cycles writeLatency;
        Cycles readLatency;
    };

  public:
    SPMPage()
    {
        obtainPageSize();
        alloc();

        setOccupancy(FREE_SLOT);
        setSpeed(HIGH_SPEED, Cycles(1), Cycles(1));
        setPriority(HIGH_PRIORITY);
        setImportance(MAX_IMPORTANCE);

        setAccessEnergy(0, 0);
        refCount = 0;
        readRefCount = 0;
        writeRefCount = 0;
        tickInserted = 0;
        lastTickAccessed = 0;
    }

    SPMPage(const SPMPage &cp);

    ~SPMPage()
    {
        if (m_alloc)
            delete [] m_data;
    }

    SPMPage& operator=(const SPMPage& obj);

    void assign(uint8_t *data);

    void clear();
    uint8_t getByte(int whichByte) const;
    const uint8_t *getData(int offset, int len) const;
    void setByte(int whichByte, uint8_t data);
    void setData(uint8_t *data, int offset, int len);
    void copyPartial(const SPMPage & dblk, int offset, int len);
    bool equal(const SPMPage& obj) const;
    void print(std::ostream& out) const;

    void setOccupancy (SPMSlotOccupancyStatus occupancy);
    void setUsed ();
    void setFree ();
    bool isFree();
    SPMSlotOccupancyStatus getOccupancy();

    void setSpeed (SPMSlotSpeedStatus speed, Cycles writeLatency, Cycles readLatency);
    SPMSlotSpeedStatus getSpeed();
    Cycles getReadSpeed();
    Cycles getWriteSpeed();

    void setPriority(ThreadPriority priority);
    ThreadPriority getPriority();

    void setImportance (DataImportance importance);
    DataImportance getImportance();

    void setAccessEnergy (float writeEnergy, float readEnergy);
    float getReadEnergy();
    float getWriteEnergy();

    void setOwner (NodeID owner);
    NodeID getOwner();

    uint8_t *m_data; // TODO: this needs to be made private

    float writeEnergy;
    float readEnergy;

    NodeID ownerNode;

    int pageSize;
    void obtainPageSize();

    /** Number of references to this page since it was brought in. */
    uint64_t refCount;
    uint64_t readRefCount;
    uint64_t writeRefCount;
    /** Last time this page was accessed */
    Tick lastTickAccessed;
    /** When this page was inserted */
    Tick tickInserted;

    void resetStatsForNewlyAddedPage();
    void readRefOccurred();
    void writeRefOccurred();
    uint64_t getRefCount();
    Tick getLastTickAccessed();
    float pageUtlization();

  private:
    void alloc();
    bool m_alloc;
    SlotStatus status;
};

inline void
SPMPage::assign(uint8_t *data)
{
    assert(data != NULL);
    if (m_alloc) {
        delete [] m_data;
    }
    m_data = data;
    m_alloc = false;
}

inline uint8_t
SPMPage::getByte(int whichByte) const
{
    return m_data[whichByte];
}

inline void
SPMPage::setByte(int whichByte, uint8_t data)
{
    m_data[whichByte] = data;
}

inline void
SPMPage::copyPartial(const SPMPage & dblk, int offset, int len)
{
    setData(&dblk.m_data[offset], offset, len);
}

inline std::ostream&
operator<<(std::ostream& out, const SPMPage& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

inline bool
operator==(const SPMPage& obj1,const SPMPage& obj2)
{
    return obj1.equal(obj2);
}

#endif // __MEM_SPMPAGE_HH__
