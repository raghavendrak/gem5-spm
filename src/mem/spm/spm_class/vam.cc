#include "mem/spm/spm_class/vam.hh"
#include "debug/VAM.hh"

VAM::VAM(unsigned int id, unsigned int s, unsigned int num_spms)
{
	SPMId = id;
	SPMSize = s;

	Addr begin = 0xefffffff;
	for (unsigned i = 0; i <= id; i++) {
		begin -= s;
	}
	end = begin + s;
	start = begin + 1;
	stack_ptr = end - 1024 + 1; // 1kB stack FIXME: use it as a parameter
	endRange = 0xefffffff;
	startRange = endRange - (num_spms * SPMSize) + 1;
	DPRINTF (VAM, "id: %d s: %d start: %x end: %x startRange: %x\n", id, s, start, end, startRange);

	begin = 0xefffffff;
	for (unsigned i = 0; i < num_spms; i++) {
		begin -= s;
		VAMTable entry;
		entry.startAddr = begin + 1;
		entry.endAddr = begin + s;
		entry.id = i;
	    DPRINTF (VAM, "id: %d start: %x end: %x\n", entry.id, entry.startAddr, entry.endAddr);
	    vamEntry.push_back(entry);
	}
}

Addr
VAM::getStackAddr(Addr addr)
{
	for(std::vector<StackTable>::iterator it = stackEntry.begin(); it != stackEntry.end(); ++it) {
		if (addr >= it->startAddr && addr <= it->endAddr) {
			Addr offset = addr - it->startAddr;
			return ((it->stackAddr + offset));
		}
	}
	assert (0);
	return addr;
}

bool
VAM::isOnStack(Addr addr)
{
	for(std::vector<StackTable>::iterator it = stackEntry.begin(); it != stackEntry.end(); ++it) {
		if (addr >= it->startAddr && addr <= it->endAddr) {
			return true;
		}
	}
	return false;
}

void
VAM::push_stack(Addr startVAAddr, Addr endVAAddr, unsigned size)
{
	StackTable entry;
	entry.startAddr = startVAAddr;
	entry.endAddr = endVAAddr;
	entry.stackAddr = stack_ptr;
	stackEntry.push_back(entry);
	stack_ptr = stack_ptr + size;
	assert (stack_ptr <= end);
	stack_size.push_back(size);
}

void
VAM::erase_stack(Addr startVAAddr, Addr endVAAddr, unsigned size)
{
	for(std::vector<StackTable>::iterator it = stackEntry.begin(); it != stackEntry.end(); ++it) {
		if (startVAAddr == it->startAddr) {
			stackEntry.erase(it);
			stack_ptr = stack_ptr - size;
			assert (stack_ptr >= start); // FIXME: have to remove from stack_size too!
			return;
		}
	}
	assert (0);
}

void
VAM::pop_stack()
{
	stackEntry.pop_back();
	stack_ptr = stack_ptr - stack_size.back();
	stack_size.pop_back();
	assert (stack_ptr >= start);
}

unsigned
VAM::getID(Addr addr)
{
	for(std::vector<VAMTable>::iterator it = vamEntry.begin(); it != vamEntry.end(); ++it) {
		if (addr >= it->startAddr && addr <= it->endAddr) {
			return it->id;
		}
	}
	panic ("Invalid VAM address\n");
}

uint64_t VAM::getOffset(Addr va)
{
	assert (va >= start);
	return (va - start);
}

Addr VAM::getStartAddr()
{
	return start;
}

Addr VAM::getEndAddr()
{
	return end;
}

Addr VAM::getStartAddrRange()
{
	return startRange;
}

Addr VAM::getEndAddrRange()
{
	return endRange;
}

VAM::~VAM()
{

}

// Might have duplicated the code for Global; as of now going ahead to see if global addresses require new functionality

Addr
VAM::getGlobalAddr(Addr addr)
{
	for(std::vector<GlobalTable>::iterator it = globalEntry.begin(); it != globalEntry.end(); ++it) {
		if (addr >= it->startAddr && addr <= it->endAddr) {
			Addr offset = addr - it->startAddr;
			return ((it->globalAddr + offset));
		}
	}
	assert (0);
	return addr;
}

bool
VAM::isOnGlobal(Addr addr)
{
	for(std::vector<GlobalTable>::iterator it = globalEntry.begin(); it != globalEntry.end(); ++it) {
		if (addr >= it->startAddr && addr <= it->endAddr) {
			return true;
		}
	}
	return false;
}

void
VAM::push_global(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, unsigned size)
{
	GlobalTable entry;
	entry.startAddr = startVAAddr;
	entry.endAddr = endVAAddr;
	entry.globalAddr = startSPMAddr;
	globalEntry.push_back(entry);
}

void
VAM::erase_global(Addr startVAAddr, Addr endVAAddr, unsigned size)
{
	for(std::vector<GlobalTable>::iterator it = globalEntry.begin(); it != globalEntry.end(); ++it) {
		if (startVAAddr == it->startAddr) {
			globalEntry.erase(it);
			return;
		}
	}
	assert (0);
}
