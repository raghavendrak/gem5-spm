#include "mem/spm/spm_class/SPMPage.hh"
#include "mem/spm/pmmu.hh"

#include <iostream>

SPMPage::SPMPage(const SPMPage &cp)
{
    m_data = new uint8_t[pageSize];
    memcpy(m_data, cp.m_data, pageSize);
    m_alloc = true;
    refCount = cp.refCount;
    readRefCount = cp.readRefCount;
    writeRefCount = cp.writeRefCount;
    tickInserted = cp.tickInserted;
    ownerNode = 100;
}

void
SPMPage::obtainPageSize()
{
	pageSize = PMMU::getPageSizeBytes();
}

void
SPMPage::alloc()
{
    m_data = new uint8_t[pageSize];
    m_alloc = true;
    clear();
}

void
SPMPage::clear()
{
    memset(m_data, 0, pageSize);
}

bool
SPMPage::equal(const SPMPage& obj) const
{
    return !memcmp(m_data, obj.m_data, pageSize);
}

void
SPMPage::print(std::ostream& out) const
{
    using namespace std;

    int size = pageSize;
    out << "[ ";
    for (int i = 0; i < size; i++) {
        out << setw(2) << setfill('0') << hex << "0x" << (int)m_data[i] << " ";
        out << setfill(' ');
    }
    out << dec << "]" << flush;
}

const uint8_t*
SPMPage::getData(int offset, int len) const
{
    assert(offset + len <= pageSize);
    return &m_data[offset];
}

void
SPMPage::setData(uint8_t *data, int offset, int len)
{
    assert(offset + len <= pageSize);
    memcpy(&m_data[offset], data, len);
}

SPMPage &
SPMPage::operator=(const SPMPage & obj)
{
//	assert (obj);
	assert (obj.m_data);
    memcpy(m_data, obj.m_data, pageSize);
    return *this;
}

void
SPMPage::setOccupancy(SPMSlotOccupancyStatus occupancy)
{
	status.occupancy = occupancy;
}

void
SPMPage::setFree()
{
	status.occupancy = SPMPage::FREE_SLOT;
}

void
SPMPage::setUsed()
{
	status.occupancy = SPMPage::USED_SLOT;
}

SPMPage::SPMSlotOccupancyStatus
SPMPage::getOccupancy()
{
	return status.occupancy;
}

bool
SPMPage::isFree()
{
	return status.occupancy == SPMPage::FREE_SLOT;
}

void
SPMPage::setSpeed (SPMSlotSpeedStatus speed, Cycles writeLatency, Cycles readLatency)
{
	status.speed = speed;
	status.writeLatency = writeLatency;
	status.readLatency = readLatency;
}

SPMPage::SPMSlotSpeedStatus
SPMPage::getSpeed()
{
	return status.speed;
}

Cycles
SPMPage::getReadSpeed()
{
	return status.readLatency;
}

Cycles
SPMPage::getWriteSpeed()
{
	return status.writeLatency;
}

void
SPMPage::setPriority(ThreadPriority priority)
{
	status.priority = priority;
}

ThreadPriority
SPMPage::getPriority()
{
	return status.priority;
}

void
SPMPage::setImportance (DataImportance importance)
{
	status.importance = importance;
}

DataImportance
SPMPage::getImportance()
{
	return status.importance;
}

void
SPMPage::setAccessEnergy (float _writeEnergy, float _readEnergy)
{
	writeEnergy = _writeEnergy;
	readEnergy = _readEnergy;
}

float
SPMPage::getReadEnergy()
{
	return readEnergy;
}

float
SPMPage::getWriteEnergy()
{
	return writeEnergy;
}

void
SPMPage::setOwner (NodeID owner)
{
	ownerNode = owner;
}

NodeID
SPMPage::getOwner()
{
	return ownerNode;
}

void
SPMPage::resetStatsForNewlyAddedPage()
{
	tickInserted = curTick();
	refCount = 0;
	readRefCount = 0;
	writeRefCount = 0;
	lastTickAccessed = 0;
}

void
SPMPage::readRefOccurred()
{
	refCount += 1;
	readRefCount += 1;
	lastTickAccessed = curTick();
}

void
SPMPage::writeRefOccurred()
{
	refCount += 1;
	writeRefCount += 1;
	lastTickAccessed = curTick();
}

uint64_t
SPMPage::getRefCount()
{
    return refCount;
}

Tick
SPMPage::getLastTickAccessed()
{
    return lastTickAccessed;
}

float
SPMPage::pageUtlization()
{
	Tick age = curTick() - tickInserted;
	if (age == 0)
		return 0;
	else
		return (refCount)/age;
}
