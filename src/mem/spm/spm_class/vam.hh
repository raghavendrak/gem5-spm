#ifndef __VAM_HH__
#define __VAM_HH__

#include <map>
#include <limits>
#include "base/types.hh"
#include "cpu/base.hh"
#include "cpu/thread_context.hh"
#include "sim/system.hh"

#include "base/misc.hh"

typedef struct VAMTable
{
    unsigned id;
    Addr startAddr;
    Addr endAddr;
} VAMTable;

typedef struct StackTable
{
    Addr startAddr;
    Addr endAddr;
    Addr stackAddr;
} StackTable;

typedef struct GlobalTable
{
    Addr startAddr;
    Addr endAddr;
    Addr globalAddr;
} GlobalTable;

// Virtual Address Management
class VAM
{

  public:
    VAM(unsigned int, unsigned int, unsigned int);
    ~VAM();

    Addr getStartAddr();
    Addr getEndAddr();
    Addr getStartAddrRange();
    Addr getEndAddrRange();
    uint64_t getOffset(Addr);
    unsigned getID(Addr addr);
    void push_stack(Addr startVAAddr, Addr endVAAddr, unsigned size);
    bool isOnStack(Addr addr);
    Addr getStackAddr(Addr addr);
    void erase_stack(Addr startVAAddr, Addr endVAAddr, unsigned size);
    void push_global(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, unsigned size);
    bool isOnGlobal(Addr addr);
    Addr getGlobalAddr(Addr addr);
    void erase_global(Addr startVAAddr, Addr endVAAddr, unsigned size);
    void pop_stack();

  private:
    unsigned int SPMId;
    unsigned int SPMSize;
    Addr start;
    Addr end;
    Addr startRange;
    Addr endRange;
    std::vector<VAMTable> vamEntry;
    std::vector<StackTable> stackEntry;
    std::vector<GlobalTable> globalEntry;
    Addr stack_ptr;
    std::vector<unsigned> stack_size;
};

#endif /* __VAM_HH__ */
