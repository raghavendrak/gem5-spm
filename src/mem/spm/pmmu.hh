#ifndef __PMMU_HH__
#define __PMMU_HH__

#include <iostream>
#include <sstream>
#include <string>

#include "mem/protocol/Types.hh"
#include "mem/ruby/common/Consumer.hh"
#include "mem/ruby/slicc_interface/AbstractController.hh"
#include "mem/spm/spm_class/spm.hh"
#include "mem/spm/att.hh"
#include "mem/qport.hh"
#include "mem/page_table.hh"
#include "mem/spm/spm_message/SPMRequestMsg.hh"
#include "mem/spm/spm_message/SPMResponseMsg.hh"
#include "mem/spm/api/spm_types.h"
#include "params/PMMU.hh"

#define NUM_NETWORKS 32

class BaseGovernor;

class PMMU : public AbstractController
{
    friend class BaseGovernor;

  public:

    class SPMSideSlavePort : public QueuedSlavePort
    {
      private:

      RespPacketQueue _respQueue;
      PMMU *my_pmmu;

      public:
        SPMSideSlavePort(const std::string &_name, PMMU *_pmmu, PortID id);
        bool recvTimingReq(PacketPtr pkt);

      protected:

        Tick recvAtomic(PacketPtr pkt)
        { panic("PMMU::SPMSideSlavePort::recvAtomic() not implemented!\n"); }

        void recvFunctional(PacketPtr pkt);

        AddrRangeList getAddrRanges() const
        { AddrRangeList ranges; return ranges; }

      private:
        bool isPhysMemAddress(Addr addr) const;
    };

    class SPMSideMasterPort : public QueuedMasterPort
    {
      private:

        ReqPacketQueue _reqQueue;
        SnoopRespPacketQueue _snoopRespQueue;
        PMMU *my_pmmu;

      public:
        SPMSideMasterPort(const std::string &_name, PMMU *_pmmu, PortID id);
        bool recvTimingResp(PacketPtr pkt);

      protected:

        Tick recvAtomic(PacketPtr pkt)
        { panic("PMMU::SPMSideMasterPort::recvAtomic() not implemented!\n"); }

        void recvFunctional(PacketPtr pkt);

        AddrRangeList getAddrRanges() const
        { AddrRangeList ranges; return ranges; }

      private:
        bool isPhysMemAddress(Addr addr) const;
    };

    typedef PMMUParams Params;
    PMMU(const Params *p);
    void init();
    void initNetQueues();

    SPM *my_spm_ptr;

    SPMSideSlavePort *spmSlavePort;     // port connected to local spm to respond to local requests
    SPMSideMasterPort *spmMasterPort;   // port connected to local spm to fulfill remote and alloc/dealloc/reloc requests

    virtual BaseMasterPort &getMasterPort(const std::string &if_name,
                                          PortID idx = InvalidPortID);
    virtual BaseSlavePort &getSlavePort(const std::string &if_name,
                                        PortID idx = InvalidPortID);

    void setSPM(SPM *_spm);

    BaseGovernor *getGovernor() { return my_governor_ptr; }

    // aligns the address to virtual page boundaries
    static Addr spmPageAlign(Addr v_addr)     { return spmPageAlignDown(v_addr); }
    static Addr spmPageAlignDown(Addr v_addr) { return (v_addr & ~(Addr(m_page_size_bytes - 1))); }
    static Addr spmPageAlignUP(Addr v_addr)   { return (v_addr + (m_page_size_bytes - 1)) &
                                                       ~(Addr(m_page_size_bytes - 1)); }

    static int getPageSizeBytes() { return m_page_size_bytes; }
    int getSPMSizePages() { return my_spm_ptr->getSize()/m_page_size_bytes; }

    NodeID getNodeID() const {return m_machineID.num;};

    /* alloc/dealloc/relocation request helper functions */
    int addATTMappingsVAddress(Addr start_v_page_addr, 
                               MachineID host_node,
                               Addr start_p_spm_addr,
                               int num_pages, 
                               FuncPageTable *pt, 
                               AllocationModes alloc_mode,
                               bool wait_for_signal = false, 
                               NodeID signaler = 0);

    int removeATTMappingsVAddress(Addr start_v_page_addr, 
                                  int num_pages, 
                                  FuncPageTable *pt, 
                                  DeallocationModes dealloc_mode,
                                  bool signal = false,
                                  NodeID signalee = 0);

    int removeATTMappingsSPMAddress(MachineID host_node,
                                    Addr p_spm_addr,
                                    int num_pages, 
                                    FuncPageTable *pt, 
                                    DeallocationModes dealloc_mode,
                                    bool signal = true,
                                    NodeID signalee = 0);

    void changeATTMappingVAddress(Addr start_v_page_addr, 
                                  MachineID future_host_node,
                                  Addr future_start_p_spm_addr,
                                  int num_pages,
                                  bool signal = true,
                                  NodeID signalee = 0);
    
    void changeATTMappingSPMAddress(MachineID current_host_node,
                                    Addr current_start_p_spm_addr,
                                    int num_pages,
                                    MachineID future_host_node,
                                    Addr future_start_p_spm_addr,
                                    bool signal = true,
                                    NodeID signalee = 0);

    void setUsedPages(Addr start_p_spm_addr, int num_pages,
                      DataImportance data_importance,
                      ThreadPriority app_priority,
                      NodeID owner);
    void setFreePages(Addr start_p_spm_addr, int num_pages);

    void wakeup();

    // Epiphany
    unsigned int numSPMs;
    void trigger_spm_allocate_dma(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
    		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params);
    bool generateRemoteReqMsgEpiphany(PacketPtr pkt, unsigned id);
    void trigger_spm_deallocate_dma(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
    		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params);
    void trigger_spm_stack_allocate(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
    		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params);
    void trigger_spm_stack_deallocate(Addr startVAAddr, Addr endVAAddr, Addr startSPMAddr, Addr endSPMAddr, MachineID host_node,
    		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params);
    void trigger_spm_copy(Addr spm_src_start, Addr spm_src_end, Addr spm_dest_start, Addr spm_dest_end, MachineID host_node,
    		FuncPageTable *pt, NodeID signaler, unsigned size, uint64_t params);
    void incrStats(SpmStats stat);

    // 5G
    uint64_t block_till_cycle;
    uint64_t block_network_for_cycles;
    void blockEpiphany(uint64_t blockCycles, uint64_t block_mode);
    bool processRemoteRequestMsg_Networks(int netId);
    Addr getBankNetworkId(Addr addr);
    std::map<uint64_t, uint64_t> execTimeList;

  private:

    Cycles m_request_latency;
    //MessageBuffer* m_mandatoryQueue_ptr;
    MessageBuffer* m_responseFromSPM_ptr;
    MessageBuffer* m_responseToSPM_ptr;
    MessageBuffer* m_requestFromSPM_ptr;
    MessageBuffer* m_requestToSPM_ptr;
    MessageBuffer* m_responseToNetwork_ptr;
    MessageBuffer* m_requestToNetwork_ptr;

    MessageBuffer* m_net_net_ptr[NUM_NETWORKS];
    MessageBuffer* m_net_spm_ptr[NUM_NETWORKS];

    static int m_page_size_bytes;

    BaseGovernor *my_governor_ptr;
    uint32_t pending_gov_reqs;

    ATT my_att;

    void addToGovernor(std::string gov_type);

    bool recvSPMTimingReq(PacketPtr pkt);
    bool recvSPMTimingResp(PacketPtr pkt);

    void triggerPageAlloc  (Addr v_page_addr,
                            MachineID host_node,
                            Addr p_spm_addr,
                            FuncPageTable *pt,
                            AllocationModes alloc_mode,
                            bool wait_for_signal, 
                            NodeID signaler);

    void triggerPageDeAlloc(Addr v_page_addr,
                            MachineID host_node,
                            Addr p_spm_addr,
                            FuncPageTable *pt, 
                            DeallocationModes dealloc_mode,
                            bool signal,
                            NodeID signalee);

    void triggerPageRelocation(MachineID current_host_node,
                               Addr current_p_spm_addr,
                               MachineID future_host_node,
                               Addr future_p_spm_addr,
                               bool signal,
                               NodeID signalee);

    void continuePageRelocationRequest(PacketPtr relocation_pkt);
    void finalizeLocalGOVReq(PacketPtr gov_pkt);

    // packet to message routines
    bool generateAccessReqMsg(PacketPtr pkt);
    bool generateAccessRespMsg(PacketPtr pkt);
    bool generateGovRespMsg(PacketPtr pkt);

    // message processing helpers
    bool processRemoteResponseMsg();
    bool processRemoteRequestMsg(bool *sending_mem_reqs_allowed);
    bool processLocalResponseMsg();
    bool processLocalRequestMsg(bool *sending_mem_reqs_allowed);

    // relocation handling
    bool sendRelocationDone(PacketPtr pkt);
    void fireupPendingAllocs(PacketPtr pkt);

    const SPMRequestMsg* getNextReadyRequestMsg(MessageBuffer *mb) const;

  public:

    void print(std::ostream& out) const;
    void resetStats();
    void regStats();
    void collateStats();

    static int getNumControllers();

    MessageBuffer *getMandatoryQueue() const;
    MessageBuffer *getMemoryQueue() const;

    void recordCacheTrace(int cntrl, CacheRecorder* tr);
    Sequencer* getCPUSequencer() const;
    GPUCoalescer* getGPUCoalescer() const;
    int functionalWriteBuffers(PacketPtr&);

  private:

    static int m_num_controllers;

    AccessPermission getAccessPermission(const Addr& param_addr);
    void functionalRead(const Addr& param_addr, Packet* param_pkt);
    int functionalWrite(const Addr& param_addr, Packet* param_pkt);
};
#endif // __PMMU_HH__
