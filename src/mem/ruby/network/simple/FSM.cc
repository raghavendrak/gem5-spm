#include "mem/ruby/network/simple/FSM.hh"

using namespace std;

FSM::FSM()
{
	state = -1;	// When FSM is first initialized, the counter is set to state++
	count_ph_times = 0;
	ph_times = std::numeric_limits<int>::max();
}

FSM::~FSM()
{

}

void
FSM::populate(unsigned c, unsigned sid)
{

	// Populate the entry according to the sid (switch id)
	// * 1 clockEdge() == 1000 ticks
	// * 1 cycle == 500 ticks

	FSMEntry entry;

	if (sid == 0) {
		entry.cycles = 10000;
		entry.dest.push_back(1); // Switch-0 always sends to Switch-1 in this example
		fsmTable.push_back(entry);
	}
	else if (sid == 1) {
		entry.cycles = 20000;
		entry.dest.push_back(0); // Switch-1 always sends to Switch-0 in this example
		fsmTable.push_back(entry);
	}
	else {
		entry.cycles = 20000;
		entry.dest.push_back(0); // Have a default just in case you run on more number of cores than the application
		fsmTable.push_back(entry);
	}

	ph_times = c;

}

bool
FSM::getAllowedDestination(int out)
{
	std::vector<unsigned>::iterator it;
	bool flag = false;
	for (it = fsmTable[state].dest.begin(); it != fsmTable[state].dest.end(); it++) {
		if (*it == out) {
			flag = true;
			break;
		}
	}
	return flag;
}

uint64_t
FSM::getNextPhase(bool& ph_times_over, uint64_t curr_c)
{
	state++;
	if (state == fsmTable.size()) {
		state = 0;
		count_ph_times++;
		if (count_ph_times == ph_times) {
			ph_times_over = true;
		}
		else {
			ph_times_over = false;
		}
	}
	else {
		ph_times_over = false;
	}
	fsmTable[state].end_cycles = fsmTable[state].cycles + curr_c;
	return fsmTable[state].cycles;
}

uint64_t
FSM::getEndCycles()
{
	return fsmTable[state].end_cycles;
}
