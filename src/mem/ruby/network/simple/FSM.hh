#ifndef __MEM_RUBY_NETWORK_SIMPLE_FSM_HH__
#define __MEM_RUBY_NETWORK_SIMPLE_FSM_HH__

#include <vector>
#include <limits>
#include "base/types.hh"

typedef struct FSMEntry
{
    uint64_t cycles;
    uint64_t end_cycles;
    std::vector<unsigned> dest;
} FSMEntry;


class FSM
{
public:
	FSM();
	~FSM();

	void populate(unsigned c, unsigned sid);
	uint64_t getNextPhase(bool& ph_times_over, uint64_t curr_c);
	uint64_t getEndCycles();
	bool getAllowedDestination(int out);

private:
	int state;
	unsigned ph_times;	// How many times the FSM should traverse
	unsigned count_ph_times;
	std::vector<FSMEntry> fsmTable;
};

#endif // __MEM_RUBY_NETWORK_SIMPLE_FSM_HH__
